#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import logging
import numpy as np


# TIRL IMPORTS

from tirl.tirlobject import TIRLObject
from tirl.operations import Operator
from tirl.operations.tensor import TensorOperator
from tirl.operations.broadcast import TensorBroadcast
from tirl import settings as ts


# DEFINITIONS

MASKMODES = ("and", "or")
MATCHMODES = (None, "broadcast", "l1_norm", "l2_norm")


# IMPLEMENTATION

class Cost(TIRLObject):
    r"""
    Cost - base class that implements the following generic cost function term:

    $$C_i=\sum_{x\in\mathcal{D}\left[\mathcal{T}\right]}c_{i}\left(\phi_t\left(
    \mathcal{T}_\left(\mathbf{x}\right)\right),\phi_{s_{2}}\left(\phi_{s_{1}}
    \left(\mathcal{S}\right)_\left(\sigma_{p}^{-1}\left(\tau_{q}\left(
    \mathbf{x}\right)\right)\right)\right)\right)$$

    where:

    - $C_i$ is the i-th term of the total cost $C$ (both are real numbers)
    - $\mathcal{T}$ is the target image (tensor image)
    - $\mathcal{S}$ is the source image (tensor image)
    - $\mathbf{x}\in\mathcal{D}\left[\mathcal{T}\right]$ are points of the
      target image domain
    - $\sigma_{p}$ is the transformation chain of the source domain with
      parameter vector $p$, the ${}^{-1}$ exponent denotes its inverse
    - $\tau_{q}$ is the transformation chain of the target domain with
      parameter vector $q$
    - $\phi_{t}$ is the target image filter function
    - $\phi_{s_{1}}$ is the source image pre-filter
      (executed before interpolation)
    - $\phi_{s_{2}}$ is the source image post-filter
      (executed after interpolation)
    - $c_i$ is the i-th cost function that has two arguments:
      the target constituent and the source constituent, respectively.
      These are two tensor images defined on the target domain.

    The filters $\phi_{t}, \phi_{s_{1}}, \phi_{s_{2}}$ must be defined in a way
    that the target and source constituents have conformable tensor shapes as a
    result of applying these operations. Tensor shapes are conformable if and
    only if all operations that are necessary to calculate $c_{i}$ are pairwise
    defined for the target and source constituents. For example, if $c_i$
    implies subtraction of two vector-valued constituents, the vectors must
    have the same dimensionality.

    Cost objects are callable and return a scalar cost. Multiple Cost objects
    can be defined for any registration, which case the total registration cost
    is calculated as the sum of all terms (including regularisation terms;
    see also: Regulariser class).

    """

    RESERVED_KWARGS = ("source", "target", "target_filter", "source_prefilter",
                       "source_postfilter", "maskmode", "matchmode", "logger",
                       "metaparameters")

    def __init__(self, source, target, target_filter=None,
                 source_prefilter=None, source_postfilter=None,
                 maskmode="and", matchmode="l2_norm", logger=None,
                 **metaparameters):
        """
        Initialisation of Cost.

        :param source:
            Moving image. Values of the source image are interpolated at the
            points of the target domain.
        :type source: TImage
        :param target:
            Fixed image. Values of the source image are interpolated at the
            points of the target domain.
        :type target: TImage
        :param target_filter:
            Filtering that is applied to the target TImage at the construction
            of the Cost object. The target filter must be an Operator,
            otherwise it must be callable with a single input argument for the
            target TImage, and the callable must return a TImage with the
            Domain of the target TImage (not a copy). If None (default), no
            filtering is performed.
        :type target_filter: Operator or Callable or None
        :param source_prefilter:
            Filtering that is applied to the source TImage at the construction
            of the Cost object. The source prefilter must be an Operator,
            otherwise it must be callable with a single input argument for the
            source TImage, and the callable must return a TImage with the
            Domain of the source TImage (not a copy). If None (default), no
            filtering is performed.
        :type source_prefilter: Operator or Callable or None
        :param source_postfilter:
            Filtering that is applied to the source TImage after interpolation,
            before the cost is calculated. The source postfilter must be an
            Operator, otherwise it must be callable with a single input
            argument for the resampled source TImage, and the callable must
            return a TImage with the Domain of the resampled source TImage
            (i.e. the domain of the target TImage; not a copy). If None
            (default), no filtering is performed.
        :type source_postfilter: Operator or Callable or None
        :param maskmode:
            Defines how the masks of the source and the target TImage should be
            combined into one mask. The following methods are supported:
            1. "and": combined mask values are calculated as the geometric mean
                      of corresponding source and target mask values (default).
                      For binary masks, this is equivalent to the AND operation.
            2. "or": combined mask values are calculated as the quadratic mean
                     of corresponding source and target mask values. For binary
                     masks this is equivalent to the OR operation.
            3. callable: function(target_mask, source_mask) -> combined_mask.
        :type maskmode: str or Callable
        :param matchmode:
            If the filtered target image and the prefiltered, resampled and
            post-filtered source image do not have conformant tensor shapes,
            this routine will be applied automatically to create conformable
            shapes. The following modes are available:
            1. None: turned off: raise error if tensors are non-conformant
            2. "broadcast": broadcast tensor shapes according to NumPy rules
            3. "l1_norm" (default): reduce to scalar using the L1-norm
            4. "l2_norm": reduce to scalar using the L2-norm
            5. callable: function(target, source_on_target) -> trg, src_on_trg
        :type matchmode: str or Callable or None
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger
        :param metaparameters:
            Additional keyword arguments.
        :type metaparameters: Any

        """
        # Invoke TIRLObject initialisation
        super(Cost, self).__init__()

        # Attach target image (+ apply target filter if specified)
        # This is a one-time event. Neither the target image, nor the target
        # filter can be modified later. If they need to be modified, a new
        # Cost object must be created.
        if hasattr(target, "__tfield__"):
            # Verify target mask (save the filtering step if this is wrong)
            target_mask = target.mask
            if target_mask is not None:
                if np.any(~np.isfinite(target_mask)):
                    raise ValueError("Infinite value in target mask.")
                if np.allclose(target_mask, 0):
                    raise ValueError("All values in the target mask are zero.")
                # if np.any(target_mask < 0):
                #     raise ValueError("Negative values found in target mask.")

            # Set target image directly if no filter was specified
            if target_filter is None:
                self._target = target
                self._trg = target

            # Attempt to apply target filter
            elif callable(target_filter):
                try:
                    self._target = target_filter(target)
                except Exception as exc:
                    raise RuntimeError("Target image filtering failed.") \
                        from exc
                else:
                    self._trg = target
            else:
                raise TypeError(f"Target image filter is not callable: "
                                f"{target_filter.__class__.__name__}")
        else:
            raise TypeError("The target image must be TField-like.")

        # Attach source image (+ apply source prefilter if specified)
        # This is a one-time event. Neither the source image, nor the source
        # prefilter cannot be modified later. If they need to be modified, a
        # new Cost object must be created.
        if hasattr(source, "__tfield__"):
            # Verify source mask (save the filtering step if this is wrong)
            source_mask = source.mask
            if source_mask is not None:
                if np.any(~np.isfinite(source_mask)):
                    raise ValueError("Infinite value in source mask.")
                if np.allclose(source_mask, 0):
                    raise ValueError("All values in the source mask are zero.")
                # if np.any(source_mask < 0):
                #     raise ValueError("Negative values found in source mask.")

            # Set source image directly if no prefilter was specified
            if source_prefilter is None:
                self._source = source
                self._src = source

            # Attempt to apply pre-filter
            elif callable(source_prefilter):
                try:
                    self._source = source_prefilter(source)
                except Exception as exc:
                    raise RuntimeError("Source image prefiltering failed.") \
                        from exc
                else:
                    self._src = source
            else:
                raise TypeError(f"Source image prefilter is not callable: "
                                f"{source_prefilter.__class__.__name__}")
        else:
            raise TypeError("The source image must be TField-like.")

        # Create metaparameter dictionary
        self._metaparameters = metaparameters
        self.logger = logger

        # Set source image post-filter
        # The source post-filter cannot be modified later. If it needs
        # modification, a new Cost object must be created.
        if callable(source_postfilter) or (source_postfilter is None):
            self.metaparameters.update(source_postfilter=source_postfilter)
        else:
            raise TypeError(f"Source image post-filter is not callable: "
                            f"{source_postfilter.__class__.__name__}")

        # Set mask combination mode
        if (maskmode in MASKMODES) or callable(maskmode):
            self.metaparameters.update(maskmode=maskmode)
        else:
            raise TypeError(
                f"Unrecognised mask mode. Specify a callable or choose one of "
                f"the following: {MASKMODES}")

        # Set tensor shape matching method
        self.matchmode = matchmode

    @property
    def logger(self):
        return self.metaparameters.get("logger")

    @logger.setter
    def logger(self, lgr):
        if lgr is None:
            self.metaparameters.update(logger=logging.getLogger())
        elif isinstance(lgr, str):
            self.metaparameters.update(logger=logging.getLogger(lgr))
        elif isinstance(lgr, logging.Logger):
            self.metaparameters.update(logger=lgr)
        else:
            raise TypeError(f"Expected logger name of logger instance, "
                            f"got {lgr} instead.")

    def log(self, msg, *args, level=ts.COST_VALUE_LOG_LEVEL, **kwargs):
        """
        This method wraps Logger.log().

        Logs an event to the Logger that has been configured for the optimiser
        object. Uses the level that is specified in COST_VALUE_LOG_LEVEL as the
        default level for the logs.

        """
        self.logger.log(level, msg, *args, **kwargs)

    @property
    def matchmode(self):
        return self.metaparameters.get("matchmode")

    @matchmode.setter
    def matchmode(self, m):
        if (m in MATCHMODES) or callable(m):
            self.metaparameters.update(matchmode=m)
        else:
            raise TypeError(f"Unrecognised tensor matching mode: "
                            f"{m.__class__.__name__}")

    @property
    def source(self):
        """
        Returns the source constituent, which is the pre-filtered, resampled,
        and post-filtered source image on the target(!) domain. This is a
        read-only property.

        """
        spof = self.metaparameters.get("source_postfilter")
        resampled = self._source.evaluate(self.target.domain)
        if spof is None:
            return resampled
        else:
            return spof(resampled)

    @property
    def target(self):
        """
        Returns the target constituent, which is the filtered target image on
        the target domain. This is a read-only property.

        """
        return self._target

    @property
    def metaparameters(self):
        """
        Returns the metaparameter dictionary of the Cost object. Read-only
        property, although the dict is mutable. Only change metaparameters
        after construction if you know what you are doing!

        """
        return self._metaparameters

    def get_constituents(self, force_match=False):
        """
        Returns the target and source images after all preparatory steps:
        target: filtered target TImage
        source: pre-filtered, resampled, post-filtered TImage
        Note that the layout (voxel/tensor major) might be different between
        the two images, and any changes may have an influence on the input
        images as well!

        :returns:
            (target constituent, source constituent)
            The returned target and source constituents are matched for tensor
            shape (unless the matching was disabled by setting the matchmode
            argument to None at the initialisation of the Cost object).
        :rtype: tuple[TImage]

        """
        matchmode = self.matchmode
        target, source = self.target, self.source
        target_shape, source_shape = target.tshape, source.tshape

        # Nothing to do or tensor matching is disabled or tensor shapes are
        # conformant. Override the latter if force_match is True.
        if force_match:
            conformant = False
        else:
            conformant = target_shape == source_shape

        if conformant or (matchmode is None):
            return target, source

        # Built-in method: convert to scalar (L1-norm)
        elif matchmode == "l1_norm":
            normop = TensorOperator(
                np.linalg.norm, opkwargs=dict(axis=-1, ord=1, keepdims=False))
            if source.tshape != ():
                source = normop(source)
            if target.tshape != ():
                target = normop(target)
            return target, source

        # Built-in method: convert to scalar (L2-norm)
        elif matchmode == "l2_norm":
            normop = TensorOperator(
                np.linalg.norm, opkwargs=dict(axis=-1, ord=2, keepdims=False))
            if source.tshape != ():
                source = normop(source)
            if target.tshape != ():
                target = normop(target)
            return target, source

        # Built-in method: broadcast tensors to a common shape
        elif matchmode == "broadcast":
            # Define template
            if len(source_shape) != len(target_shape):
                template = max((source_shape, target_shape), key=len)
            elif source_shape[0] == 1:
                template = target_shape
            elif target_shape[0] == 1:
                template = source_shape
            else:
                raise ArithmeticError(
                    f"The tensors of the source {source_shape} and the "
                    f"target image {target_shape} are not broadcastable.")
            # Create broadcast operator
            broadcast = TensorBroadcast(template)
            # Broadcast
            if template != target_shape:
                target = broadcast(target)
            if template != source_shape:
                source = broadcast(source)
            return target, source

        # User-supplied function
        elif callable(matchmode):
            try:
                target, source = matchmode(self.target, self.source)
            except Exception as exc:
                raise RuntimeError("Tensor shape matching failed.") from exc
            else:
                return target, source

    def __call__(self, *args, **kwargs):
        """
        Calculates the scalar cost by calling the class-specific cost function.

        :return: scalar cost function term (a measure of image dissimilarity)
        :rtype: float

        """
        # Get target and source constituents
        target, source = self.get_constituents()

        # Combine masks
        # TODO: Think about how this could be calculated more sparingly.
        combined_mask = self.combine_masks(target.mask, source.mask)

        # Calculate scalar cost
        cst = float(self.function(target, source, combined_mask))
        self.log(f"Cost ({self.name}): {cst}")
        return cst

    def combine_masks(self, target_mask, source_mask):
        """
        Creates composite mask from the source and target TImage masks. The
        combination algorithm is defined by the 'maskmode' property of the
        Cost object.

        :param target_mask: Target image mask
        :type target_mask: np.ndarray
        :param source_mask: Source image mask (resampled to the target domain)
        :type source_mask: np.ndarray

        :returns:
            Composite mask as TField, or None if neither the source nor the
            target image has a mask.
        :rtype: TField or None

        """
        sm = source_mask is not None
        tm = target_mask is not None

        # Return None if neither of the masks is defined
        if not sm and not tm:
            return None
        # Return target mask if source mask is not defined
        elif tm and not sm:
            return target_mask
        # Return source mask if target mask is not defined
        elif sm and not tm:
            return source_mask

        # Create composite mask if both masks are defined
        else:
            # Enforce the "and" match mode if need to mask missing data. With
            # the "or" method, the mask values may end up 0.5 instead of 0.
            if ts.TIMAGE_MASK_MISSING_DATA:
                mm = "and"
            else:
                mm = self.metaparameters.get("maskmode")

            if mm == "and":
                # Negative values are impossible, hence NaNs are not expected.
                # Wrong: spline interpolation can make values negative!
                newmask = (source_mask * target_mask) ** 0.5
                newmask[np.isnan(newmask)] = 0
                return newmask
            elif mm == "or":
                newmask = ((source_mask ** 2 + target_mask ** 2) / 2) ** 0.5
                newmask[np.isnan(newmask)] = 0
                return newmask
            else:
                raise ValueError(
                    f"Unrecognised mask combination method: '{mm}'. You may "
                    f"specify a callable or choose from the following "
                    f"built-in options: {MASKMODES}")

    def function(self, target, source, combined_mask):
        """
        Implements the cost function, that takes the target and source
        constituents and the combined mask as arguments and returns the scalar
        cost. Subclasses MUST overload this method to provide their
        class-specific implementation.

        The base class implements mean squared differences normalised by the
        number of voxels where the combined mask is non-zero.

        """
        return NotImplementedError()

    def costmap(self):
        """
        Returns an array of voxelwise cost contributions. Only applicable to
        distributive cost functions.

        The costmap is useful to detect the most misaligned areas. The base
        class method returns the difference of the resampled moving (source)
        and the fixed (target) image. Compatible subclasses SHOULD implement
        this method more specifically where appropriate.

        """
        return self.source - self.target


def combine_filters(f1, f2):
    """
    Combines filters f1 and f2: f2(f1(x)). If either input is None, the only
    callable filter is returned. If both inputs are None, None is returned.

    """
    if (f1 is not None) and (f2 is not None):
        return lambda x: f2(f1(x))
    elif f1 is not None:
        return f1
    elif f2 is not None:
        return f2
    else:
        return None


if __name__ == "__main__":  # pragma: no cover
    print("This module is not intended for execution.")
