#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl import settings as ts
from tirl.cost.cost import Cost


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class CostMSD(Cost):
    """
    CostMSD - Mean Squared Differences cost function term

    """
    def __init__(self, source, target, target_filter=None,
                 source_prefilter=None, source_postfilter=None,
                 maskmode="and", matchmode="l2_norm", normalise=True,
                 logger=None, **metaparameters):
        """
        Initialisation of CostMSD.

        :param source:
            Moving image. Values of the source image are interpolated at the
            points of the target domain.
        :type source: TImage
        :param target:
            Fixed image. Values of the source image are interpolated at the
            points of the target domain.
        :type target: TImage
        :param target_filter:
            Filtering that is applied to the target TImage at the construction
            of the Cost object. The target filter must be an Operator,
            otherwise it must be callable with a single input argument for the
            target TImage, and the callable must return a TImage with the
            Domain of the target TImage (not a copy). If None (default), no
            filtering is performed.
        :type target_filter: Operator or Callable or None
        :param source_prefilter:
            Filtering that is applied to the source TImage at the construction
            of the Cost object. The source prefilter must be an Operator,
            otherwise it must be callable with a single input argument for the
            source TImage, and the callable must return a TImage with the
            Domain of the source TImage (not a copy). If None (default), no
            filtering is performed.
        :type source_prefilter: Operator or Callable or None
        :param source_postfilter:
            Filtering that is applied to the source TImage after interpolation,
            before the cost is calculated. The source postfilter must be an
            Operator, otherwise it must be callable with a single input
            argument for the resampled source TImage, and the callable must
            return a TImage with the Domain of the resampled source TImage
            (i.e. the domain of the target TImage; not a copy). If None
            (default), no filtering is performed.
        :type source_postfilter: Operator or Callable or None
        :param maskmode:
            Defines how the masks of the source and the target TImage should be
            combined into one mask. The following methods are supported:
            1. "and": combined mask values are calculated as the geometric mean
                      of corresponding source and target mask values (default).
                      For binary masks, this is equivalent to the AND operation.
            2. "or": combined mask values are calculated as the quadratic mean
                     of corresponding source and target mask values. For binary
                     masks this is equivalent to the OR operation.
            3. callable: function(target_mask, source_mask) -> combined_mask.
        :type maskmode: str or Callable
        :param matchmode:
            If the filtered target image and the prefiltered, resampled and
            post-filtered source image do not have conformant tensor shapes,
            this routine will be applied automatically to create conformable
            shapes. The following modes are available:
            1. None: turned off: raise error if tensors are non-conformant
            2. "broadcast": broadcast tensor shapes according to NumPy rules
            3. "l1_norm" (default): reduce to scalar using the L1-norm
            4. "l2_norm": reduce to scalar using the L2-norm
            5. callable: function(target, source_on_target) -> trg, src_on_trg
        :type matchmode: str or Callable or None
        :param normalise:
            If True (default), the cost is normalised by the number of non-zero
            voxels in the combined mask, or the number of voxels in the target
            image if no mask was set.
        :type normalise: bool
        :param metaparameters:
            Additional keyword arguments.
        :type metaparameters: Any

        """
        # Call superclass initialisation
        super(CostMSD, self).__init__(
            source, target, target_filter=target_filter,
            source_prefilter=source_prefilter,
            source_postfilter=source_postfilter, maskmode=maskmode,
            matchmode=matchmode, logger=logger, **metaparameters)

        # Set class-specific metaparameters
        if isinstance(normalise, bool):
            self.metaparameters.update(normalise=normalise)
        else:
            raise TypeError(f"Expected bool value for normalise option, "
                            f"got {normalise.__class__.__name__}")

    def function(self, target, source, combined_mask):
        """
        Calculates the scalar cost based on squared differences between the
        target and the source constituents. If the normalise option is set
        (default), the cost is normalised by the number of foreground voxels.

        """
        if combined_mask is not None:
            nonzero = np.flatnonzero(combined_mask)
            if not np.any(nonzero):
                raise AssertionError(
                    "All elements of the combined mask are zero.")
        else:
            nonzero = slice(None)

        srcdata = source.dataview(TENSOR_MAJOR).reshape(*source.tshape, -1)
        trgdata = target.dataview(TENSOR_MAJOR).reshape(*target.tshape, -1)
        costs = (srcdata.astype(ts.DEFAULT_FLOAT_TYPE)[..., nonzero] -
                 trgdata.astype(ts.DEFAULT_FLOAT_TYPE)[..., nonzero]) ** 2

        if combined_mask is not None:
            costs = costs * combined_mask.flat[nonzero]
        if self.metaparameters.get("normalise"):
            return np.sum(costs) / nonzero.size
        else:
            return np.sum(costs)

    def jacobian_aos(self, dim=None):
        """
        Returns the voxelwise total derivative of the cost with respect to the
        local displacement vector. The result is a 2D array:
        (voxels, displacement dimension).

        Note that the cost Jacobian is calculated on the voxel grid of the
        target image. As a consequence, to obtain valid parameter Jacobians of
        the total cost, the result of this function must be multiplied by
        transformation jacobians mapped into the voxel space of the target
        image.

        """
        source = self.source  # only evaluate once
        mask = self.combine_masks(self.target.mask, source.mask)
        diff = source - self.target
        diff.apply_mask(mask, normalise=True)
        diff.order = VOXEL_MAJOR
        source.order = VOXEL_MAJOR
        taxes = diff.taxes

        if dim is None:
            dim = source.vaxes
            grad = np.stack(np.gradient(source, axis=dim), axis=-1)
            n = grad.ndim
            jac = np.einsum(diff, [Ellipsis, *taxes], grad,
                            [Ellipsis, *taxes, n])
        elif isinstance(dim, int):
            dim = source.vaxes[dim]
            grad = np.gradient(source, axis=dim)
            jac = np.einsum(diff, [Ellipsis, *taxes], grad, [Ellipsis, *taxes])
        else:
            raise TypeError("Invalid dimension specification.")

        jac /= self.target.tsize
        return jac.reshape((source.domain.numel, -1))

    def dx(self, dim=None, source=None):
        """
        Returns the voxelwise total derivative of the source with respect to the
        local displacement vector. The result has the following shape:
        (voxels, *tensor_shape, gradient_directions).

        Note that the cost Jacobian is calculated on the voxel grid of the
        target image. As a consequence, to obtain valid parameter Jacobians of
        the total cost, the result of this function must be multiplied by
        transformation jacobians mapped into the voxel space of the target
        image.

        """
        if source is None:
            source = self.source  # only evaluate once
        mask = self.combine_masks(self.target.mask, source.mask)
        # diff = source - self.target
        # diff.order = VOXEL_MAJOR
        # source.order = VOXEL_MAJOR
        srcdata = source.dataview(VOXEL_MAJOR)
        # taxes = diff.taxes

        if dim is None:
            dim = tuple(range(source.vdim))
            grad = np.stack(np.gradient(srcdata, axis=dim), axis=-1)
            # Neumann boundary condition
            # for ax in dim:
            #     slicer = (slice(None),) * ax + (0, Ellipsis)
            #     grad[slicer] = 0
            #     slicer = (slice(None),) * ax + (-1, Ellipsis)
            #     grad[slicer] = 0
            grad = grad.reshape((
                source.domain.numel, *source.tshape, len(dim)))
        elif isinstance(dim, int):
            # dim = source.vaxes[dim]
            grad = np.gradient(srcdata, axis=dim)
            # Neumann boundary condition
            # slicer = (slice(None),) * dim + (0, Ellipsis)
            # grad[slicer] = 0
            # slicer = (slice(None),) * dim + (-1, Ellipsis)
            # grad[slicer] = 0
            grad = grad.reshape((source.domain.numel, *source.tshape))

        else:
            raise TypeError("Invalid dimension specification.")

        # Apply mask to the gradients
        if mask is not None:
            mask = mask.ravel()
            grad = (grad.T * (mask / np.max(mask))).T

        return grad
