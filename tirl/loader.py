#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import re
import imageio
import numpy as np
from operator import add
from functools import reduce


# TIRL IMPORTS

from tirl import utils as tu
from tirl import settings as ts
from tirl.tirlobject import TIRLObject

# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class GenericLoader(TIRLObject):

    def __init__(self, storage=None, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(GenericLoader, self).__init__()
        self.dtype = dtype
        self.storage = storage
        self.kwargs = kwargs
        self.cpu = tu.verify_n_cpu(ts.CPU_CORES)
        self.parallelisation = (self.cpu.real > 1) or (self.cpu.imag > 1)

    def __call__(self, f):
        """
        Loads image data from file into array format.

        :param f: file pathology
        :type f: str

        :returns: tuple: image array, image header
        :rtype: tuple[Union[np.ndarray, np.memmap], dict]

        """
        if not os.path.isfile(f):
            raise FileNotFoundError("File not found: {}".format(f))

        # Decide on storage type unless it is already set
        if self.storage is None:
            if os.path.getsize(f) > self.kwargs.pop("memorylimit", np.inf):
                self.storage = HDD
            else:
                self.storage = SHMEM if self.parallelisation else MEM
        else:
            self.kwargs.pop("memorylimit", None)


class ImageIOLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = \
        reduce(add, (f.extensions for f in imageio.formats)) + \
        (".pnm",)

    def __init__(self, method="imread", storage=None, dtype=None, **kwargs):
        super(ImageIOLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)
        self.method = method

    def __call__(self, f):
        super(ImageIOLoader, self).__call__(f)
        img = getattr(imageio, self.method)(f, **self.kwargs)
        try:
            meta = dict(img.meta)
        except AttributeError:
            meta = dict()
        dtype = self.dtype or img.dtype
        if self.storage in (MEM, SHMEM):
            data = np.ascontiguousarray(img, dtype=dtype)
        elif self.storage == HDD:
            fd, fname = self.tmpfile(prefix="imgio_", dir=ts.TWD)
            data = np.memmap(fname, dtype=img.dtype, mode="r+", offset=0,
                             shape=img.shape, order="C")
            data[...] = np.frombuffer(img.data, dtype=img.dtype, count=img.size,
                                      offset=0).reshape(img.shape)
            data.flush()
            import tirl.utils as tu
            import psutil
            memlimit = psutil.virtual_memory().available
            data = tu.change_memmap_dtype(data, dtype, memlimit)
        else:
            raise ValueError("Unknown storage option: {}".format(self.storage))
        hdr = {"meta": meta,
               "input_file": os.path.abspath(f)}

        return data, hdr


class TiffLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".svs", ".tif", ".tiff")

    def __init__(self, series=0, storage=HDD, dtype="uint8", **kwargs):
        super(TiffLoader, self).__init__(storage=storage, dtype=dtype, **kwargs)
        self.pages = kwargs.get("pages", None)
        self.series = series

    def __call__(self, f):
        super(TiffLoader, self).__call__(f)
        import tifffile as tiff
        tfobj = tiff.TiffFile(f)
        from operator import mul
        t = getattr(tfobj, "series")[self.series]
        nbytes = reduce(mul, t.shape) * np.dtype(t.dtype).itemsize
        # hdr = {"meta": tfobj}
        hdr = {}
        if self.storage in (MEM, SHMEM):
            arr = tfobj.asarray(key=self.pages, series=self.series)
            hdr["input_file"] = os.path.abspath(f)
        else:
            fileno, fname = self.tmpfile(prefix="tiff_", dir=ts.TWD)
            hdr["input_file"] = os.path.abspath(fname)
            arr = np.memmap(fname, dtype=t.dtype, mode="r+",
                            offset=0, shape=t.shape, order="C")
            tfobj.asarray(key=self.pages, series=self.series, out=arr,
                          maxworkers=self.cpu or None)
            if np.dtype(t.dtype) != np.dtype(self.dtype):
                import psutil
                memlimit = psutil.virtual_memory().available
                arr = tu.change_memmap_dtype(arr, self.dtype, memlimit)
            arr.flush()

        return arr, hdr


class TiffTileLoader(GenericLoader):
    """
    TiffTileLoader can be used to load select pages from a TIFF file in a
    RAM-efficient way, using parallel processes.

    """

    def __init__(self, pages=None, dtype=None, storage=HDD, **kwargs):
        """
        Initialisation of TiffTileLoader.

        """
        from numbers import Integral
        super(TiffTileLoader, self).__init__(
            dtype=dtype, storage=storage, kwargs=kwargs)
        self.pages = pages

    @property
    def pages(self):
        return self._pages

    @pages.setter
    def pages(self, p):
        from numbers import Integral
        if p is None:
            self._pages = None
        elif isinstance(p, Integral):
            self._pages = (int(p),)
        elif hasattr(p, "__iter__"):
            self._pages = tuple(int(ix) for ix in p)
        else:
            raise TypeError(f"Invalid TIFF page specification: {p}")

    def __call__(self, f):

        import tifffile as tiff
        from tirl.dataobj import DataObject
        arrays = []

        with tiff.TiffFile(f) as tif:

            # If the TIFF has multiple images, find them first
            pages = TiffPageIterator(tif)

            for ix in tuple(self.pages or range(len(pages))):
                page = pages[ix]
                arrshape = page.shape
                dtype = page.dtype if self.dtype is None else self.dtype
                # Create output buffer for the current page
                if self.storage in (MEM, SHMEM):
                    dataobj = DataObject(shape=arrshape, dtype=dtype)
                # elif self.storage == SHMEM:
                #     dataobj = DataObject("shared", shape=arrshape, dtype=dtype)
                elif self.storage == HDD:
                    dataobj = DataObject("memmap", shape=arrshape, dtype=dtype)
                else:
                    raise ValueError(f"Invalid storage mode: {self.storage}")

                jobs = enumerate(zip(page.dataoffsets, page.databytecounts))
                for index, (offset, bytecount) in jobs:
                    if bytecount <= 0:
                        continue
                    fh = tif.filehandle
                    fh.seek(offset)
                    data = fh.read(bytecount)
                    jpeg = page.jpegtables
                    tile, indices, shape = \
                        page.decode(data, index, jpegtables=jpeg)
                    pos_y, pos_x = indices[2:4]
                    height, width = shape[1:3]
                    tile = np.atleast_2d(np.squeeze(tile)).astype(
                        dataobj.value.dtype)
                    shape = dataobj.value.shape
                    stop_y = min(pos_y + height, shape[0])
                    stop_x = min(pos_x + width, shape[1])
                    dataobj.value[pos_y:stop_y, pos_x:stop_x] = \
                        tile[:stop_y - pos_y, :stop_x - pos_x]

                # Destroy shared memory but keep memory-mapped file
                if dataobj.kind == "SharedMemory":
                    arrays.append(dataobj.value.copy())
                    del dataobj
                else:
                    arrays.append(dataobj.value)
                    dataobj.owner = False

        # Pages sometimes encode colour channels, other times resolution levels.
        # If the pages have identical shapes, stack the pages in VOXEL_MAJOR
        # orientation, otherwise just return the tuple of arrays (or a single
        # array if only one page exists).
        if (len(arrays) > 1) and \
                all(arr.shape == arrays[0].shape for arr in arrays):
            arrays = np.stack(arrays, axis=-1)
        else:
            arrays = tuple(arrays) if len(arrays) > 1 else arrays[0]
        return arrays, dict()


def TiffPageIterator(tif):
    if (len(tif.series) > 0) and tif.series[0].is_pyramidal:
        return [lvl.pages[0] for lvl in tif.series[0].levels]
    elif len(tif.pages) > 0:
        return [page for page in tif.pages]
    else:
        raise AssertionError("Unrecognised TIFF format.")


def tifftile_init(f, pix, dataobj):
    global target_buffer
    target_buffer = dataobj
    global filename
    filename = f
    global p_index
    p_index = pix


def tifftile_worker(job):
    import tifffile as tiff
    import multiprocessing as mp
    pid = mp.current_process().pid
    index, (offset, bytecount) = job
    with tiff.TiffFile(filename) as tif:
        page = TiffPageIterator(tif)[p_index]
        fh = tif.filehandle
        fh.seek(offset)
        data = fh.read(bytecount)
        jpeg = page.jpegtables
        tile, indices, shape = page.decode(data, index, jpegtables=jpeg)
        pos_y, pos_x = indices[2:4]
        height, width = shape[1:3]
        tile = np.squeeze(tile).astype(target_buffer.value.dtype)
        shape = target_buffer.value.shape
        stop_y = min(pos_y + height, shape[0])
        stop_x = min(pos_x + width, shape[1])
        target_buffer.value[pos_y:stop_y, pos_x:stop_x] = \
            tile[:stop_y - pos_y, :stop_x - pos_x]


class OpenSlideLoader(GenericLoader):

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, level=-1,
                 **kwargs):
        super(OpenSlideLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)
        self.level = level

    def __call__(self, f):
        super(OpenSlideLoader, self).__call__(f)
        import openslide
        import multiprocessing as mp
        from functools import partial
        import psutil
        memlimit = psutil.virtual_memory().available
        memlimit = min(memlimit, self.kwargs.get("memlimit", memlimit))
        itemsize = 4 * 2 * np.dtype(self.dtype).itemsize
        batchsize = int(np.sqrt(memlimit / itemsize / self.cpu))

        # Load the slide object
        obj = openslide.open_slide(f)
        w, h = obj.level_dimensions[self.level]

        if h * w * itemsize >= memlimit:
            import warnings
            warnings.warn(f"The current memory limit ({memlimit / 1024 ** 2}) "
                          f"is lower than needed to load the object "
                          f"({h * w * itemsize / 1024 ** 2}). Switching to "
                          f"HDD mode.")
            self.storage = HDD

        # Create storage space and populate it with the histology image data
        if self.storage in (MEM, SHMEM):
            arr = np.empty(shape=(h, w, 4), dtype=self.dtype, order="C")
            with mp.pool.ThreadPool(processes=int(self.cpu.real)) as pool:
                jobfunc = partial(openslide_worker_thread, obj=obj,
                                  level=self.level, arr=arr)
                jobs = openslide_generate_jobs(h, w, batchsize)
                pool.map(jobfunc, jobs)

        else:
            fileno, fname = self.tmpfile(prefix="tiff_", dir=ts.TWD)
            arr = np.memmap(fname, dtype=self.dtype, mode="r+", offset=0,
                          shape=(h, w, 4), order="C")
            m = (fname, np.dtype(self.dtype).str, (h, w, 4))
            ctx = mp.get_context(ts.MP_CONTEXT)
            with ctx.Pool(processes=int(self.cpu.real)) as pool:
                lock = mp.Manager().RLock()
                jobfunc = partial(openslide_worker_process, slide=f,
                                  level=self.level, arr=m, lock=lock)
                jobs = openslide_generate_jobs(h, w, batchsize)
                pool.map(jobfunc, jobs)

        hdr = dict(input_file=f)
        # hdr = {
        #     "input_file": f,
        #     "meta": obj
        # }
        return arr, hdr


def openslide_worker_thread(job, obj, level, arr):
    row, endrow, col, endcol = job
    x = int(col * obj.level_downsamples[level])
    y = int(row * obj.level_downsamples[level])
    region = obj.read_region(
        (x, y), level=level, size=(endcol - col, endrow - row))
    region = np.asarray(region, dtype=arr.dtype)
    arr[row:endrow, col:endcol] = region[...]


def openslide_worker_process(job, slide, level, arr, lock):
    from numpy.lib.format import open_memmap
    row, endrow, col, endcol = job
    import openslide
    obj = openslide.open_slide(slide)
    x = int(col * obj.level_downsamples[level])
    y = int(row * obj.level_downsamples[level])
    region = obj.read_region(
        (x, y), level=level, size=(endcol - col, endrow - row))
    lock.acquire()
    fname, dtype, (h, w, c) = arr
    offset = row * w * c * np.dtype(dtype).itemsize
    arr = np.memmap(fname, dtype=dtype, mode="r+", offset=offset,
                    shape=(endrow - row, w, c), order="C")
    arr[:, col:endcol] = np.asarray(region, dtype=arr.dtype)
    del arr
    lock.release()


def openslide_generate_jobs(h, w, batchsize):
    for row in range(0, h, batchsize):
        endrow = min(row + batchsize, h)
        for col in range(0, w, batchsize):
            endcol = min(col + batchsize, w)
            # print(f"Loading ({row}, {col}) - ({endrow}, {endcol})")
            yield row, endrow, col, endcol


class NIfTILoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".nii", ".nii.gz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        affine = kwargs.pop("affine", None)
        super(NIfTILoader, self).__init__(
            storage=storage, dtype=dtype, affine=affine, **kwargs)

    def __call__(self, f):
        super(NIfTILoader, self).__call__(f)

        try:
            import nibabel as nib
        except ImportError:
            raise ImportError(f"{self.__class__.__name__} has a missing "
                              f"dependency: nibabel.")
        mri = nib.load(f)
        hdr = mri.header

        # Comply with the memory limit
        if self.storage in (MEM, SHMEM):
            arr = np.asanyarray(mri.dataobj)

        elif self.storage == HDD:
            if f.lower().endswith(".nii.gz"):
                # Unzip .nii.gz file to the temporary working directory
                import gzip
                with gzip.open(f, mode="rb") as niigz:
                    fd, f = self.tmpfile(
                        dir=ts.TWD, prefix="nifti_", suffix=".nii")
                    with open(f, 'wb') as nii:
                        import shutil
                        shutil.copyfileobj(niigz, nii)
            mri = nib.load(f)
            arr = mri.dataobj.get_unscaled()

        else:
            raise ValueError("Invalid storage mode.")

        # Obtain NIfTI transformation (fsl standard [n] > sform [y] > qform [y])
        if hdr is not None:
            from tirl.transformations.affine import TxAffine
            smat = TxAffine(self.sampling_mat(hdr), name="fsl", lock="all")
            sform = TxAffine(hdr.get_sform()[:3], name="sform", lock="all")
            qform = TxAffine(hdr.get_qform()[:3], name="qform", lock="all")
            affine = \
                TxAffine(hdr.get_best_affine()[:3], name="affine", lock="all")
            txs = dict(
                fsl=smat,
                sform=sform,
                qform=qform,
                affine=affine
            )
        else:
            txs = dict()

        # Generate TField header
        hdr = dict(
            meta=hdr,
            input_file=os.path.abspath(f),
            transformations=txs
        )
        arr = np.asanyarray(arr, order="C")  # ensure C-ordering
        return arr, hdr

    def sampling_mat(self, niftiheader):
        """
        FSL's sampling_mat, which scales voxels by their sizes, and enforces
        radiological orientation.

        """
        xdim, ydim, zdim = niftiheader.get_zooms()[:3]
        xsize, ysize, zsize = niftiheader.get_data_shape()[:3]
        sform = niftiheader.get_best_affine()
        sm = np.diag([xdim, ydim, zdim, 1])
        if np.linalg.det(sform) < 0:
            swapmat = np.diag([-1, 1, 1, 1]).astype(ts.DEFAULT_FLOAT_TYPE)
            swapmat[0, -1] = xsize - 1
            sm = sm @ swapmat
        return sm


class MINCSITKLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".mnc", ".mnc.gz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(MINCSITKLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)

    def __call__(self, f):
        super(MINCSITKLoader, self).__call__(f)

        try:
            import SimpleITK as sitk
        except ImportError:
            raise ImportError(f"{self.__class__.__name__} has a missing "
                              f"dependency: SimpleITK.")
        if self.storage == HDD:
            raise NotImplementedError("HDD storage mode is not supported by "
                                      f"{MINCLoader.__class__.__name__}")

        if f.lower().endswith(".mnc.gz"):
            # Unzip the .mnc.gz file to the temporary working directory
            import gzip
            with gzip.open(f, mode="rb") as mncgz:
                fd, f = self.tmpfile(dir=ts.TWD, prefix="minc_", suffix=".mnc")
                with open(f, 'wb') as mnc:
                    import shutil
                    shutil.copyfileobj(mncgz, mnc)

        vol = sitk.ReadImage(f)
        regex_dims = re.compile(r".{2}", re.MULTILINE | re.DOTALL)
        dim_spec = regex_dims.findall(vol.GetMetaData("dimension_order"))
        dim_spec = [spec[::-1] for spec in dim_spec
                    if spec[-1].lower() in "xyz"]
        dim_order = np.argsort(dim_spec)
        arr = np.atleast_3d(sitk.GetArrayFromImage(vol))
        axes = list(range(arr.ndim))
        axes[:len(dim_spec)] = dim_order
        arr = arr.transpose(axes).astype(self.dtype)
        for ax, spec in zip(dim_order, dim_spec):
            if "-" in spec:
                arr = np.flip(arr, axis=ax)

        # Obtain MINC affine
        from tirl.transformations.affine import TxAffine
        affine = TxAffine(self.get_affine(vol), name="affine", lock="all")
        smat = TxAffine(self.sampling_mat(vol), name="fsl", lock="all")
        txs = dict(
            fsl=smat,
            affine=affine
        )
        # Generate TField header
        hdr = dict(
            meta={k: vol.GetMetaData(k) for k in vol.GetMetaDataKeys()},
            input_file=os.path.abspath(f),
            transformations=txs
        )
        return arr, hdr

    def get_affine(self, sitkImage):
        dim = sitkImage.GetDimension()
        voxelsize = sitkImage.GetSpacing()
        affine = np.diag([*voxelsize, 1]).astype(ts.DEFAULT_FLOAT_TYPE)
        # Assuming that the output is in row-major order
        A = np.asarray(sitkImage.GetDirection(),
                       dtype=affine.dtype).reshape(dim, dim)
        affine[:dim, :dim] *= A
        origin = np.asarray(sitkImage.GetOrigin()).astype(affine.dtype)
        affine[:dim, dim] = origin
        return affine

    def sampling_mat(self, sitkImage):
        """
        FSL's sampling_mat, which scales voxels by their sizes, and enforces
        radiological orientation.

        """
        xdim, ydim, zdim = sitkImage.GetSpacing()
        xsize, ysize, zsize = sitkImage.GetSize()[:3]
        affine = self.get_affine(sitkImage)
        sm = np.diag([xdim, ydim, zdim, 1])
        if np.linalg.det(affine) < 0:
            swapmat = np.diag([-1, 1, 1, 1]).astype(ts.DEFAULT_FLOAT_TYPE)
            swapmat[0, -1] = xsize - 1
            sm = sm @ swapmat
        return sm


class MINCNibabelLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".mnc", ".mnc.gz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(MINCNibabelLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)

    def __call__(self, f):
        super(MINCNibabelLoader, self).__call__(f)

        try:
            import nibabel as nib
        except ImportError:
            raise ImportError(f"{self.__class__.__name__} has a missing "
                              f"dependency: nibabel.")
        # TODO: add support
        if self.storage == HDD:
            raise NotImplementedError(
                "HDD storage mode is not supported by "
                "{NibabelMINCLoader.__class__.__name__}"
            )

        if f.lower().endswith(".mnc.gz"):
            # Unzip the .mnc.gz file to the temporary working directory
            import gzip
            with gzip.open(f, mode="rb") as mncgz:
                fd, f = self.tmpfile(dir=ts.TWD, prefix="minc_", suffix=".mnc")
                with open(f, 'wb') as mnc:
                    import shutil
                    shutil.copyfileobj(mncgz, mnc)

        vol = nib.load(f)
        arr = np.asarray(vol.get_fdata(), dtype=self.dtype)
        hdr = vol.header

        # Obtain MINC affine
        from tirl.transformations.affine import TxAffine
        affine = TxAffine(hdr.get_best_affine(), name="affine", lock="all")
        smat = TxAffine(self.sampling_mat(hdr), name="fsl", lock="all")
        txs = dict(
            fsl=smat,
            affine=affine
        )
        # Generate TField header
        hdr = dict(
            meta=hdr,
            input_file=os.path.abspath(f),
            transformations=txs
        )
        return arr, hdr

    def sampling_mat(self, hdr):
        """
        FSL's sampling_mat, which scales voxels by their sizes, and enforces
        radiological orientation.

        """
        xdim, ydim, zdim = hdr.get_zooms()
        xsize, ysize, zsize = hdr.get_data_shape()[:3]
        affine = hdr.get_best_affine()
        sm = np.diag([xdim, ydim, zdim, 1])
        if np.linalg.det(affine) < 0:
            swapmat = np.diag([-1, 1, 1, 1]).astype(ts.DEFAULT_FLOAT_TYPE)
            swapmat[0, -1] = xsize - 1
            sm = sm @ swapmat
        return sm


class MINCLoader(GenericLoader):
    """
    Universal MINC format loader. This class integrates two different ways that
    differently encoded MINC files (HDF and CDF) can be loaded into Python.

    """
    SUPPORTED_IMAGE_TYPES = (".mnc", ".mnc.gz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(MINCLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)

    def __call__(self, f):
        super(MINCLoader, self).__call__(f)

        # Try first wit the Nibabel loader
        try:
            loader = MINCNibabelLoader(
                storage=self.storage, dtype=self.dtype, **self.kwargs)
            return loader(f)
        except:
            pass

        # Try next with the SimpleITK loader
        try:
            loader = MINCSITKLoader(
                storage=self.storage, dtype=self.dtype, **self.kwargs)
            return loader(f)
        except:
            raise TypeError(
                f"File format is not supported by any of the TIRL "
                f"loaders: {f}")


class NumPyLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".npy", ".npz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(NumPyLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)

    def __call__(self, f):
        super(NumPyLoader, self).__call__(f)
        hdr = {"meta": {}}
        if self.storage in (MEM, SHMEM):
            arr = np.load(f, mmap_mode=None)
            hdr["input_file"] = os.path.abspath(f)
        else:
            arr = np.load(f, mmap_mode="r+")
            hdr["input_file"] = getattr(arr, "filename", os.path.abspath(f))

        return arr, hdr


LOADER_PREFERENCES = {
    '.3fr': ImageIOLoader,
    '.arw': ImageIOLoader,
    '.avi': ImageIOLoader,
    '.bay': ImageIOLoader,
    '.bmp': ImageIOLoader,
    '.bmq': ImageIOLoader,
    '.bsdf': ImageIOLoader,
    '.bufr': ImageIOLoader,
    '.bw': ImageIOLoader,
    '.cap': ImageIOLoader,
    '.cine': ImageIOLoader,
    '.cr2': ImageIOLoader,
    '.crw': ImageIOLoader,
    '.cs1': ImageIOLoader,
    '.ct': ImageIOLoader,
    '.cur': ImageIOLoader,
    '.cut': ImageIOLoader,
    '.dc2': ImageIOLoader,
    '.dcm': ImageIOLoader,
    '.dcr': ImageIOLoader,
    '.dcx': ImageIOLoader,
    '.dds': ImageIOLoader,
    '.dicom': ImageIOLoader,
    '.dng': ImageIOLoader,
    '.drf': ImageIOLoader,
    '.dsc': ImageIOLoader,
    '.ecw': ImageIOLoader,
    '.emf': ImageIOLoader,
    '.eps': ImageIOLoader,
    '.erf': ImageIOLoader,
    '.exr': ImageIOLoader,
    '.fff': ImageIOLoader,
    '.fit': ImageIOLoader,
    '.fits': ImageIOLoader,
    '.flc': ImageIOLoader,
    '.fli': ImageIOLoader,
    '.foobar': ImageIOLoader,
    '.fpx': ImageIOLoader,
    '.ftc': ImageIOLoader,
    '.fts': ImageIOLoader,
    '.ftu': ImageIOLoader,
    '.fz': ImageIOLoader,
    '.g3': ImageIOLoader,
    '.gbr': ImageIOLoader,
    '.gdcm': ImageIOLoader,
    '.gif': ImageIOLoader,
    '.gipl': ImageIOLoader,
    '.grib': ImageIOLoader,
    '.h5': ImageIOLoader,
    '.hdf': ImageIOLoader,
    '.hdf5': ImageIOLoader,
    '.hdp': ImageIOLoader,
    '.hdr': ImageIOLoader,
    '.ia': ImageIOLoader,
    '.icns': ImageIOLoader,
    '.ico': ImageIOLoader,
    '.iff': ImageIOLoader,
    '.iim': ImageIOLoader,
    '.iiq': ImageIOLoader,
    '.im': ImageIOLoader,
    '.img': ImageIOLoader,
    '.ipl': ImageIOLoader,
    '.j2c': ImageIOLoader,
    '.j2k': ImageIOLoader,
    '.jfif': ImageIOLoader,
    '.jif': ImageIOLoader,
    '.jng': ImageIOLoader,
    '.jp2': ImageIOLoader,
    '.jpc': ImageIOLoader,
    '.jpe': ImageIOLoader,
    '.jpeg': ImageIOLoader,
    '.jpf': ImageIOLoader,
    '.jpg': ImageIOLoader,
    '.jpx': ImageIOLoader,
    '.jxr': ImageIOLoader,
    '.k25': ImageIOLoader,
    '.kc2': ImageIOLoader,
    '.kdc': ImageIOLoader,
    '.koa': ImageIOLoader,
    '.lbm': ImageIOLoader,
    '.lfp': ImageIOLoader,
    '.lfr': ImageIOLoader,
    '.lsm': ImageIOLoader,
    '.mdc': ImageIOLoader,
    '.mef': ImageIOLoader,
    '.mgh': ImageIOLoader,
    '.mgz': NIfTILoader,
    '.mha': ImageIOLoader,
    '.mhd': ImageIOLoader,
    '.mic': ImageIOLoader,
    '.mkv': ImageIOLoader,
    '.mnc': MINCLoader,
    '.mnc.gz': MINCLoader,
    '.mnc2': ImageIOLoader,
    '.mos': ImageIOLoader,
    '.mov': ImageIOLoader,
    '.mp4': ImageIOLoader,
    '.mpeg': ImageIOLoader,
    '.mpg': ImageIOLoader,
    '.mpo': ImageIOLoader,
    '.mri': ImageIOLoader,
    '.mrw': ImageIOLoader,
    '.msp': ImageIOLoader,
    '.nef': ImageIOLoader,
    '.nhdr': ImageIOLoader,
    '.nia': ImageIOLoader,
    '.nii': NIfTILoader,
    '.nii.gz': NIfTILoader,
    '.nonexistentext': ImageIOLoader,
    '.npy': NumPyLoader,
    '.npz': NumPyLoader,
    '.nrrd': ImageIOLoader,
    '.nrw': ImageIOLoader,
    '.orf': ImageIOLoader,
    '.pbm': ImageIOLoader,
    '.pcd': ImageIOLoader,
    '.pct': ImageIOLoader,
    '.pcx': ImageIOLoader,
    '.pef': ImageIOLoader,
    '.pfm': ImageIOLoader,
    '.pgm': ImageIOLoader,
    '.pic': ImageIOLoader,
    '.pict': ImageIOLoader,
    '.png': ImageIOLoader,
    '.pnm': ImageIOLoader,
    '.ppm': ImageIOLoader,
    '.ps': ImageIOLoader,
    '.psd': ImageIOLoader,
    '.ptx': ImageIOLoader,
    '.pxn': ImageIOLoader,
    '.pxr': ImageIOLoader,
    '.qtk': ImageIOLoader,
    '.raf': ImageIOLoader,
    '.ras': ImageIOLoader,
    '.raw': ImageIOLoader,
    '.rdc': ImageIOLoader,
    '.rgb': ImageIOLoader,
    '.rgba': ImageIOLoader,
    '.rw2': ImageIOLoader,
    '.rwl': ImageIOLoader,
    '.rwz': ImageIOLoader,
    '.sgi': ImageIOLoader,
    '.spe': ImageIOLoader,
    '.sr2': ImageIOLoader,
    '.srf': ImageIOLoader,
    '.srw': ImageIOLoader,
    '.sti': ImageIOLoader,
    '.stk': ImageIOLoader,
    '.svs': TiffTileLoader,
    '.swf': ImageIOLoader,
    '.targa': ImageIOLoader,
    '.tga': ImageIOLoader,
    '.tif': TiffTileLoader,
    '.tiff': TiffTileLoader,
    '.vtk': ImageIOLoader,
    '.wap': ImageIOLoader,
    '.wbm': ImageIOLoader,
    '.wbmp': ImageIOLoader,
    '.wdp': ImageIOLoader,
    '.webp': ImageIOLoader,
    '.wmf': ImageIOLoader,
    '.wmv': ImageIOLoader,
    '.xbm': ImageIOLoader,
    '.xpm': ImageIOLoader
}
