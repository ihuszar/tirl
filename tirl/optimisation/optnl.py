#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import nlopt
import logging
import numpy as np
from attrdict import AttrMap
from types import SimpleNamespace


# TIRL IMPORTS

from tirl.optimisation.optimiser import Optimiser


# DEFINITIONS

# The listing below is not complete. Feel free to add new items.
BOUNDED_METHODS = (nlopt.LN_BOBYQA, nlopt.LN_NEWUOA_BOUND)


# IMPLEMENTATION

# Optimisation methods (extracted from NLOpt 2.6.1)
OptMethods = SimpleNamespace(
    GN_DIRECT                   = nlopt.GN_DIRECT,
    GN_DIRECT_L                 = nlopt.GN_DIRECT_L,
    GN_DIRECT_L_RAND            = nlopt.GN_DIRECT_L_RAND,
    GN_DIRECT_NOSCAL            = nlopt.GN_DIRECT_NOSCAL,
    GN_DIRECT_L_NOSCAL          = nlopt.GN_DIRECT_L_NOSCAL,
    GN_DIRECT_L_RAND_NOSCAL     = nlopt.GN_DIRECT_L_RAND_NOSCAL,
    GN_ORIG_DIRECT              = nlopt.GN_ORIG_DIRECT,
    GN_ORIG_DIRECT_L            = nlopt.GN_ORIG_DIRECT_L,
    GD_STOGO                    = nlopt.GD_STOGO,
    GD_STOGO_RAND               = nlopt.GD_STOGO_RAND,
    LD_LBFGS_NOCEDAL            = nlopt.LD_LBFGS_NOCEDAL,
    LD_LBFGS                    = nlopt.LD_LBFGS,
    LN_PRAXIS                   = nlopt.LN_PRAXIS,
    LD_VAR1                     = nlopt.LD_VAR1,
    LD_VAR2                     = nlopt.LD_VAR2,
    LD_TNEWTON                  = nlopt.LD_TNEWTON,
    LD_TNEWTON_RESTART          = nlopt.LD_TNEWTON_RESTART,
    LD_TNEWTON_PRECOND          = nlopt.LD_TNEWTON_PRECOND,
    LD_TNEWTON_PRECOND_RESTART  = nlopt.LD_TNEWTON_PRECOND_RESTART,
    GN_CRS2_LM                  = nlopt.GN_CRS2_LM,
    GN_MLSL                     = nlopt.GN_MLSL,
    GD_MLSL                     = nlopt.GD_MLSL,
    GN_MLSL_LDS                 = nlopt.GN_MLSL_LDS,
    GD_MLSL_LDS                 = nlopt.GD_MLSL_LDS,
    LD_MMA                      = nlopt.LD_MMA,
    LN_COBYLA                   = nlopt.LN_COBYLA,
    LN_NEWUOA                   = nlopt.LN_NEWUOA,
    LN_NEWUOA_BOUND             = nlopt.LN_NEWUOA_BOUND,
    LN_NELDERMEAD               = nlopt.LN_NELDERMEAD,
    LN_SBPLX                    = nlopt.LN_SBPLX,
    LN_AUGLAG                   = nlopt.LN_AUGLAG,
    LD_AUGLAG                   = nlopt.LD_AUGLAG,
    LN_AUGLAG_EQ                = nlopt.LN_AUGLAG_EQ,
    LD_AUGLAG_EQ                = nlopt.LD_AUGLAG_EQ,
    LN_BOBYQA                   = nlopt.LN_BOBYQA,
    GN_ISRES                    = nlopt.GN_ISRES,
    AUGLAG                      = nlopt.AUGLAG,
    AUGLAG_EQ                   = nlopt.AUGLAG_EQ,
    G_MLSL                      = nlopt.G_MLSL,
    G_MLSL_LDS                  = nlopt.G_MLSL_LDS,
    LD_SLSQP                    = nlopt.LD_SLSQP,
    LD_CCSAQ                    = nlopt.LD_CCSAQ,
    GN_ESCH                     = nlopt.GN_ESCH
)


class OptNL(Optimiser):
    """
    OptNL - TIRL optimiser that wraps the NLOpt library

    """
    # NLOpt optimisation methods in a namespace
    methods = OptMethods

    def __init__(self, target, *cost_items, method="LN_BOBYQA", mode="min",
                 stopval=-np.inf, ftol_abs=1e-15, ftol_rel=1e-13,
                 xtol_abs=1e-15, xtol_rel=1e-13, maxeval=-1, maxtime=0,
                 step=0.5, normalised=False, logger=None, **metaparameters):
        """
        Initialisation of OptNL.

        :param target:
            Transformation or OptimisationGroup object whose parameters will
            be optimised. During the optimisation, the parameters of this
            object will be updated in place.
        :type target:
            Transformation or OptimisationGroup or Iterable[Transformation]
        :param cost_items:
            One or more Cost or Regulariser objects that together define the
            objective function for the optimisation.
        :type cost_items: Cost or Regulariser
        :param metaparameters:
            Additional keyword argument to the Optimiser object.
        :type metaparameters: Any

        """
        super(OptNL, self).__init__(
            target, *cost_items, logger=logger, **metaparameters)

        # Set method
        if isinstance(method, int):
            pass
        elif isinstance(method, str):
            method = getattr(nlopt, method.upper())
        elif not callable(method):
            raise NameError(f"Unrecognised optimisation method: {method}")
        self.metaparameters.update(method=method)

        # Set optimisation mode
        mode = str(mode).lower()
        if mode in ("min", "max"):
            self.metaparameters.update(mode=mode)

        # Set stopping criteria and step size
        self.metaparameters.update(
            stopval=stopval, ftol_abs=ftol_abs, ftol_rel=ftol_rel,
            xtol_abs=xtol_abs, xtol_rel=xtol_rel, maxeval=maxeval,
            maxtime=maxtime, step=step, normalised=normalised)

        # Create native NLOpt optimiser instance
        self.opt = self.create_optimiser()

    @property
    def normalised(self):
        return self.metaparameters.get("normalised")

    @normalised.setter
    def normalised(self, n):
        if isinstance(n, bool):
            self.metaparameters.update(normalised=n)
        else:
            raise TypeError(f"Expected boolean for 'normalised' option, "
                            f"got {n} instead.")

    def create_optimiser(self):
        """
        Creates the native equivalent object from the NLOpt library.

        """
        p = AttrMap(self.metaparameters)

        # Create optimiser instance
        opt = nlopt.opt(p.method, self.transformation.parameters.size)

        # Set mode
        if p.mode == "min":
            opt.set_min_objective(self.measure_cost)
        else:
            opt.set_max_objective(self.measure_cost)

        # Set bounds
        if self.normalised:
            opt.set_lower_bounds(-np.ones(self.transformation.parameters.size))
            opt.set_upper_bounds(np.ones(self.transformation.parameters.size))
        else:
            opt.set_lower_bounds(
                self.transformation.parameters.get_lower_bounds())
            opt.set_upper_bounds(
                self.transformation.parameters.get_upper_bounds())

        # Set the metaparameters of the optimiser

        # Stopping based on function values
        if p.stopval is not None:
            opt.set_stopval(p.stopval)
        if p.ftol_abs is not None:
            opt.set_ftol_abs(p.ftol_abs)
        if p.ftol_rel is not None:
            opt.set_ftol_rel(p.ftol_rel)

        # Stopping based on parameter values
        if p.xtol_abs is not None:
            # The absolute tolerance can be set separately for the parameters.
            p.xtol_abs = np.atleast_1d(p.xtol_abs)
            n = self.transformation.parameters.size
            if (p.xtol_abs.size == 1) and (n != 1):
                p.xtol_abs = np.repeat(p.xtol_abs, n)
            opt.set_xtol_abs(p.xtol_abs)
        if p.xtol_rel is not None:
            # The relative tolerance can only be set collectively.
            opt.set_xtol_rel(p.xtol_rel)

        # Stopping based on iterations
        if p.maxeval is not None:
            opt.set_maxeval(p.maxeval)
        if p.maxtime is not None:
            opt.set_maxtime(p.maxtime)

        # Set initial step for optimisation
        # The step size can be set individually for each parameter.
        p.step = np.atleast_1d(p.step)
        n = self.transformation.parameters.size
        if (p.step.size == 1) and (n != 1):
            p.step = np.repeat(p.step, n)
        opt.set_initial_step(p.step)

        return opt

    def measure_cost(self, x, grad=None):
        if self.normalised:
            self.log(self.revert_parameters(x))
            return self.measure_cost_normalised(x, grad)
        else:
            self.log(x)
            return self.measure_cost_absolute(x, grad)

    def measure_cost_absolute(self, x, grad=None):
        """
        Sets the parameters and returns the current total cost. This method
        constitutes an indirection from the total_cost method, and is needed
        for compatibility with the NLOpt optimiser class.

        """
        self.transformation.set(x)
        if self.metaparameters.get("visualise", False):
            from tirl import tirlvision
            tirlvision.call("optnl", "showimg", self)
        self.log(f"Current parameters: {x}")
        if (grad is not None) and (grad.size > 0):
            grad[:] = self.approx_dcdx(x)
            self.log(f"Current total gradient: {grad}")
        costval = self.total_cost()
        self.log(f"Current total cost: {costval}")
        return costval

    def measure_cost_normalised(self, x, grad=None):
        """
        Sets the parameters and returns the current total cost. This method
        constitutes an indirection from the total_cost method, and is needed
        for compatibility with the NLOpt optimiser class. Rescales parameter
        values to [-1, 1] based on the parameter bounds.

        """
        self.transformation.set(self.revert_parameters(x))
        if self.metaparameters.get("visualise", False):
            from tirl import tirlvision
            tirlvision.call("optnl", "showimg", self)
        if (grad is not None) and (grad.size > 0):
            grad[...] = self.transform_gradient(self.approx_dcdx(x))
            self.log(f"Current total gradient: {grad}")
        costval = self.total_cost()
        self.log(f"Current total cost: {costval}")
        return costval

    def approx_dcdx(self, x):
        from scipy.optimize.optimize import approx_fprime
        return approx_fprime(
            x, self.measure_cost, [0.01, 0.01, 0.01, 0.01, 1, 1])

    def __call__(self):
        """
        Performs optimisation by calling the respective method from the NLOpt
        library.

        """
        x0 = self.transformation.get()
        self._safe_bounds = self.safe_bounds()
        if self.normalised:
            x0 = self.transform_parameters(x0)
        x_opt = self.opt.optimize(x0)
        if self.normalised:
            x_opt = self.revert_parameters(x_opt)
        self.transformation.set(x_opt)

        # Report
        retval = self.opt.last_optimize_result()

        # Successful outcomes
        if retval == nlopt.SUCCESS:
            self.log("Optimisation succeeded.")
        elif retval == nlopt.STOPVAL_REACHED:
            self.log("Optimisation succeeded: stopval was reached.")
        elif retval == nlopt.FTOL_REACHED:
            self.log("Optimisation succeeded: "
                     "ftol_rel or ftol_abs was reached.")
        elif retval == nlopt.XTOL_REACHED:
            self.log("Optimisation succeeded: "
                     "xtol_rel or xtol_abs was reached.")
        elif retval == nlopt.MAXEVAL_REACHED:
            self.log("Optimisation succeeded: maxeval was reached.")
        elif retval == nlopt.MAXTIME_REACHED:
            self.log("Optimisation succeeded: maxtime was reached.")

        # Failures: report at an elevated log level (logging.ERROR)
        elif retval == nlopt.FAILURE:
            self.log("Optimisation failed.", level=logging.ERROR)
        elif retval == nlopt.INVALID_ARGS:
            self.log("Optimisation failed. There was problem with the "
                     "optimisation arguments. Lower bounds were bigger than "
                     "upper bounds, or an unknown algorithm was specified.",
                     level=logging.ERROR)
        elif retval == nlopt.OUT_OF_MEMORY:
            self.log("Optimisation failed. Out of memory.",
                     level=logging.ERROR)
        elif retval == nlopt.ROUNDOFF_LIMITED:
            self.log("Optimisation failed. Halted because rounding errors "
                     "limited progress. (In this case, the optimization still "
                     "typically returns a useful result.)",
                     level=logging.ERROR)
        elif retval == nlopt.FORCED_STOP:
            self.log("Optimisation failed. Forced termination.",
                     level=logging.ERROR)

        # Also report optimum parameters upon completion.
        if self.opt.last_optimize_result() > 0:
            self.log(f"Final parameters: {x_opt}")

    # PARAMETER NORMALISATION METHODS

    def safe_bounds(self):
        """
        Obtain safe normalisation range, free from NaNs and Infs. Non-finite
        values are replaced by [90%; 110%] brackets, zeros are replaced with
        [-1; 1].

        """
        # These rules were taken from a previous version of TIRL, that used to
        # initialise default parameter bounds by these rules. This was later
        # replaced by infinite values to avoid confusion. Since parameter
        # normalisation is a feature of the OptNL object, these rules are now
        # implemented locally.
        ub = self.upper_bounds()
        uinf = ~np.isfinite(ub)
        if np.any(uinf):
            current = self.transformation.parameters[:]
            dub = current + np.abs(current) * 0.1
            dub = np.where(np.isclose(dub, 0) & np.isclose(current, 0), 1, dub)
            ub = np.where(uinf, dub, ub)

        lb = self.lower_bounds()
        linf = ~np.isfinite(lb)
        if np.any(linf):
            current = self.transformation.parameters[:]
            dlb = current - np.abs(current) * 0.1
            dlb = np.where(np.isclose(dlb, 0) & np.isclose(current, 0), -1, dlb)
            lb = np.where(linf, dlb, lb)

        return lb, ub

    def lower_bounds(self):
        if hasattr(self.transformation, "__tx_priority__"):
            return self.transformation.parameters.get_lower_bounds()
        elif hasattr(self.transformation, "get_lower_bounds"):
            return getattr(self.transformation, "get_lower_bounds")()

    def upper_bounds(self):
        if hasattr(self.transformation, "__tx_priority__"):
            return self.transformation.parameters.get_upper_bounds()
        elif hasattr(self.transformation, "get_upper_bounds"):
            return getattr(self.transformation, "get_upper_bounds")()

    def transform_parameters(self, parameters):
        """
        If both lower and upper bounds are properly defined, this method
        rescales the parameters given in the arguments to the [-1, 1] range,
        the endpoints of which correspond to the lower and upper bounds,
        respectively.

        :param parameters: native parameter values
        :type parameters: np.ndarray

        :returns: transformed parameters (optimiser parameters)
        :rtype: np.ndarray

        """
        lb, ub = self._safe_bounds
        di = ub - lb
        # if np.any(di <= np.finfo(di.dtype).eps):
        #     raise ValueError("Parameter bounds leave no space for "
        #                      "optimisation.")
        su = ub + lb
        return (2 * parameters - su) / di

    def transform_gradient(self, gradient):
        """
        Transforms the cost gradient to the optimisation parameter space.

        :param gradient:
        :return:

        """
        lb, ub = self._safe_bounds
        di = ub - lb
        if np.any(di <= 0):
            raise ValueError("Parameter bounds leave no space for "
                             "optimisation.")
        return gradient * di / 2.

    def revert_parameters(self, parameters):
        """
        If both lower and upper bounds are properly defined, this method
        rescales the optimisation parameters given in the arguments from the
        [-1, 1] range to their native values.

        :param parameters: optimisation parameter values
        :type parameters: np.ndarray

        :returns: native parameters
        :rtype: np.ndarray

        """
        lb, ub = self._safe_bounds
        di = ub - lb
        if np.any(di <= 0):
            raise ValueError("Parameter bounds leave no space for "
                             "optimisation.")
        su = ub + lb
        return (di * parameters + su) / 2.
