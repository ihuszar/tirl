#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# PACKAGE IMPORTS

# from tirl import expose_package_contents
#
# # Expose all interpolator objects at the module level
# # Note: this routine imports the Interpolator base class, and every
# # subclass thereof from all modules within the "interpolation" package.
#
# expose_package_contents(
#     baseclass=Interpolator, pkg="tirl.interpolation", pathology=__path__,
#     globals=globals())
