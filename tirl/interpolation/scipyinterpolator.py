#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl import settings as ts
from tirl.interpolation.interpolator import Interpolator


# IMPLEMENTATION

class ScipyInterpolator(Interpolator):
    """
    ScipyInterpolator class - wrapper around Scipy.ndimage.map_coordinates

    """
    default_options = dict(
        cval=ts.FILL_VALUE,
        mode="constant",
        order=3,
        prefilter=True,
        hold=False
    )

    def __init__(self, *args, **kwargs):
        super(ScipyInterpolator, self).__init__(*args, **kwargs)
        self.default_options.update(ScipyInterpolator.default_options)
        self.set_attributes(**kwargs)

    def interpolate(self, coordinates):
        """
        Wraps scipy.ndimage.interpolation.map_coordinates.

        """
        from scipy.ndimage.interpolation import map_coordinates
        return map_coordinates(
            self.data, coordinates.T, output=None, order=self.order,
            mode=self.mode, cval=self.fill_value, prefilter=self.prefilter
        )

    @property
    def mode(self):
        return self.kwargs.get("mode")

    @mode.setter
    def mode(self, m):
        self.kwargs.update(mode=m)

    @property
    def order(self):
        return self.kwargs.get("order")

    @order.setter
    def order(self, value):
        value = int(value)
        assert value > 0, "Spline order must be greater than or equal to 1."
        self.kwargs.update(order=value)

    @property
    def prefilter(self):
        return self.kwargs.get("prefilter")

    @prefilter.setter
    def prefilter(self, pf):
        self.kwargs.update(prefilter=bool(pf))


class ScipyRegularGridInterpolator(Interpolator):

    default_options = dict(
        bounds_error=False,
        fill_value=ts.FILL_VALUE,
        method="linear",
        hold=False
    )

    def __init__(self, *args, **kwargs):
        """
        Initialisation of ScipyRegularGridInterpolator.

        """
        super(ScipyRegularGridInterpolator, self).__init__(*args, **kwargs)
        self.default_options.update(
            ScipyRegularGridInterpolator.default_options)
        self.set_attributes(**kwargs)

    def interpolate(self, coordinates):
        """
        Wraps scipy.interpolate.RegularGridInterpolator.

        """
        from scipy.interpolate import RegularGridInterpolator
        grid = tuple(np.arange(dim) for dim in self.data.shape)
        ip = RegularGridInterpolator(
            points=grid, values=self.data, method=self.method,
            bounds_error=self.bounds_error, fill_value=self.fill_value
        )
        return ip(coordinates)

    @property
    def bounds_error(self):
        return self.kwargs.get("bounds_error")

    @bounds_error.setter
    def bounds_error(self, be):
        self.kwargs.update(bounds_error=bool(be))

    @property
    def method(self):
        return self.kwargs.get("method")

    @method.setter
    def method(self, m):
        self.kwargs.update(method=m)


class ScipyNearestNeighbours(Interpolator):

    default_options = dict(
        rescale=False,
        tree_options=None,
        hold=False
    )

    def __init__(self, *args, **kwargs):
        """
        Initialisation of ScipyNearestNeighbour.

        """
        super(ScipyNearestNeighbours, self).__init__(*args, **kwargs)
        self.default_options.update(ScipyNearestNeighbours.default_options)
        self.set_attributes(**kwargs)

    def interpolate(self, coordinates):
        """
        Wraps scipy.interpolate.NearestNDInterpolator

        """
        from scipy.interpolate import NearestNDInterpolator
        vcoords = np.stack(
            np.meshgrid(*tuple(range(dim) for dim in self.data.shape),
                        indexing="ij"), axis=-1)
        ip = NearestNDInterpolator(
            x=vcoords.reshape((-1, self.data.ndim)),
            y=self.data.ravel(), rescale=self.rescale,
            tree_options=self.tree_options
        )
        return ip(coordinates)

    @property
    def rescale(self):
        return self.kwargs.get("rescale")

    @rescale.setter
    def rescale(self, r):
        self.kwargs.update(rescale=r)

    @property
    def tree_options(self):
        return self.kwargs.get("tree_options")

    @tree_options.setter
    def tree_options(self, topt):
        self.kwargs.update(tree_options=topt)
