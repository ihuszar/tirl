#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES


# TIRL IMPORTS


# IMPLEMENTATION

class Signature(list):

    """
    Signature implements the unique fingerprint of a TIRL object. The signature
    consists of multiple levels that are increasingly more expensive to
    compute. When called, the object returns the hash codes corresponding to a
    chosen level, or the hash codes for all levels if a level is not specified.

    """
    def __init__(self, *contents):
        super(Signature, self).__init__(contents)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        if len(self) != len(other):
            return False
        for slevel, olevel in zip(self, other):
            slevel = slevel() if callable(slevel) else slevel
            olevel = olevel() if callable(olevel) else olevel
            if slevel != olevel:
                return False
        else:
            return True

    def __call__(self, *levels):
        acc = []
        levels = list(self) if not levels else [self[ix] for ix in levels]
        if len(levels) == 0:
            return None
        for i, level in enumerate(levels):
            if isinstance(level, str):
                acc.append(level)
            elif callable(level):
                while callable(level):
                    level = level()
                if isinstance(level, str):
                    acc.append(level)
                elif hasattr(level, "__iter__"):
                    acc.extend(level)
                else:
                    acc.append(level)
        else:
            return acc if len(acc) else acc[0]
