#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _       __      __  _         _
#  |__   __| |_   _| |  __ \  | |      \ \    / / (_)       (_)
#     | |      | |   | |__) | | |       \ \  / /   _   ___   _    ___    _ __
#     | |      | |   |  _  /  | |        \ \/ /   | | / __| | |  / _ \  | '_ \
#     | |     _| |_  | | \ \  | |____     \  /    | | \__ \ | | | (_) | | | | |
#     |_|    |_____| |_|  \_\ |______|     \/     |_| |___/ |_|  \___/  |_| |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES


# TIRL IMPORTS

from tirl import settings as ts

# Safe import Matplotlib
if ts.ENABLE_VISUALISATION:
    import os
    import matplotlib
    if os.getenv("DISPLAY"):
        matplotlib.use(ts.MPL_BACKEND)
    else:
        matplotlib.use("agg")
    import matplotlib.pyplot as plt
else:
    import warnings
    warnings.warn("Visualisations are disabled in tirlconfig. "
                  "Matplotlib was not loaded.", ImportWarning)


# IMPLEMENTATION

def create_object_figure(obj):
    try:
        return obj.figure
    except AttributeError:
        obj.figure = plt.figure()
        return obj.figure


def showimg(obj):
    fig = create_object_figure(obj)
    ax = fig.add_subplot(111)
    costobj = obj.costs[0]
    target = costobj.target / costobj.target.max()
    target.tensors.reduce()
    source = costobj.source.evaluate(target.domain)
    source = source / source.max()
    source.tensors.reduce()
    if target.vdim == 2:
        diff = (source - target).tensors[0].data
        # diff = rescale_intensity(diff.data, out_range=(0, 1))
        ax.imshow(diff, cmap="gray")
    plt.pause(0.01)
    fig.show()
