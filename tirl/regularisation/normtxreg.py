#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Real


# TIRL IMPORTS

from tirl.regularisation.txregulariser import TransformationRegulariser
from tirl.transformations.transformation import Transformation


# IMPLEMENTATION

class NormTxRegulariser(TransformationRegulariser):
    """
    NormTxRegulariser - calculates regularisation term from the norm of
    transformation parameters.

    """
    RESERVED_KWARGS = ("transformation", "order", "weight", "logger",
                       "metaparameters")

    def __init__(self, transformation, order=2, weight=1.0, logger=None,
                 **metaparameters):
        """
        Initialisation of NormTxRegulariser.

        :param transformation:
            Transformation object whose parameters are used to compute to the
            regularisation term.
        :type transformation: Transformation or OptimisationGroup
        :param weight: Regularisation weight.
        :type weight: Real
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger
        :param kwargs:
            Additional keyword arguments required to compute the
            regularisation term.
        :type kwargs: Any

        """
        # Call superclass initialisation
        super(NormTxRegulariser, self).__init__(
            transformation, weight=weight, logger=logger, **metaparameters)

        # Set class-specific metaparameters
        self.order = order

    @property
    def order(self):
        return self.metaparameters.get("order")

    @order.setter
    def order(self, r):
        if isinstance(r, Real):
            self.metaparameters.update(order=float(r))
        else:
            raise TypeError(f"Expected real number for norm oder, "
                            f"got {r.__class__.__name__}")

    def function(self):
        """
        Returns p-norm of the transformation parameters.

        """
        p = self.order
        res = np.sum(np.abs(self.transformation.parameters[:]) ** p) ** (1. / p)
        res /= self.transformation.parameters.size
        return res

    def grad1(self):
        """
        Analytical Jacobian of the p-norm.

        """
        p = self.order
        x = self.parameters
        pnorm = self.function()
        jac = x * np.abs(x) ** (p - 2) / (pnorm ** (p - 1))
        return jac

    def grad2(self):
        """
        Analytical Hessian of the p-norm.

        """
        p = self.order
        n = self.parameters.size
        if p == 1:
            # Avoid calculations and rounding errors: Hessian is 0 for L1-norm
            hess = np.zeros((n, n))
        else:
            pnorm = self.function()
            x = self.parameters.reshape((-1, 1))
            xx = x * np.abs(x) ** (p - 2)
            hess = (1 - p) * pnorm ** (1 - 2 * p) * xx @ xx.T \
                   + (p - 1) * np.diag(x) ** (p - 2) / (pnorm ** (p - 1))
        return hess


if __name__ == "__main__":
    print("""This module is not intended for execution.""")
