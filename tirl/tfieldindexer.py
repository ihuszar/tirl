#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl.domain import Domain
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.translation import TxTranslation


# DEFINITIONS

from tirl.constants import *


# DEVELOPMENT NOTES


# IMPLEMENTATION

# noinspection SpellCheckingInspection
class TFieldIndexer(object):
    """
    TFieldIndexer - provides ndarray-style slicing for TField-like instances.

    """

    def __init__(self, tfield, axes, context=None):
        if hasattr(tfield, "__tfield__"):
            self.tfield = tfield
        else:
            raise TypeError("TFieldIndexer: Invalid input for TField.")

        # Verify axes input
        if not hasattr(axes, "__iter__"):
            if not isinstance(axes, (int, np.integer)):
                raise TypeError("TFieldIndexer: Invalid axis specification.")
            else:
                if axes < self.tfield.ndim:
                    axes = (axes,)
        else:
            if not all(isinstance(ax, (int, np.integer)) for ax in axes):
                raise IndexError("TFieldIndexer: Invalid axis specification.")
            else:
                if all(ax < self.tfield.ndim for ax in axes):
                    axes = tuple(axes)
                else:
                    raise ValueError("TFieldIndexer: Axis index out of bounds.")
        # Handle negative input
        axes = tuple(ax % self.tfield.ndim for ax in axes)

        # Set visible and hidden axes
        self.visible = np.asarray(
            tuple(filter(lambda ax: ax not in axes, range(self.tfield.ndim))))
        self.hidden = np.asarray(axes)

        # Initialise alternative slicer that will replace the user input
        self.slicer = np.asarray([slice(None)] * self.tfield.ndim)

        # Specify slicing context
        self.context = context

    def __getitem__(self, item):
        # Return the original object if no axis can be indexed.
        if self.visible.size == 0:
            if item == 0:
                return self.tfield
            else:
                raise IndexError("Indexing scalar values is undefined for "
                                 "indices other than 0.")
        # Consider other cases

        # Multi-indexing by a tuple
        if isinstance(item, tuple):
            for i in range(self.slicer.flat[self.visible].size):
                # self.slicer.flat[self.visible] = np.asarray(item)
                if hasattr(item[i], "__iter__"):
                    self.slicer[self.visible[i]] = np.asarray(item[i])
                else:
                    self.slicer[self.visible[i]] = item[i]

        # Subarray indexing: fancy indexing using boolean subarray
        elif isinstance(item, np.ndarray) \
                and np.issubdtype(item.dtype, np.bool_):
            inshape = np.asarray(self.tfield.shape)[self.visible]
            if self.hidden.size > 0:
                outshape = np.asarray(self.tfield.shape)[self.hidden]
            else:
                outshape = (1,)
            if item.size == np.prod(inshape):
                if self.visible[0] == 0:
                    flatshape = (-1, *outshape)
                    ret = self.tfield.data.reshape(flatshape)[item.ravel()]
                else:
                    flatshape = (*outshape, -1)
                    ret = self.tfield.data.reshape(flatshape)[..., item.ravel()]
                context = (self.context, item)
                return self.__finish__(ret, item, context=context)

        # Fancy indexing by integers
        elif isinstance(item, np.ndarray) \
                and np.issubdtype(item.dtype, np.integer):
            self.slicer[self.visible[0]] = item

        elif hasattr(item, "__tfield__"):
            mask = item.__array__() != 0
            data = np.moveaxis(
                self.tfield.data, self.visible, range(len(self.visible)))
            data = data.reshape(
                (-1, *np.asarray(self.tfield.shape)[self.hidden]))
            return data[mask.ravel(), :]  # ndarray

        # Single-axis indexing
        else:
            if self.visible.size > 0:
                self.slicer[self.visible[0]] = item

        # Execute alternative slicing and return values in correct format
        ret = self.tfield.data[tuple(self.slicer)]
        context = (self.context, item)
        try:
            ret = self.__finish__(ret, item, context=context)
        except Exception:
            pass
        return ret

    def __setitem__(self, key, value):
        # Raise IndexError if scalars are being indexed.
        if self.visible.size == 0:
            if key != 0:
                raise IndexError("Indexing scalar values is undefined for "
                                 "indices other than 0.")

        # Consider other cases.

        # Multi-indexing by a tuple
        if isinstance(key, tuple):
            self.slicer.flat[self.visible] = np.asarray(key)

        # Subarray indexing: fancy indexing using boolean subarray
        elif isinstance(key, np.ndarray) \
                and np.issubdtype(key.dtype, np.bool_):
            inshape = np.asarray(self.tfield.shape)[self.visible]
            outshape = np.asarray(self.tfield.shape)[self.hidden]
            if key.size == np.prod(inshape):
                if self.visible[0] == 0:
                    flatshape = (-1, *outshape)
                    self.tfield.data.reshape(flatshape)[key.ravel()] = value
                else:
                    flatshape = (*outshape, -1)
                    self.tfield.data.reshape(flatshape)[..., key.ravel()] \
                        = value

        # Single-axis indexing
        else:
            if self.visible.size > 0:
                self.slicer[self.visible[0]] = key

        # If the value itself has tensor axes, match the order with the target
        # of the assignment operation.
        if hasattr(value, "__tfield__"):
            value_order = value.order
            value.order = self.tfield.order

        # Execute alternative slicing and assign values
        self.tfield.data[tuple(self.slicer)] = value

        # Revert the order of the value (if relevant)
        if hasattr(value, "__tfield__"):
            value.order = value_order

    def __finish__(self, arr, item, context=None):
        """
        Inspects the result of slicing, and returns the result as a specialised
        TIRL object (TField and subclasses) if possible, otherwise returns the
        result as an ndarray.

        :param arr: result of slicing
        :type arr: np.ndarray
        :param item: slicing specification
        :type item: Union[tuple, slice, int, np.integer, Ellipse]
        :param context: context information (for post-hoc type casting)
        :type context: Any

        :returns: result of slicing
        :rtype: Union[TField, np.ndarray, np.memmap]

        """
        if not isinstance(item, tuple):
            item = (item,)
        offset = []
        discontinuous = False
        inversion = False
        reducing = []
        for i, sl in enumerate(item):
            # If the current slice is along a tensor axis...
            if self.visible[i] in self.tfield.taxes:
                if isinstance(sl, (int, np.integer)):
                    reducing.append(self.visible[i])
                elif isinstance(sl, slice):
                    start, stop, step = \
                        sl.indices(self.tfield.shape[self.visible[i]])
                    if stop == start:
                        raise ValueError("0-slice is not supported.")
                    # if stop - start == 1:
                    #     reducing.append(self.visible[i])

            # If the current slice is along a voxel axis...
            elif self.visible[i] in self.tfield.vaxes:
                if isinstance(sl, (int, np.integer)):
                    offset.append(sl)
                    reducing.append(self.visible[i])
                elif isinstance(sl, slice):
                    start, stop, step = \
                        sl.indices(self.tfield.shape[self.visible[i]])
                    if step == -1:
                        inversion = True
                        break
                    elif step != 1:
                        discontinuous = True
                        break
                    else:
                        offset.append(start)
                        if stop == start:
                            raise ValueError("0-slice is not supported.")
                        # if stop - start == 1:
                        #     reducing.append(self.visible[i])
                elif isinstance(sl, type(Ellipsis)):
                    offset.append(Ellipsis)
                else:
                    discontinuous = True
                    break

        # print("Inversion:", inversion)
        # print("Discontinuous:", discontinuous)
        # print("Offset:", offset)
        # print("Reducing:", reducing)

        if inversion:
            raise IndexError("TField axis inversion by slicing is not "
                             "supported in the current version.")

        if discontinuous:
            return arr

        else:
            # Replace Ellipsis in offset
            try:
                pos = offset.index(Ellipsis)
            except ValueError:
                pos = len(offset)
                n = self.tfield.vdim - len(offset)
            else:
                offset.pop(pos)
                n = self.tfield.vdim - len(offset) + 1

            # Obtain offset for unrepresented spatial dimensions
            offset = offset[:pos] + [0] * n + offset[pos:]
            # print("Updated offset:", offset)

            # Infer remaining tensor axes and voxel axes
            new_axes = tuple(ax for ax in range(self.tfield.ndim)
                             if ax not in reducing)
            new_taxes = tuple(i for i, ax in enumerate(new_axes)
                              if ax in self.tfield.taxes)
            # print("New tensor axes:", new_taxes)
            new_tshape = tuple(arr.shape[ax] for ax in new_taxes)
            new_vaxes = tuple(i for i, ax in enumerate(new_axes)
                              if ax in self.tfield.vaxes)
            new_vshape = tuple(arr.shape[ax] for ax in new_vaxes)

            # If the spatial domain was reduced to a scalar, keep it as a
            # singleton dimension.
            if not new_vshape:
                new_vshape = (1,)
                if self.tfield.order == TENSOR_MAJOR:
                    arr = arr[..., np.newaxis]
                    # new_vaxes = (self.tfield.tdim,)
                    new_taxes = tuple(range(self.tfield.tdim))
                else:
                    arr = arr[np.newaxis, ...]
                    # new_vaxes = (0,)
                    new_taxes = tuple(range(1, self.tfield.tdim + 1))

            # Create compensatory translation if the offset is non-zero.
            od = self.tfield.domain
            otxs = list(od.offset)  # note: no copy iterator!
            if any(val != 0 for val in offset):
                otxs = [TxTranslation(*offset)] + otxs

            # Create compensatory embedding if the slicing was reducing.
            # Note that the TxEmbed transformation must precede the
            # translation offsets for the latter to be effective.
            dim_gap = od.ndim - len(new_vshape)
            if dim_gap:
                otxs = [TxEmbed(len(new_vshape), od.ndim)] + otxs

            # Create new Domain with the original transformations
            # (no copy iterator is used)
            new_domain = Domain(
                new_vshape, transformations=od.chain, offset=otxs, name=None,
                storage=od.storage, memlimit=od.memlimit)

            # Create new interpolator
            interpolator = self.tfield.interpolator.copy()
            interpolator.data = arr
            # interpolator.tensor_axes = new_taxes

            # Create new TField instance
            name = self.tfield.name + "_slice"
            tf = type(self.tfield)
            ret = tf(arr, domain=new_domain, tshape=new_tshape,
                     order=self.tfield.order, dtype=self.tfield.dtype,
                     interpolator=interpolator, name=name,
                     header=self.tfield.header, evmgr=self.tfield.evmgr,
                     rule=self.tfield.rule)
            # Allow post-hoc type casting by caller object
            if hasattr(self.tfield, "__slice_wrap__"):
                ret = self.tfield.__slice_wrap__(ret, context=context)
            return ret

    def _axes_prepare(self, axis):
        if axis:
            axis = (axis,) if not hasattr(axis, "__iter__") else axis
            n_axes = self.hidden.size
            offset = self.hidden[0]
            axes = []
            for ax in axis:
                assert ax < n_axes or ax >= -n_axes, \
                    "Axis index out of range (0-{}).".format(n_axes - 1)
                ax = offset + ax % n_axes if ax < 0 else offset + ax
                axes.append(ax)
            return tuple(axes)
        else:
            return tuple(self.hidden)

    def abs(self, *args, **kwargs):
        # Result is an object of identical type
        return self.tfield.abs(*args, **kwargs)

    # def argmin(self, *args, **kwargs):
    #     # Reducing operation, result may be a scalar, an ndarray,
    #     # or type(self.tfield)
    #     axis = args.pop(0) if args else kwargs.get("axis", None)
    #     axes = self._axes_prepare(axis)
    #     res = []
    #     for ax in axes:
    #         kwargs.update({"axis": ax})
    #         res.append(self.tfield.argmin(*args, **kwargs))
    #     context = ("argmin", self.context, self.tfield.order)
    #     return self.tfield.__array_wrap__(res, context=context)
    #
    # def argmax(self, *args, **kwargs):
    #     # Reducing operation, result may be a scalar, an ndarray,
    #     # or type(self.tfield)
    #     axis = args.pop(0) if args else kwargs.get("axis", None)
    #     axes = self._axes_prepare(axis)
    #     res = []
    #     for ax in axes:
    #         kwargs.update({"axis": ax})
    #         res.append(self.tfield.argmax(*args, **kwargs))
    #     context = ("argmax", self.context, self.tfield.order)
    #     return self.tfield.__array_wrap__(res, context=context)

    def min(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or
        # type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.min(*args, **kwargs)

    def amin(self, *args, **kwargs):
        return self.min(*args, **kwargs)

    def max(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.max(*args, **kwargs)

    def amax(self, *args, **kwargs):
        return self.max(*args, **kwargs)

    def mean(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.mean(*args, **kwargs)

    def median(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.median(*args, **kwargs)

    def sum(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.sum(*args, **kwargs)

    def astype(self, dtype):
        # Result is self.tfield, with different dtype
        ret = self.tfield.astype(dtype)
        if self.context == "tensors":
            ret.order = TENSOR_MAJOR
        elif self.context == "voxels":
            ret.order = VOXEL_MAJOR
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")
        return ret

    def reshape(self, *newshape):
        if self.context != "tensors":
            raise AssertionError(
                "The shape of the voxel array cannot be changed.")

        if self.tfield.tsize != np.prod(newshape).astype(int):
            raise AssertionError(f"Cannot reshape tensors from "
                                 f"{self.tfield.tshape} to {newshape}")
        else:
            if self.tfield.order == TENSOR_MAJOR:
                shape = newshape + self.tfield.vshape
            else:
                shape = self.tfield.vshape + newshape
            data = self.tfield.data.reshape(shape)
        tf = self.tfield
        return type(tf)(
            data, vshape=tf.vshape, tshape=newshape, order=tf.order,
            domain=tf.domain[:], header=tf.header.copy(),
            interpolator=tf.interpolator.copy(), name=tf.name,
            keepdims=True, evmgr=tf.evmgr, rule=tf.rule)

    def reduce(self, tensor_op=None):
        """
        Flattens the tensors of the field, yielding a TField with scalar voxel
        values. Unless copy=True, the operation is in-place, hence cannot be
        undone (default behaviour).

        :param tensor_op:
            Reduction operation. If None, scalar values are calculated by
            taking the L2-norm of the tensor values.
        :type tensor_op: TensorOperator

        :returns: a new scalar TField instance
        :rtype: Union[None, TImage]

        """
        if self.context != "tensors":
            raise AssertionError("The voxel array cannot be reduced.")

        tf = self.tfield
        # Do not reduce tensors if the tensor dimension is already 0.
        if not tf.tdim:
            return tf

        # Reduce tensors if the tensor dimension is not 0.
        from tirl.operations.operations import Operator
        if tensor_op is None:
            from tirl.operations.tensor import TensorOperator
            taxes = tuple(range(1, tf.tdim + 1))
            tensor_op = TensorOperator(
                np.linalg.norm, name="l2-norm", opkwargs=dict(axis=taxes))
        elif not isinstance(tensor_op, Operator):
            raise TypeError(
                f"Expected Operator instance, got {tensor_op} instead.")
        ret = tensor_op(tf)
        return ret

    @property
    def shape(self):
        """ Proxy to TField subdomain shape. """
        if self.context == "tensors":
            return self.tfield.tshape
        elif self.context == "voxels":
            return self.tfield.vshape
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")

    @property
    def ndim(self):
        """ Proxy to TField subdomain dimensionality. """
        if self.context == "tensors":
            return self.tfield.tdim
        elif self.context == "voxels":
            return self.tfield.vdim
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")

    @property
    def axes(self):
        """ Proxy to TField subdomain axes. """
        if self.context == "tensors":
            return self.tfield.taxes
        elif self.context == "voxels":
            return self.tfield.vaxes
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")
