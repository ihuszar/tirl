#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
This file defines constants that are global to the TIRL library. These
constants are imported to the main 'tirl' namespace ('import tirl').
Due to the global import, this file must not import from other sources.
User-adjustable constants must not be stored in this file. Please use the
'settings' module for these.

"""


# DEFINITIONS

# Storage modes (Domain, TField, TImage)
MEM = "mem"
HDD = "hdd"
SHMEM = "shmem"
STORAGE_MODES = (MEM, HDD, SHMEM)

# Basis of displacement vectors in TxDisplacementField
NL_ABS = "abs"
NL_REL = "rel"

# Vector/Tensor reorientation (mapping) rules (Chain)
RULE_FS = "fs"
RULE_PPD = "ppd"
RULE_SSR = "ssr"

# Categories of transformations
TX_GENERIC = "generic"
TX_LINEAR = "linear"
TX_NONLINEAR = "nonlinear"
TX_UNKNOWN = "unknown"

# TField layout (axis order)
TENSOR_MAJOR = "T"
VOXEL_MAJOR = "V"

# Default MIND kernel types
MK_STAR = "star"
MK_FULL = "full"
