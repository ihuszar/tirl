#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import dill
import numbers
import warnings
import numpy as np
from pydoc import locate
from numbers import Number, Integral


# TIRL IMPORTS

import tirl
from tirl.utils import kw
from tirl import utils as tu
from tirl.layout import Layout
from tirl import settings as ts
from tirl.dataobj import DataObject
from tirl.tirlobject import TIRLObject
from tirl.tfieldmixin import TFieldMixin
from tirl.evmgr import EvaluationManager
from tirl.domain import Domain, DomainArgs

from tirl.transformations.scale import TxScale
from tirl.operations.spatial import SpatialOperator
from tirl.interpolation.interpolator import Interpolator


# DEFINITIONS

from tirl.constants import *


# DEVELOPMENT NOTES


# IMPLEMENTATION

class TField(TIRLObject, TFieldMixin, np.lib.mixins.NDArrayOperatorsMixin):

    # Set the array priority higher than that of np.matrix with 10.0
    # and np.array with 0.0, so that this class can control ufunc behaviour.
    __array_priority__ = 15.0

    # TField supports arithmetic with the following classes:
    _HANDLED_TYPES = (np.ndarray, numbers.Number)

    @classmethod
    def _constr_tfield(cls, src, **kwargs):
        """
        Returns the input TField if no other arguments are specified.

        If keyword arguments are specified, a new TField instance is returned,
        whose attributes are updated according to the keywords. If the keywords
        permit, the new TField instance will share data (and transformations)
        with the original instance. Where keywords prohibit, the data and/or
        the transformations will be copied instead. These changes will not
        affect the input instance, but field or parameter data may still be
        shared between the objects.

        The header is copied, and the domain is soft-linked, meaning that
        transformations are identical, and parameters are linked, but adding
        or removing transformations from either will not affect the other.
        Swapping the tensor orientation in one object will also have no
        bearing on the other instance. The interpolation are exact copies,
        but are not shared either.

        :param src: source TField instance
        :type src: TField
        :param kwargs: keywords arguments for the new TField instance
        :type kwargs: any

        :returns: new TField instance with updated parameters
        :rtype: TField

        """
        if not kwargs:
            return src
        else:
            fieldkwargs = dict(
                domain=src.domain[:, :],
                # domain=src.domain,
                layout=src.layout.copy(),
                header=src.header.copy(),
                interpolator=src.interpolator.copy(),
                name=src.name,
                resolution=src.resolution,
                evmgr=src.evmgr.copy(),
                rule=src.rule,
                storage=src.storage
            )
            data = kwargs.pop("data", src.data)
            fieldkwargs.update(kwargs)
            return cls._constr_ndarray(data, **fieldkwargs)

    @classmethod
    def _constr_ndarray(cls, src, **kwargs):
        """
        Returns a TField instance that shares data with the input ndarray and
        the provided transformations (if any). If keywords prohibit (such as
        copy=True), the data and/or transformation parameters will be copied
        instead. In addition to the data specification, shape and domain
        (transformation) specifications may be optionally provided:

        1. Data specification

        2. Shape specification (default: scalar array)
            (..., tshape=(1,), order=#) or (..., vshape=src.shape, order=#) or
            (..., taxes=None,) or (..., vaxes=range(src.ndim),)

        3. Domain (transformation) specification (default: voxel array)
            (..., domain=Domain(src.shape)) or
            (..., offset=None, chain=None)

        :Keyword Arguments:
            * *copy*=False ``bool`` -- If True, the resultant TField instance
            will not share data with the input array.

        :param src: input ndarray
        :type src: np.ndarray

        """
        dataobj = cls._get_data_specification(src, **kwargs)
        return cls._constr_dataobj(dataobj, **kwargs)

    @classmethod
    def _constr_dataobj(cls, src, **kwargs):
        """
        Returns a TField instance that is based on the input DataObject
        instance. This constructor avoids copying the data, hence it is less
        flexible than _constr_ndarray().

        :param src: DataObject instance
        :type src: DataObject

        """
        assert isinstance(src, DataObject), "Input is not a DataObject."
        obj = super().__new__(cls)
        # Header
        obj.header = kwargs.pop("header", {})
        # Data specification
        obj._set_data_object(src)
        # Shape specification
        kwargs.pop("array", None)
        obj.layout = cls._get_shape_specification(array=obj.data, **kwargs)
        # Domain specification
        kwargs.pop("layout", None)
        obj.domain = cls._get_domain_specification(
            header=obj.header, layout=obj.layout, **kwargs)
        # Interpolator
        if not obj.domain.iscompact:
            ipkwargs = kwargs.pop("ipkwargs", {})
            ipkwargs.update(basis=obj.domain.base_coordinates().T)
            kwargs.update(ipkwargs=ipkwargs)
        obj.interpolator = cls._get_interpolator_specification(**kwargs)

        # Miscellaneous properties
        obj.name = kwargs.pop("name", None)
        userresolution = kwargs.pop("resolution", None)
        obj.resolution = userresolution or obj.resolution  # override default

        return obj

    @staticmethod
    def _get_loader_specification(**kwargs):
        """
        Returns a Loader instance from various spacifications.

        """
        import inspect
        import tirl.loader
        src = kwargs.pop("file")
        loader = kwargs.pop("loader", None)

        # If the loader is not specified, infer the best Loader from the
        # file extension.
        if loader is None:
            ext = os.path.splitext(src)[-1].lower()
            loader = tirl.loader.LOADER_PREFERENCES.get(ext)
            # Try double extension (like .nii.gz)
            if loader is None:
                ext = "." + ".".join(src.split(".")[-2:])
                loader = tirl.loader.LOADER_PREFERENCES.get(ext)
                if loader is None:
                    raise TypeError(f"None of the TIRL loaders support the "
                                    f"input file type: {ext}")

        # Configure Loader instance (TField constructor arguments dtype and
        # storage should override loader-specific values)

        dtype = kwargs.pop("dtype", None)
        storage = kwargs.pop("storage", None)
        memorylimit = tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT)
        loaderkwargs = kwargs.get("loaderkwargs", {})
        loaderkwargs.update(memorylimit=memorylimit)
        if dtype is not None:
            loaderkwargs.update(dtype=dtype)
        if storage is not None:
            loaderkwargs.update(storage=storage)

        if inspect.isclass(loader):
            return loader(**loaderkwargs)
        elif isinstance(loader, tirl.loader.GenericLoader):
            loader.dtype = dtype
            loader.storage = storage
            return loader
        elif isinstance(loader, str):
            from pydoc import locate
            ltype = locate("tirl.loader." + loader)
            try:
                return ltype(**loaderkwargs)
            except Exception as exc:
                raise TypeError(f"Loader instance could not be created "
                                f"for type {ltype}") from exc
        elif isinstance(loader, dict):
            from pydoc import locate
            mpath = "tirl.loader."
            ltype = str(loader.pop("type"))
            if not ltype.startswith(mpath):
                ltype = mpath + ltype
            loader.pop("dtype")
            loader.pop("storage")
            loaderkwargs.update(**loader)
            ltype = locate(ltype)
            try:
                return ltype(**loaderkwargs)
            except Exception as exc:
                raise TypeError(f"Loader instance could not be created "
                                f"for type {ltype}") from exc
        else:
            raise AssertionError("Invalid Loader specification.")

    @staticmethod
    def _get_data_specification(src, **kwargs):
        """
        Returns an ndarray (view) from different data specifications.

        Specifications are considered in the following order of preference:
        ndarray > array-like > buffer + dtype + count + shape

        :Keyword Arguments:
            * *copy*=False ``bool`` -- If True, the resultant TField instance
            will not share data with the input array.

        """
        if kwargs.get("keepdims", False):
            ignore_singleton_dimensions = False
        else:
            ignore_singleton_dimensions = ts.TFIELD_IGNORE_SINGLETON_DIMENSIONS

        if isinstance(src, np.ndarray):
            # This may be an in-memory ndarray or an np.memmap
            if ignore_singleton_dimensions:
                data = np.squeeze(src)
            else:
                data = src
            dtype = kwargs.get("dtype", data.dtype)
            if data.dtype != dtype:
                data = data.astype(dtype)
        elif hasattr(src, "__array__"):
            if ignore_singleton_dimensions:
                data = np.squeeze(np.asanyarray(src))
            else:
                data = np.asanyarray(src)
            dtype = kwargs.get("dtype", data.dtype)
            if data.dtype != dtype:
                data = data.astype(dtype)
        elif isinstance(src, bytearray):
            shape = kwargs.get("shape")
            if ignore_singleton_dimensions:
                shape = tuple(dim for dim in shape if dim not in (0, 1))
            ds = kw(kwargs, "dtype", "offset", "count")
            data = np.frombuffer(src, **ds).reshape(shape)
        elif isinstance(src, memoryview):
            ds = kw(kwargs, "dtype", "shape")
            if ds.get("shape", None) is None:
                ds["shape"] = TField._get_shape_specification(**kwargs).shape
            data = np.ndarray(buffer=src, **ds)
        else:
            raise TypeError("Invalid data specification.")

        # Copy data on request
        if kwargs.pop("copy", False):
            if isinstance(data, np.memmap):
                fd, fn = tu.tmpfile(prefix="data_", dir=ts.TWD)
                newdata = np.memmap(
                    fn, dtype=data.dtype, mode="r+", offset=0,
                    shape=data.shape, order="C")
                newdata[...] = data[...]
                newdata.flush()
            else:
                newdata = data.copy()
            data = newdata

        storage = kwargs.pop("storage", None)
        if storage == SHMEM:
            dataobj = DataObject("shared", shape=data.shape, dtype=data.dtype)
            dataobj.value[...] = data[...]
        else:
            dataobj = DataObject(data)  # data is either ndarray or memmap

        return dataobj

    @staticmethod
    def _get_shape_specification(**kwargs):
        """
        Returns a fully configured Layout object, which has the following
        attributes: shape, tshape, vshape, taxes, vaxes, order.

        """
        # If a layout object is given, use this.
        layout = kwargs.pop("layout", None)
        if isinstance(layout, Layout):
            layout.verify()
            return layout

        # Try to obtain the full shape from the data
        # (While the shape attribute may be already given, the shape of the
        # input data or input domain should override any possibly
        # erroneous user specification.)
        arr = kwargs.pop("array", None)
        domain = kwargs.pop("domain", None)
        shape = kwargs.pop("shape", None)
        if arr is not None:
            shape = arr.shape
        if domain is not None:
            kwargs.update(vshape=domain.shape)

        # Autodetect tensor axes if no tensor information is specified and
        # this feature is enabled.
        taxes = kwargs.get("taxes", None)
        tshape = kwargs.get("tshape", None)
        vaxes = kwargs.get("vaxes", None)
        vshape = kwargs.get("vshape", None)
        if (shape is not None) and (taxes is None) and (vaxes is None) and \
                (tshape is None) and (vshape is None) and \
                ts.TFIELD_AUTODETECT_TENSOR_AXIS:
            taxes = TField._autodetect_tensor_axes(shape)
            kwargs.update(taxes=taxes)

        # Create new Layout configuration
        return Layout(shape=shape, **kwargs)

    @staticmethod
    def _get_domain_specification(**kwargs):
        """
        Returns a fully configured Domain object from various different
        specifications.

        Specifications are considered in the following order of preference:
        domain > shape + offset + chain/transformations

        """
        # Use a domain object if available
        domain = kwargs.get("domain", None)
        if hasattr(domain, "__domain__"):
            return domain
        elif isinstance(domain, str) and os.path.exists(domain):
            return tirl.load(domain)

        # Obtain shape
        layout = kwargs.pop("layout", None)
        if isinstance(layout, Layout):
            shape = layout.vshape
        else:
            shape = kwargs.pop("vshape", None)
        if shape is None:
            raise AssertionError("Insufficient domain specification.")
        kwargs.update(shape=shape)

        # Assign labelled transformations from the header
        # Transformations in the header come from external sources.
        kwargs = DomainArgs(**kwargs)
        hdr = kwargs.pop("header", {})
        internal = kwargs.pop("internal", [])
        if isinstance(internal, str) or (not hasattr(internal, "__iter__")):
            internal = [internal]
        external = kwargs.pop("external", [])
        if isinstance(external, str) or (not hasattr(external, "__iter__")):
            external = [external]
        if "transformations" in hdr.keys():
            internal = [hdr["transformations"][key] if isinstance(key, str)
                        else key for key in internal]
            external = [hdr["transformations"][key] if isinstance(key, str)
                        else key for key in external]

        return Domain(internal=internal, external=external, **kwargs)

    @staticmethod
    def _get_interpolator_specification(**kwargs):
        """
        Returns a configured Interpolator instance from various different
        specifications:

        1. Interpolator instance (with updated kwargs)
        2. Interpolator class (new instance configured to the data object)
        3. Interpolator type name (new instance configured to the data object)
        3. dict(type="InterpolatorType", **ipkwargs)

        """
        import inspect
        from pydoc import locate
        ipol = kwargs.pop("interpolator", None)
        ipkwargs = kwargs.pop("ipkwargs", {})

        # Input: Interpolator subclass
        if inspect.isclass(ipol) and issubclass(ipol, Interpolator):
            ipol = ipol(**ipkwargs)

        # Input: Interpolator instance
        elif isinstance(ipol, Interpolator):
            # Use the instance provided unless ipkwargs are given
            if ipkwargs:
                ipol = type(ipol)(ipol, **ipkwargs)

        # Input: name of Interpolator subclass (with module pathology)
        elif isinstance(ipol, str):
            try:
                ipol = locate(ipol)(**ipkwargs)
            except Exception as exc:
                raise TypeError(f"Interpolator instance could "
                                f"not be created from {ipol}.") from exc

        # Input: Interpolator object dump
        elif isinstance(ipol, dict):
            ipkwargs = ipol
            try:
                ipol = locate(ipkwargs.pop("type"))(**ipkwargs)
            except Exception as exc:
                raise AssertionError("Invalid interpolator type.") from exc

        # Interpolator: no input - default option
        elif ipol is None:
            domain = kwargs.get("domain", None)
            # Tailor interpolator to Domain type if available
            if hasattr(domain, "iscompact"):
                if domain.iscompact:
                    ipc = locate(ts.DEFAULT_COMPACT_INTERPOLATOR)
                else:
                    ipc = locate(ts.DEFAULT_NONCOMPACT_INTERPOLATOR)
                try:
                    ipol = ipc(**ipkwargs)
                except Exception as exc:
                    raise TypeError(f"Interpolator instance could "
                                    f"not be created from {ipc}.") from exc
            # Assume compact domain type otherwise
            else:
                default_ipc = ts.DEFAULT_COMPACT_INTERPOLATOR
                try:
                    ipol = locate(default_ipc)(**ipkwargs)
                except Exception as exc:
                    raise TypeError(f"Interpolator instance could " 
                                    f"not be created from {ipol}.") from exc

        else:
            raise AssertionError("Invalid interpolator specification.")

        return ipol

    @classmethod
    def _constr_empty(cls, **kwargs):
        """
        Constructs empty TField from shape specification.

        :Keyword Arguments:
            * *fill_value* ``any`` -- default value of the tensors
            (any scalar or tensor value)

        :returns: a new TField instance
        :rtype: TField

        """
        layout = cls._get_shape_specification(**kwargs)
        kwargs.pop("layout", None)
        try:
            dtype = np.dtype(kwargs.pop("dtype", ts.DEFAULT_FLOAT_TYPE))
        except:
            raise AssertionError("Invalid dtype specification.")
        storage = kwargs.pop("storage", None)
        if storage is None:
            nbytes = np.prod(layout.shape) * np.dtype(dtype).itemsize
            if nbytes > tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT):
                storage = HDD
            else:
                n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
                parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
                storage = SHMEM if parallelisation else MEM

        header = kwargs.pop("header", {})
        fill_value = kwargs.pop("fill_value", None)
        if storage in (MEM, SHMEM):
            if fill_value is None:
                data = np.empty(shape=layout.shape, dtype=dtype, order="C")
            elif isinstance(fill_value, Number):
                data = np.full(shape=layout.shape, fill_value=fill_value,
                               dtype=dtype, order="C")
            else:
                data = np.empty(shape=layout.shape, dtype=dtype, order="C")
                cls._fill(data=data, layout=layout, value=fill_value)

        elif storage == HDD:
            fp, fn = tu.tmpfile(prefix="TField_", dir=ts.TWD, suffix=None)
            header.update(filename=fn)
            data = np.memmap(filename=fn, dtype=dtype, mode="r+", offset=0,
                             shape=layout.shape, order="C")
            if fill_value is not None:
                cls._fill(data=data, layout=layout, value=fill_value)

        else:
            raise AssertionError("Invalid storage specification.")

        return cls._constr_ndarray(data, layout=layout, storage=storage,
                                   header=header, **kwargs)

    @classmethod
    def _constr_file(cls, src, **kwargs):
        """
        Constructs a new TField instance from file input.

        This constructor uses filetype-specific Loader instances to return an
        ndarray and header information. Specific loaders can be specified under
        the "loader" keyword in either of the following ways to override the
        default loader :

        1. Configured Loader instance
        2. Loader type (new instance with default arguments)
        3. Name of Loader type (new instance with default arguments)
        4. dict: {"type": "loadername", **loader_kwargs} (to add arguments)

        :returns: a new TField instance
        :rtype: TField

        """
        # TField file
        if src.lower().endswith(ts.EXTENSIONS["TField"]):
            tf = tirl.load(src)
            assert isinstance(tf.data, np.memmap)
            storage = kwargs.pop("storage", None)
            # Choose the most suitable storage option if not specified
            if storage is None:
                if tf.data.nbytes > tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT):
                    storage = HDD
                else:
                    n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
                    parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
                    storage = SHMEM if parallelisation else MEM
            # Assign data from the appropriate data container
            if storage in (MEM, SHMEM):
                tf._update_tfield(np.array(tf.data), tensor_axes=tf.taxes)
            elif storage == HDD:
                tf.header.update(filename=tf.data.filename)
            return cls._constr_tfield(tf, **kwargs)

        loader = kwargs.pop("loader", None)
        loader = cls._get_loader_specification(
            file=src, loader=loader, **kwargs)

        # Apply Loader and configure output
        data, hdr = loader(src)
        header = kwargs.pop("header", hdr)
        return cls._constr_ndarray(data, header=header, **kwargs)

    def __new__(cls, *args, **kwargs):
        """
        Main constructor of TField. Options for construction:

        1. TField or TField-like
        2. ndarray + shape specification
        3. continuous buffer + data type specification + shape specification
        4. TField file
        5. shape specification for new empty TField

        """
        # If a leading positional argument is specified...
        if len(args) > 0:
            src = args[0]
            # Note: the rest of the positional arguments are always ignored.

            if hasattr(src, "__tfield__"):
                return cls._constr_tfield(src, **kwargs)

            elif isinstance(src, DataObject):
                return cls._constr_dataobj(src, **kwargs)

            elif isinstance(src, Integral):
                shape = tuple(int(dim) for dim in args)
                return cls._constr_empty(shape=shape, **kwargs)

            elif isinstance(src, (np.ndarray, memoryview, bytearray)) or \
                    hasattr(src, "__array__"):
                return cls._constr_ndarray(src, **kwargs)

            elif isinstance(src, str):
                if os.path.exists(src):
                    return cls._constr_file(src, **kwargs)
                else:
                    raise FileNotFoundError(f"File not found: {src}")

            else:
                raise AssertionError("Invalid construction signature.")

        # If no positional arguments are specified...
        else:
            return cls._constr_empty(**kwargs)

    def __init__(self, *args, **kwargs):
        """
        Initialisation of TField.

        """
        super(TField, self).__init__()

        # Set EvaluationManager
        evmgr = kwargs.get("evmgr", None)
        if isinstance(evmgr, EvaluationManager):
            evmgr = evmgr.copy()
            evmgr.tf = self
            self.evmgr = evmgr
        elif evmgr is None:
            self.evmgr = EvaluationManager(self, verbose=True)
        else:
            raise TypeError(
                f"Expected EvaluationManager, got {type(evmgr)} instead.")

        # Set tensor transformation rule
        self.rule = kwargs.get("rule", ts.DEFAULT_TENSOR_XFM_RULE)
        # Set instance name
        self.name = kwargs.get("name", self.name)

    ############################# MAGIC METHODS ################################

    def __array__(self, *args):
        """
        Array interface of TField.

        """
        return self.data.__array__(*args)

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):

        default_order = VOXEL_MAJOR  # enables matrix-aware ufuncs!
        # Create a list of inputs converted to default-oriented ndarray
        args = []
        for input_ in inputs:
            if hasattr(input_, "__tfield__"):
                args.append(input_.dataview(default_order))
            else:
                args.append(input_)

        # Create a list of outputs converted to default-oriented ndarray
        outputs = kwargs.pop("out", None)
        if outputs:
            out_args = []
            for output in outputs:
                if hasattr(inputs[-1], "__tfield__"):
                    out_args.append(output.dataview(default_order))
                else:
                    out_args.append(output)
            kwargs["out"] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        # If any of the inputs is a memory mapped array, redirect ufunc
        # operation to the dask implementation.
        # input_arrays = tuple(arr for arr in inputs if hasattr(arr, "__array__"))
        # if any(isinstance(getattr(arr, "__array__")(), np.memmap)
        #        for arr in input_arrays):
        #     raise NotImplementedError(
        #         "ufuncs for HDD objects are not supported in the current "
        #         "version of TIRL. Consider using objects from the TIRL "
        #         "Operator class.")

        # If all inputs are in-memory, redirect ufunc operation to the
        # NumPy implementation (coded in C).
        # else:
        results = self.data.__array_ufunc__(ufunc, method, *args, **kwargs)

        # Create a collection of the final results
        if ufunc.nout == 1:
            results = (results,)
        final_results = []
        for i, (result, output) in enumerate(zip(results, outputs)):
            if output is not None:
                if hasattr(output, "__tfield__"):
                    output.dataview(default_order)[...] = result
                else:
                    output[...] = result
                final_results.append(output)
            else:
                context = (ufunc.__name__, method, default_order)
                tfout = self.__array_wrap__(result, context=context)
                # The order of the output should match that of the ufunc handler
                if hasattr(tfout, "__tfield__"):
                    tfout.order = self.order
                final_results.append(tfout)

        if results is NotImplemented:
            return NotImplemented

        return final_results[0] if len(final_results) == 1 else final_results

    def __array_wrap__(self, arr, context=None):
        """
        Finishing step after applying a NumPy ufunc to the TField.
        Subclasses SHOULD implement this method to provide appropriate type
        casting.

        :param arr:
            Result of the ufunc.
        :type arr: np.ndarray
        :param context:
            Ufunc context information: (ufunc_name, ufunc_method, input_order)
        :type context: Any

        :returns: type cast ufunc output object
        :rtype: TField

        """
        # Return ndarray if the domain shape change is evident (1st test)
        if arr.ndim < self.vdim:
            return arr

        # Has the voxel shape changed?
        uname, umethod, arr_order = context
        if arr_order == VOXEL_MAJOR:
            voxel_intact = arr.shape[:self.vdim] == self.vshape
            new_tshape = arr.shape[self.vdim:]
        else:
            voxel_intact = arr.shape[-self.vdim:] == self.vshape
            new_tshape = arr.shape[:-self.vdim]

        # Return ndarray if voxel shape change has become evident (2nd test)
        if not voxel_intact:
            return arr

        if ts.TFIELD_IGNORE_SINGLETON_DIMENSIONS:
            new_tshape = tuple(dim for dim in new_tshape if dim > 1)

        # Return TField if the voxel shape has not changed
        return type(self)(
            arr, domain=self.domain, tshape=new_tshape, order=arr_order,
            dtype=arr.dtype, interpolator=self.interpolator.copy(),
            name=None, storage=self.storage, header=self.header.copy(),
            evmgr=self.evmgr.copy(), rule=self.rule)

    def __getitem__(self, item):
        """ Indexing is deferred to the data array. """
        return self.data[item]

    @classmethod
    def __operation_finalise__(cls, tfout, op=None, template=None):
        """
        This magic method is called by the Operator class when the output
        TField instance is not specified, and the Operator must therefore
        return a new instance. This method MAY be overloaded in subclasses to
        provide subclass-specific casting of the result of the operation.
        The value returned by this method will be returned to the caller of the
        operation as the result.

        :param tfout: TField output of the operation
        :type: tfout: TField
        :param op: operator
        :type op: Operator
        :param template: input object of the operation
        :type template: Any

        :returns: result of the operation
        :rtype: Any

        """
        return tfout

    def __repr__(self):
        if self.order == VOXEL_MAJOR:
            s = "{0}(domain={1}, tensor={2}, dtype={3}, {4})"
            arg1 = self.vshape
            arg2 = self.tshape
        else:
            s = "{0}(tensor={1}, domain={2}, dtype={3}, {4})"
            arg1 = self.tshape
            arg2 = self.vshape
        return s.format(self.__class__.__name__, arg1, arg2, self.dtype,
                        self.storage)

    def __setitem__(self, key, value):
        """ Indexing is deferred to the data array. """
        self.data[key] = value

    def __slice_wrap__(self, ret, context=None):
        """
        Finishing step of all slicing operations on a TField. Subclasses SHOULD
        implement this method to ensure that the result of the slicing is
        properly type cast and has the correct attributes and methods.

        :param ret:
            TField object returned from the slicing operation.
        :type ret: TField
        :param context:
            Context information: (slicing context, slicing expression)
            - Slicing context: either "voxels" or "tensors", whichever
              subfield of the TField is being indexed.
            - Slicing expression: tuple/slice expression that defines the
              slicing.
        :type context: Any

        :returns: type cast slicing result object
        :rtype: TField

        """
        return ret

    def __tfield__(self):
        """
        TField interface, which subclasses should implement for enhanced
        compatibility.

        :returns: equivalent TField object
        :rtype: TField

        """
        return self

    ########################### PROTECTED METHODS ##############################

    @staticmethod
    def _autodetect_tensor_axes(shape):
        """
        This function attempts to detect tensor axes automatically from the
        image shape using the following rules:

            1. If the image is 2D, a 0-size tensor axis is implied.
            2. If the image is 3D, dimensions with (0, 1, 2, 3, 4) sizes are
               detected as tensor axes.
            3. If the image is 4D, the axes with (0, 1) size, or in absence of
               these, the axis with the smallest size is detected as a tensor
               axis.
            4. If the image is >4D, dimensions with (0, 1) sizes are detected
               as tensor axes.

        :param shape: shape of the input data array
        :type shape: Union[tuple, list]

        :return: indices of logical axes
        :rtype: tuple

        """
        ndim = len(shape)
        t_axes = ()
        if ndim == 3:
            t_axes = tuple(i for i, ax in enumerate(shape)
                           if ax in (0, 1, 2, 3, 4))
        elif ndim == 4:
            t_axes = tuple(i for i, ax in enumerate(shape) if ax in (0, 1))
            if not t_axes:
                t_axes = (tuple(shape).index(min(shape)),)

        elif ndim > 4:
            t_axes = tuple(i for i, ax in enumerate(shape) if ax in (0, 1))

        # Notify about positive result
        # (as this may be unexpected after leaving "t_axes" blank
        if t_axes:
            warnings.warn("Auto-detected tensor axes: {}".format(t_axes))

        return t_axes

    def _change_memmap_dtype(self, m, dt, chunksize=ts.TFIELD_CHUNKSIZE):
        return tu.change_memmap_dtype(m, dt, chunksize=chunksize)

    def _dump(self):
        objdump = super(TField, self)._dump()
        # The header might contain non-serialisable metadata
        try:
            hdr = dill.dumps(self.header)
        except dill.PicklingError:
            hdr = dill.dumps({})
        objdump.update(
            data=self.data,
            layout=self.layout,
            domain=self.domain,
            interpolator=self.interpolator,
            header=hdr,
            name=self.name,
            evmgr=self.evmgr,
            rule=self.rule
        )
        return objdump

    @staticmethod
    def _fill(data, layout, value):
        """
        Fills the tensors of a TField data object with a given scalar or tensor
        value.

        :param data: data object
        :type data: np.ndarray
        :param layout: data layout
        :type layout: Layout
        :param value: fill value (scalar or tensor)
        :type value: any

        """
        if isinstance(value, Number):
            data[...] = value
        else:
            value = np.asarray(value).reshape(layout.tshape)
            if layout.order == TENSOR_MAJOR:
                axtrick = (Ellipsis,) + (np.newaxis,) * layout.vdim
                data[...] = value[axtrick]
            else:
                data[...] = value

        # Flush changes if the data object is memory mapped
        if hasattr(data, "flush"):
            data.flush()

    @classmethod
    def _load(cls, dump):

        data = dump.pop("data")
        layout = dump.pop("layout")
        domain = dump.pop("domain")
        ipol = dump.pop("interpolator")
        header = dill.loads(dump.pop("header"))
        name = dump.pop("name")
        evmgr = dump.pop("evmgr", None)
        rule = dump.pop("rule", None)

        return cls(data, layout=layout, domain=domain, interpolator=ipol,
                   header=header, name=name, evmgr=evmgr, rule=rule)

    def _set_data_object(self, data):
        newdataobj = self._newdataobj(data)
        if not hasattr(self, "_dataobj"):
            self._dataobj = newdataobj
        elif newdataobj is not self._dataobj:
            self._dataobj = newdataobj
        else:
            pass
            # Bugfix 20 May 2021 INH: If we try to replace the DataObject with
            # itself, it will lose its (potentially only) reference from the
            # TField, and it will get destroyed.
        self._data = self._dataobj.value

    def _update_tfield(self, data, tensor_axes):
        # Update data object
        self._set_data_object(data)
        # Update Layout
        self.layout = self._get_shape_specification(
            shape=self._data.shape, taxes=tensor_axes)
        # Update interpolator
        self.interpolator.data = self._data

    ############################ PUBLIC METHODS ################################

    def copy(self, **kwargs):
        """
        Copy constructor. Adding keywords will have an effect on the new
        TField instance.

        """
        # Copy data
        kwargs.pop("copy", None)
        # Copy domain with transformations
        domain = kwargs.pop("domain", self.domain.copy())
        return type(self)(self, domain=domain, copy=True, **kwargs)

    def interpolate(self, coords):
        """
        Multi-threaded evaluation of the complete TField at the specified
        target, which may be a Domain instance or a (n_points, n_dims)
        coordinate array. If the target is specified in physical space, it is
        assumed that all transformations in the chain can be inverted.

        :param coords: target domain or target coordinates (n_points, n_dims)
        :type coords: Domain or np.ndarray

        :returns:
            Interpolated TField data. If the target is a Domain, a new TField
            instance is returned that is based on the target domain. If the
            target is a coordinate array, the result is returned as an ndarray
            with the following shape: (*t_shape, n_points)
        :rtype: TField or np.ndarray

        """
        coords = np.atleast_2d(coords)
        cobj = self.evmgr.interpolate(coords=coords)
        if cobj.kind != "Memory":
            ret = np.array(cobj.value)
        else:
            ret = cobj.value
        return ret

    def evaluate(self, domain, rule="auto"):
        """
        Evaluate tensor field on a specified domain. The original TField
        instance is returned if the target domain is the same as the source
        domain. If the domain instances are different but equal, a copy of the
        original TField instance is returned.

        :param domain:
            Target domain. Tensor values will be evaluated at the points of
            this domain, specified by their physical coordinates.
        :type domain: Domain
        :param rule:
            Defines what happens to the tensors after evaluation at the target
            domain.
            1. None (default): tensor components are interpolated, and no
               further operations take place. This is recommended for
               transformation-invariant tensor fields, such as colour space
               vectors.
            2. "ppd": Preservation of principal directions.
            3. "FS" : Finite strain (only rotations)
            4. "ssr": Scaling + skew + rotations
        :type rule: str or None

        :returns:
            New TField instance with field estimates evaluated on the specified
            domain.
        :rtype: TField

        """
        rule = self.rule if str(rule).lower() == "auto" else rule

        # Save time when the evaluation is trivial
        if domain is self.domain:
            return self
        elif domain == self.domain:
            other = self.copy(domain=domain)
            return other
        else:
            out = self.evmgr.evaluate(domain, rule=rule)  # DataObject

        # Adjust DataObject shape
        if self.order == TENSOR_MAJOR:
            shape = self.tshape + domain.shape
        else:
            shape = domain.shape + self.tshape
        out.value = out.value.reshape(*shape)
        out.shape = shape

        # Create new TField
        if domain.iscompact == self.domain.iscompact:
            ip = self.interpolator.copy()
        elif domain.iscompact:
            ip = locate(ts.DEFAULT_COMPACT_INTERPOLATOR)
        else:
            ip = locate(ts.DEFAULT_NONCOMPACT_INTERPOLATOR)

        # Note: the xfm rule should be taken from the current instance
        return type(self)(
            out, domain=domain, tshape=self.tshape, order=self.order,
            dtype=self.dtype, interpolator=ip, name=None, copy=False,
            header=self.header.copy(), evmgr=self.evmgr.copy(), rule=self.rule)

    def fill(self, value):
        """
        Fills the tensors of the TField with a given scalar or tensor value.
        If the new value cannot be cast to the TField's current data type, the
        data type will be changed and a new data object will be assigned to the
        TField instance.

        :param value: fill value (scalar or tensor)
        :type value: any

        """
        if np.can_cast(getattr(value, "dtype", type(value)), self.dtype):
            self._fill(self.data, self.layout, value)

        else:
            dtype = np.result_type(value, self.dtype)
            if self.storage == MEM:
                data = np.empty_like(self.data, dtype=dtype)
            elif self.storage == SHMEM:
                data = DataObject("shared", shape=self.shape, dtype=dtype)
            else:
                fd, fn = self.tmpfile(prefix="data_", dir=ts.TWD)
                data = np.memmap(fn, dtype=dtype, mode="r+", offset=0,
                                 shape=self.shape, order="C")

            self._fill(data, self.layout, value)
            self._update_tfield(data, self.taxes)

    def flat(self):
        # Result is a flatiterator object to the data array
        return self.data.flat

    @classmethod
    def ones_like(cls, template):
        """
        Returns a new TField instance with a new data object that has the same
        shape and data type as the template, and is filled with ones.
        Additional TField properties (tensor shape and layout, header, domain)
        are copied from the template if available.

        :param template: array-like or TField-like template
        :type template: TField, np.ndarray

        :returns: new TField instance that does not share data with the template
        :rtype: TField

        """
        if isinstance(template, TField):
            obj = template.copy()
            obj.fill(1)
            return obj
        elif hasattr(template, "__array__"):
            template = np.asanyarray(template)
            n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
            parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
            memselect = SHMEM if parallelisation else MEM
            storage = HDD if isinstance(template, np.memmap) else memselect
            return cls(shape=template.shape, tshape=(), fill_value=1,
                       dtype=template.dtype, storage=storage)
        else:
            raise TypeError("Input data type not understood.")

    def preview(self, mode="composite", **kwargs):
        """
        Preview tensor field.

        :param mode: Preview mode.
            "rgb": interpret vector components as red, green and blue image
                   channels
            "composite": separate grayscale image for each tensor component
            "hsv": colour-coded representation of field strength and direction
            "quiver": 2D/3D arrow representation of field strength and direction
        :type mode: Union["rgb", "composite", "quiver"]
        :param kwargs:
            Additional keyword arguments for display function.
        :type kwargs: Any

        """
        from tirl import tirlvision

        # Check the compatibility of the TField with the selected mode
        mode = str(mode).lower()
        if (mode == "rgb") and (self.tshape != (3,)):
            raise ValueError("RGB mode is not compatible with tensor shape {}."
                             .format(self.tshape))
        elif (mode in ("hsv", "quiver")) and (self.tshape not in ((2,), (3,))):
            raise ValueError("{} mode is not compatible with tensor shape {}."
                             .format(mode.upper(), self.tshape))
        elif (mode == "quiver") and (self.vdim not in (2, 3)):
            raise ValueError("QUIVER mode is not compatible with the number of "
                             "spatial dimensions of the TField domain ({})."
                             .format(self.vdim))

        # Call TIRLVision with the selected mode
        if mode == "rgb":
            tirlvision.call("tfield", "preview_rgb", self, **kwargs)
        elif mode == "hsv":
            tirlvision.call("tfield", "preview_hsv", self, **kwargs)
        elif mode == "composite":
            tirlvision.call("tfield", "preview_composite", self, **kwargs)
        elif mode == "quiver":
            tirlvision.call("tfield", "preview_quiver", self, **kwargs)
        else:
            raise ValueError("Unrecognised preview mode: {}".format(mode))

    def rescale(self, *scales, **kwargs):
        """
        Returns a new TField instance that is a scaled version of the original.
        The rescaling only affects the resolution of the TField, but not its
        physical extent. The dimensions of the new TField are rounded to the
        nearest integer value after scaling.

        :param scales: dimension-specific scaling factors (or a global one)
        :type scales: float
        :param kwargs: metadata for the new TImage level
        :type kwargs: any

        """
        # Calculate new size
        if len(scales) == 1:
            scales = (scales[0],) * self.vdim
        else:
            assert len(scales) == self.vdim
        assert all(isinstance(f, Number) for f in scales), \
            "Scale factors must be numeric."
        assert all(f > 0 for f in scales), \
            "Scale factors must be positive."

        newsize = np.round(np.multiply(self.vshape, scales)).astype(int)
        return self.resize(*newsize, **kwargs)

    def resize(self, *newsize, **kwargs):
        """
        Resamples the TField data to a specific domain size and returns a new
        TField instance that shares the transformations (but not the chain)
        with the original TField. Unlike the TImage class, the TField does not
        do resampling in place, and does not keep track of various resolutions.
        Consequently, consecutive down and upsamplings may degrade the data.

        :param newsize: new shape of the TImage
        :type newsize: int
        :param kwargs: keyword arguments for resampling
        :type kwargs: any

        :Keyword Arguments:
            * *presmooth*=True (``bool``) -- If True, the input data is
            smoothed with a Gaussian kernel (FWHM=sampling factor) when a
            lower-resolution TField is generated. This ensures that information
            from all higher-resolution voxels is represented at the lower
            resolution as well. If False, resampling becomes faster, but the
            resultant TField may appear noisy/granular.

            * *update_chain*=True (``bool``) -- If True, all nonlinear
            transformations that are linked dynamically to the input domain
            will be resampled to match the resolution of the new domain. If
            False, nonlinear transformations will not be resampled.

        """
        assert len(newsize) == self.vdim, \
            f"Dimension mismatch ({self.vdim} vs. {len(newsize)}) " \
            f"between old shape {self.vshape} " \
            f"and new shape {newsize}."

        # Create new domain (with offset, such that mapping is preserved)
        d = self.domain
        factors = np.divide(newsize, d.shape)
        invtx = TxScale(*(1 / factors), lock="all", name="resample")
        newdomain = Domain(
            newsize, transformations=[*d.chain], offset=[invtx, *d.offset],
            name=None, storage=d.storage, memlimit=d.memlimit)

        # Pre-smooth input upon request (recommended)
        if kwargs.get("presmooth", ts.TFIELD_PRESMOOTH):
            coeff = 1. / (2. * np.sqrt(2 * np.log(2)))
            t_sigmas = (0,) * self.tdim
            v_sigmas = tuple(1 / f * coeff if f < 1 else 0 for f in factors)
            sigmas = t_sigmas + v_sigmas  # operator uses TENSOR_MAJOR order
            sm = self.smooth(*sigmas, copy=True)
            ret = sm.evaluate(newdomain, rule=None)  # do not transform tensors
            del sm
        else:
            # Do not transform tensors
            ret = self.evaluate(newdomain, rule=None)

        # Ensure that the chain of the new layer is synchronised with the
        # active layer. If update_chain is true, rescale all nonlinear
        # transformations that were dynamically linked to the active layer.
        if kwargs.get("update_chain", True):
            for i, tx in enumerate(self.domain.chain):
                if tx.kind == TX_NONLINEAR:
                    # Only regrid grid-like domains
                    if not tx.domain.iscompact:
                        continue
                    # If the transformation is not dynamically linked to
                    # the current domain, re-grid that transformation.
                    if (tx.domain != ret.domain[:i]) and \
                            (tx.domain == self.domain[:i]):
                        oldtxdom = tx.domain
                        # Erase transformations on the tx domain
                        tx.metaparameters["domain"] = self.domain[:0]
                        # Resample between voxel domains
                        new_tx = tx.regrid(ret.domain[:0])
                        # Add back all new antecedent transformations
                        new_tx.metaparameters["domain"] = ret.domain[:i]
                        # Add the current transformation to the new domain
                        ret.domain.chain[i] = new_tx
                        # Restore domain in the original tx
                        tx.metaparameters["domain"] = oldtxdom

        return ret

    def smooth(self, *sigmas, spatial_op=None, copy=True):
        """
        Applies Gaussian smoothing to the TField data.

        :param sigmas:
            If scalar, it is a global smoothing radius. For the default
            Gaussian smoothing kernel, it is the sigma value in voxels.
            Smoothing radii can be defined for each axis for anisotropic
            smoothing.
        :type sigmas: Union[int, float]
        :param spatial_op:
            Smoothing operation. If None, Gaussian smoothing is applied with
            4 * sigma anisotropic kernel size. The operator must not change the
            voxel shape of the TImage. The operator must accept smoothing radii
            as leading arguments.
        :type spatial_op: SpatialOperator
        :param copy:
            If True (default), the smoothing result is returned as a copy of
            the original TImage (the Domain is preserved). If False, the values
            will be changed in place.

        """
        assert all(isinstance(s, Number) for s in sigmas), \
            "Smoothing radii must be numeric."
        assert all(s >= 0 for s in sigmas), \
            "Smoothing radii (sigmas) must be non-negative."

        # Extend global specification to voxel array
        if len(sigmas) == 1:
            zeros = (0,) * self.tdim
            sigmas = sigmas * self.vdim
            if self.order == TENSOR_MAJOR:
                sigmas = zeros + sigmas
            else:
                sigmas = sigmas + zeros

        if np.allclose(sigmas, 0):
            # Don't copy the transformations, just the data and objects.
            return self.copy(domain=self.domain[:]) if copy else None

        # Define smoothing operator
        from tirl.operations import Operator
        if isinstance(spatial_op, Operator):
            smop = spatial_op
        elif spatial_op is None:
            from tirl.operations.spatial import SpatialOperator
            from scipy.ndimage.filters import gaussian_filter
            kernelsize = ts.TFIELD_PRESMOOTH_KERNELSIZE_NSIGMA
            neighbourhood = int(max(np.ceil(kernelsize * max(sigmas)), 1))
            smop = SpatialOperator(
                gaussian_filter, radius=neighbourhood, name="smooth",
                opkwargs=dict(sigma=sigmas, truncate=kernelsize))
        else:
            raise TypeError("Invalid smoothing operator.")

        # Apply smoothing either in-place or create a copy
        if copy:
            # Don't copy the transformations, just the data and the objects.
            ret = smop(self)
            ret.domain = self.domain[:]
            return ret
        else:
            smop(self, out=self)

    def sum(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        args = list(args)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        kwargs.update({"axis": axis})
        return np.add.reduce(self, *args, **kwargs)

    @classmethod
    def zeros_like(cls, template):
        """
        Returns a new TField instance with a new data object that has the same
        shape and data type as the template, and is filled with zeros.
        Additional TField properties (tensor shape and layout, header, domain)
        are copied from the template if available.

        :param template: array-like or TField-like template
        :type template: TField, np.ndarray

        :returns: new TField instance that does not share data with the template
        :rtype: TField

        """
        if isinstance(template, TField):
            obj = template.copy()
            obj.fill(0)
            return obj
        elif hasattr(template, "__array__"):
            template = np.asanyarray(template)
            n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
            parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
            memselect = SHMEM if parallelisation else MEM
            storage = HDD if isinstance(template, np.memmap) else memselect
            return cls(shape=template.shape, tshape=(), fill_value=0,
                       dtype=template.dtype, storage=storage)
        else:
            raise TypeError("Input data type not understood.")
