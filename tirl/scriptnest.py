#!/usr/bin/env python
import os
import sys
import tirl
import importlib.util
from tirl import settings as ts
from tirl.utils import verify_n_cpu
from multiprocessing.managers import SharedMemoryManager

n_cpu = verify_n_cpu(ts.CPU_CORES)


def main():
    # Create library-wide SharedMemoryManager instance
    smm = SharedMemoryManager()
    smm.start()
    tirl.__smm__ = smm

    # Run the script
    try:
        _script = str(sys.argv[1])
        dirname = os.path.dirname(_script)
        fname = os.path.basename(_script)
        fname, ext = os.path.splitext(fname)
        os.chdir(dirname)
        sys.path.append(dirname)
        m = importlib.import_module(fname)
        # spec = importlib.util.spec_from_file_location("tirlscript", _script)
        # m = importlib.util.module_from_spec(spec)
        # spec.loader.exec_module(m)
        return m.main()
    except Exception:
        raise
    finally:
        smm.shutdown()


if __name__ == "__main__":
    main()