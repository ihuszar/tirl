#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import tirl
import os.path
import tempfile
import warnings

import numpy as np
from numbers import Number
import multiprocessing as mp
from tirl import settings as ts
from tirl.tirlobject import TIRLObject
from numpy.lib.mixins import NDArrayOperatorsMixin
from multiprocessing.shared_memory import SharedMemory


# DEVELOPMENT NOTES

"""
2021-May-17 INH
The DataObject class was designed and implemented after the EvaluationManager 
and the Operators, and there is not sufficient time to revise these 
implementations before the release of version 3.0. As a future improvement, 
TField istances should be allowed to be defined on shared memory, as this class 
would fully support this, and it would eliminate the time that is currently 
spent on copying the data back and forth to share with multiple workers.

"""

# IMPLEMENTATION

class DataObject(TIRLObject, NDArrayOperatorsMixin):
    """
    DataObject allows other TIRLObjects to manipulate in-memory data and
    memory-mapped files using a uniform interface. By tokenising files, it
    keeps track of shared dependencies between multiple TIRLObjects, and
    removes files as soon as they become redundant. Shared memory is allocated,
    shared, and deallocated as needed.

    """
    _HANDLED_TYPES = (np.ndarray, Number)

    def __new__(cls, *args, **kwargs):
        if args and isinstance(args[0], cls):
            return args[0]
        else:
            return super().__new__(cls)

    def __init__(self, *args, **kwargs):
        """
        Initialisation of DataObject.

        DataObject(ndarray) --> memory
        DataObject(shape, dtype)  --> memory
        DataObject(memmap) --> memmap
        DataObject(filename, shape, dtype, filename, [offset=0]) --> memmap
        DataObject(SharedMemory) --> memory
        DataObject(SharedMemory address, shape, dtype) --> ndarray
        DataObject(Buffer, shape, dtype, [offset=0]) --> memory

        """
        super(DataObject, self).__init__()
        self.name = kwargs.get("name", super(DataObject, self).name)

        # Obtain the principal argument
        src = args[0] if args else None

        # Special instructions
        if isinstance(src, str):
            if src.lower() == "memmap":
                self._mmap(src, **kwargs)
                return
            elif src.lower() == "shared":
                self._shared(src, **kwargs)
                return

        # Decide action based on the principal argument
        if src is None:
            src = kwargs.get("memmap", kwargs.get("filename", None))
        if isinstance(src, np.memmap) or isinstance(src, str):
            self._mmap(src, **kwargs)
            return
        if src is None:
            src = kwargs.get("shared", kwargs.get("address", None))
        if isinstance(src, SharedMemory) or isinstance(src, str):
            self._shared(src, **kwargs)
            return
        if src is None:
            src = kwargs.get("buffer", None)
        if isinstance(src, (memoryview, bytearray)):
            self._buffer(src, **kwargs)
        if src is None:
            src = kwargs.get("array", kwargs.get("shape", None))
        if isinstance(src, np.ndarray) or isinstance(src, tuple):
            self._memory(src, **kwargs)
            return

        if src is None:
            raise TypeError(f"DataObject could not be created from "
                            f"{type(src)}")

    def _memory(self, src, **kwargs):
        """
        Initialises an in-memory array.

        """
        self.kind = "Memory"
        self.address = None
        self.offset = 0
        if isinstance(src, np.ndarray):
            self.shape = src.shape
            self.dtype = src.dtype
            self.value = src
            self.owner = True

        elif isinstance(src, tuple):
            self.shape = tuple(int(dim) for dim in src)
            self.dtype = kwargs.get("dtype", np.float_)
            self.value = np.empty(shape=self.shape, dtype=self.dtype)
            self.owner = True

        else:
            raise TypeError(f"Expected shape definition or ndarray "
                            f"instance to initialise DataObject,"
                            f"got {type(src)} instead.")

    def _mmap(self, src, **kwargs):
        """
        Initialises a memory mapped file.

        """
        self.kind = "MemoryMap"
        if isinstance(src, np.memmap):
            self.address = src.filename
            self.offset = int(getattr(src, "offset", 0) or 0)
            self.shape = src.shape
            self.dtype = src.dtype
            self.value = src
            self.owner = True

        elif isinstance(src, str):
            if src.lower() == "memmap":
                import tirl.utils as tu
                src = os.path.join(ts.TWD, "tirl.data." + tu.timehash())
            dirname = os.path.dirname(src)
            if not os.path.isdir(dirname):
                os.makedirs(dirname)
            self.address = src
            self.offset = int(kwargs.get("offset", 0))
            self.shape = kwargs.get("shape")
            self.dtype = kwargs.get("dtype", np.float_)
            mode = kwargs.get("mode", "r+")
            if os.path.isfile(self.address):
                self.owner = False
            else:
                with open(self.address, "w"):
                    pass
                self.owner = True
            self.value = np.memmap(
                filename=self.address,
                offset=self.offset,
                mode=mode,
                shape=self.shape,
                dtype=self.dtype
            )

        else:
            raise TypeError(f"Expected the file name or a memmap object, "
                            f"got {type(src)} instead.")

    def _shared(self, src, **kwargs):
        """
        Initialises a shared memory array.

        """
        self.kind = "SharedMemory"
        self.offset = 0
        if isinstance(src, SharedMemory):
            self.address = src.name
            self.shape = kwargs.get("shape")
            self.dtype = kwargs.get("dtype")
            self.sobj = src
            self.owner = True

        elif isinstance(src, str):
            self.address = src
            self.shape = kwargs.get("shape")
            self.dtype = kwargs.get("dtype", np.float_)
            size = int(np.prod(self.shape) * np.dtype(self.dtype).itemsize)
            try:
                self.sobj = SharedMemory(name=self.address, create=False)
            except FileNotFoundError:
                if self.address.lower() == "shared":
                    self.address = None
                self.sobj = SharedMemory(create=True, size=size)
                self.address = self.sobj.name
                self.owner = True
            else:
                self.owner = False

        else:
            raise TypeError(f"Expected a SharedArray instance "
                            f"or a shared array address, "
                            f"got {type(src)} instead.")

        self.value = np.frombuffer(
            self.sobj.buf,
            dtype=self.dtype,
            count=int(np.prod(self.shape)),
            offset=self.offset
        ).reshape(self.shape)

    def _buffer(self, src, **kwargs):
        """
        Initialises an array from a buffer.

        """
        assert isinstance(src, (memoryview, bytearray)), \
            f"Buffer input must be a memoryview of a bytearray instance, " \
            f"not {type(src)}"
        self.kind = "Buffer"
        self.address = None
        self.offset = int(kwargs.get("offset", 0))
        self.shape = kwargs.get("shape")
        self.dtype = kwargs.get("dtype", np.float_)
        self.owner = False
        self.value = np.frombuffer(
            src,
            dtype=self.dtype,
            count=int(np.prod(self.shape)),
            offset=self.offset
        ).reshape(self.shape)

    def __array__(self):
        return self.value

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        out = kwargs.get('out', ())
        for x in inputs + out:
            # Only support operations with instances of _HANDLED_TYPES.
            # Use ArrayLike instead of type(self) for isinstance to
            # allow subclasses that don't override __array_ufunc__ to
            # handle ArrayLike objects.
            if not isinstance(x, self._HANDLED_TYPES + (DataObject,)):
                return NotImplemented

        # Defer to the implementation of the ufunc on unwrapped values.
        inputs = tuple(x.value if isinstance(x, DataObject) else x
                       for x in inputs)
        if out:
            kwargs['out'] = tuple(
                x.value if isinstance(x, DataObject) else x
                for x in out)
        result = getattr(ufunc, method)(*inputs, **kwargs)

        if type(result) is tuple:
            # multiple return values
            return tuple(type(self)(x) for x in result)
        elif method == 'at':
            # no return value
            return None
        else:
            # one return value
            return type(self)(result)

    @classmethod
    def _load(cls, dump):
        kind = dump.get("kind")
        address = dump.get("address")
        offset = int(dump.get("offset"))
        shape = tuple(dump.get("shape"))
        dtype = np.dtype(dump.get("dtype"))
        value = dump.get("value")

        if kind == "MemoryMap":
            obj = cls(filename=address, offset=offset, shape=shape, dtype=dtype)
        elif kind == "SharedMemory":
            obj = cls(address=address, offset=offset, shape=shape, dtype=dtype)
        else:
            obj = cls(value, offset=offset, shape=shape, dtype=dtype)

        # In theory a DataObject should not be saved. When it is used for it's
        # intended purpose, and is pickled, ownership should be retracted from
        # the reconstructed objects as long as they are pointing to shared
        # resources.
        if obj.kind in ("MemoryMap", "SharedMemory"):
            obj.owner = False

        return obj

    def _dump(self):
        objdump = super(DataObject, self)._dump()
        if self.kind in ("MemoryMap", "SharedMemory"):
            value = None
        else:
            value = self.value
        objdump.update(
            address=self.address,
            kind=self.kind,
            shape=self.shape,
            dtype=np.dtype(self.dtype).str,
            offset=self.offset,
            owner=self.owner,
            value=value
        )
        return objdump

    def deactivate(self):
        """
        Deallocates shared memory and removes the memory-mapped file. These are
        executed in a main method instead of __del__ to avoid warnings, because
        the SharedMemory module attempts to import a module when the
        SharedMemory instance is deallocated, however all imports fail
        in __del__. To avoid warnings, call this method directly from the code
        that uses the DataObject.

        """
        # Deallocate shared memory
        if hasattr(self, "sobj"):
            self.sobj.close()
            self.sobj.unlink()
            delattr(self, "sobj")
        # Delete temporary file
        if isinstance(self.address, str) and os.path.isfile(self.address):
            dirname = os.path.dirname(self.address)
            if os.path.abspath(dirname) == os.path.abspath(ts.TWD):

                os.remove(self.address)
                self.address = None

    def reload(self):
        if self.kind == "MemoryMap":
            self.value = np.memmap(
                filename=self.address,
                offset=self.offset,
                mode="r+",
                shape=self.shape,
                dtype=self.dtype
            )

    def __del__(self):
        try:
            if self.owner:
                self.deactivate()
            else:
                if hasattr(self, "sobj"):
                    self.sobj.close()
        except Exception:
            pid = mp.current_process().pid
            warnings.warn(f"Failed deallocation of DataObject {self.name} "
                          f"in Process {pid} at the following address: "
                          f"{self.address}")
        # else:
        #     if self.owner:
        #         pid = mp.current_process().pid
        #         print(f"Successful deallocation of DataObject {self.name} "
        #                f"in Process {pid} at the following address: "
        #                f"{self.address}")

    def __getitem__(self, item):
        return self.value.__getitem__(item)

    def __setitem__(self, key, value):
        self.value.__setitem__(key, value)
    #
    # def __reduce__(self):
    #     args = ()
    #     kwargs = dict(
    #         shape=self.shape,
    #         dtype=self.dtype,
    #         offset=self.offset,
    #         owner=False,
    #         name=self.name
    #     )
    #     if self.kind == "MemoryMap":
    #         kwargs.update(filename=self.address)
    #     elif self.kind == "SharedMemory":
    #         kwargs.update(address=self.address)
    #     else:
    #         args = (self.value,)
    #
    #     from functools import partial
    #     newdobj = partial(self.__class__, **kwargs)
    #
    #     return newdobj, args

    def __repr__(self):
        return '%s[%s](%r)' % (type(self).__name__, self.kind, self.value)

    def copy(self):
        """ Creates a new DataObject instance with an exact copy of the data."""
        if self.kind == "Memory":
            return type(self)(self.value.copy())
        elif self.kind == "MemoryMap":
            fd, fn = tempfile.mkstemp(dir=ts.TWD, prefix="tirl.mmap.")
            m = np.memmap(fn, dtype=self.dtype, mode="r+", offset=self.offset,
                          shape=self.shape, order="C")
            m.flat[...] = self.value.flat[...]
            return type(self)(m)
        elif self.kind == "SharedMemory":
            size = int(np.prod(self.shape) * np.dtype(self.dtype).itemsize)
            sm = SharedMemory(create=True, size=size)
            return type(self)(sm, shape=self.shape, dtype=self.dtype)
        elif self.kind == "Buffer":
            # Crate an in-memory copy
            arr = np.empty(shape=self.shape, dtype=self.dtype, order="C")
            arr.flat[...] = self.value.flat[...]
            return type(self)(arr)
        else:
            raise TypeError(f"DataObject kind not understood: {self.kind}")

    def flush(self):
        getattr(self.value, "flush", lambda: None)()

    @property
    def nbytes(self):
        return int(np.prod(self.shape) * np.dtype(self.dtype).itemsize)
