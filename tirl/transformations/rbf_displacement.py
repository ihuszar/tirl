#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral
from scipy.interpolate import Rbf


# TIRL IMPORTS

import tirl.utils as tu
from tirl.domain import Domain
from tirl.cmodules.finv import local_affine
from tirl.transformations.displacement import TxDisplacementField


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class TxRbfDisplacementField(TxDisplacementField):
    """
    TxRbfDisplacementField class -
        a deformation field defined on a sparse domain.

    """
    RESERVED_KWARGS = ("field", "domain", "interpolator", "bounds", "lock",
                       "name", "invertible", "mode", "vectorder", "dense_shape",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, field, domain=None, interpolator=None, bounds=None,
                 lock=None, name=None, invertible=True, mode=NL_ABS,
                 vectorder=None, dense_shape=None, **metaparameters):
        """
        Initialisation of TxRbfDisplacementField.

        To qualify, this class must be initialised with a Domain that is
        non-compact, or a TField that is defined on such a Domain.

        :param field:
            Vector field defined as a TField or an NDArray. If the input is a
            TField, the initialiser will enforce TENSOR_MAJOR layout on the
            input field. The input field must be defined on a non-compact
            domain. If the input is an NDArray, the shape must be (v, ...),
            where v is the dimensionality of the displacement vectors. By
            default, v should be equal to the number of spatial dimensions of
            the transformation domain. If the vectors are only defined on a
            subset of the spatial dimensions, or the orientation of the vector
            space is different from that of the transformation domain, this
            must be indicated by setting the "vectorder" argument accordingly
            (see there).
        :type field: TField or np.ndarray
        :param domain:
            Domain that defines the extent of the transformation. This
            argument is ignored if 'field' is a TField. If the field is an
            array, the domain must be specified by a non-compact Domain.
        :type domain: None or Domain
        :param interpolator:
            Field interpolator instance. This argument takes precedence over
            the TField specification. If None (default), and the field input is
            an array, the DEFAULT_NONCOMPACT_INTERPOLATOR will be used.
        :type interpolator: Interpolator or None
        :param bounds:
            Bounds specification for the field parameters.
        :type bounds: tuple
        :param metaparameters: metaparameters of the transformation
        :type metaparameters: Any

        """
        d = domain or getattr(field, "domain", None)
        if not isinstance(d, Domain) or d.iscompact:
            raise TypeError(f"{self.__class__.__name__} must be initialised "
                            f"with a non-compact domain.")

        super(TxRbfDisplacementField, self).__init__(
            field, domain=domain, interpolator=interpolator, bounds=bounds,
            lock=lock, name=name, invertible=invertible, mode=mode,
            vectorder=vectorder, **metaparameters)

        # Set class-specific metaparameters
        self.dense_shape = dense_shape

    # Make sure that this class switches to the dense representation in all
    # arithmetics with other transformations.

    def __add__(self, other):
        return self.dense().__add__(other)

    def __radd__(self, other):
        return self.dense().__radd__(other)

    def __sub__(self, other):
        return self.dense().__sub__(other)

    def __mul__(self, other):
        return self.dense().__mul__(other)

    def __rmul__(self, other):
        return self.dense().__rmul__(other)

    @property
    def dense_shape(self):
        return self.metaparameters.get("dense_shape")

    @dense_shape.setter
    def dense_shape(self, shape):
        # Set default dense shape if the input is None
        # This assumes proper voxel coordinates, not floats in [0, 1].
        if shape is None:
            shape = np.ceil(np.max(
                self.domain.get_voxel_coordinates(), axis=0)).astype(int)
            if np.any(shape < 1):
                from warnings import warn
                warn(f"Dense domain shape for {self.name} is too small. Make "
                     f"sure that voxel coordinates are true voxel coordinates, "
                     f"not normalised floating-point values.")

        if hasattr(shape, "__iter__") and \
                all(isinstance(ix, Integral) for ix in shape):
            shape = tuple(shape)
            if len(shape) == self.domain.ndim:
                self.metaparameters.update(dense_shape=shape)
            else:
                raise AssertionError(
                    "The shape of the dense Domain must have the "
                    "same number of dimensions as the sparse "
                    "domain.")
        elif shape == "auto":
            self.metaparameters.update(dense_shape=shape)
        else:
            raise ValueError(f"Expected a sequence of ints or 'auto' for "
                             f"dense_shape got {shape} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def copy(self):
        """
        Creates an identical TxRbfDisplacementField instance.
        All transformations are copied, hence the new instance does not share
        it with the template.

        """
        field = self.field().copy()
        metaparameters = tu.rcopy(self.metaparameters)
        meta = {k: v for k, v in metaparameters.items()
                if k not in self.RESERVED_KWARGS}
        bounds = (self.parameters.lower_bounds, self.parameters.upper_bounds)
        tx = type(self)(
            field, bounds=bounds, lock=self.parameters.locked, name=self.name,
            invertible=self.invertible, mode=self.mode,
            vectorder=self.vectorder, dense_shape=self.dense_shape, **meta)
        return tx

    def dense(self):
        """
        Returns a dense representation of the deformation field.

        """
        if self.dense_shape == "auto":
            bc = self.domain.base_coordinates()
            lows = np.min(bc, axis=0)
            highs = np.max(bc, axis=0)
            shape = highs - lows
            shape = np.maximum(shape / shape[0] * 100, 1)
            spacing = np.sqrt(np.product(shape) / bc.shape[0])
            dense_shape = np.ceil(shape + 2 * spacing)
            dense_shape[shape == 1] = 1
            from tirl.transformations.scale import TxScale
            from tirl.transformations.translation import TxTranslation
            offset = np.asarray([-spacing / 100 if dense_shape[i] > 1 else 0
                      for i in range(len(dense_shape))]) + lows
            offset = TxTranslation(*offset)
            scale = [0.01 if dense_shape[i] > 1 else 1
                     for i in range(len(dense_shape))]
            scale = TxScale(*scale)
            dense_shape = tuple(dense_shape.astype(int))
            domain = Domain(dense_shape,
                            offset=[scale, offset, *self.domain.offset],
                            transformations=self.domain.chain, name="dense")
        else:
            domain = Domain(self.dense_shape, offset=self.domain.offset,
                            transformations=self.domain.chain, name="dense")
        field = self.std().absolute().field(domain)
        return TxDisplacementField(field)

    @classmethod
    def _load(cls, dump):

        params = dump.get("parameters")
        meta = dump.get("metaparameters")

        # Create TField
        domain = meta.pop("domain")
        vectorder = meta.pop("vectorder")
        interpolator = meta.pop("interpolator")
        # interpolator.data = params.parameters.reshape((-1, *domain.shape))
        # assert interpolator.data.base is params.parameters.base
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")
        dense_shape = meta.pop("dense_shape", None)

        from tirl.tfield import TField

        field = TField(
            params.parameters.data, domain=domain, tshape=(len(vectorder),),
            order=TENSOR_MAJOR, dtype=params.parameters.dtype,
            interpolator=interpolator, name=None, storage=MEM
        )

        # Create transformation object
        bounds = params.get_bounds()
        lock = params.locked
        meta = {k: v for k, v in meta.items() if k not in cls.RESERVED_KWARGS}
        obj = cls(field, bounds=tuple(bounds.T), lock=lock, name=name,
                  invertible=invertible, mode=mode, vectorder=vectorder,
                  dense_shape=dense_shape, **meta)

        return obj

    def matrix(self, coords=None):
        """
        Returns the local affine approximation to the transformation at the
        specified physical locations.

        """
        # Obtain absolute deformation field on an equivalent dense domain
        dense_domain = self.dense().domain
        if self.mode == NL_ABS:
            field = self.field(dense_domain)
        else:
            field = self.rel2abs(self.field(dense_domain))

        # Locations where the local transformation is defined exactly
        gridlocations = field.domain.get_physical_coordinates()
        # spacedim = gridlocations.shape[-1]

        # Obtain the locations within the field
        # where the matrix should be evaluated.
        if coords is None:
            coords = gridlocations
        else:
            coords = np.atleast_2d(coords)

        # Choose local affine routine (C/Cython functions) depending on the
        # dimensions of the physical space, in which the current
        # transformation operates. Note that this may differ from vectdim: the
        # transformation may only operate on x and y coordinates of 3D space.
        affines = local_affine(
            field=field.data, newlocations=coords, gridlocations=gridlocations,
            output=None, vectorder=self.vectorder, maxiter=1000)

        # Fix rank deficiency: if the transformation does not update all
        # coordinates of the input points, the estimated local affine
        # transformation will be rank-deficient, with a zero determinant. This
        # is fixed here by replacing the 0s on the diagonal with 1s.
        # if self.vectdim != spacedim:
        #     for ax in range(spacedim):
        #         if ax not in self.vectorder:
        #             affines[:, ax, ax] = 1
        # Instead of the above, fix rank deficiency empirically, as it may also
        # arise when the displacement along one dimension is infinitesimally
        # small, albeit the dimension per se is defined.
        def fix_rank_deficiency(a, columns, cond=1e-5):
            for col in np.asarray(columns, dtype=int):
                diagonals = a[:, col]
                a[:, col] = np.where(diagonals < cond, 1, diagonals)
            return a

        if affines.shape[-1] == 12:
            affines = fix_rank_deficiency(affines, [0, 4, 8])
            # Note that the C-function uses LAPACK routines and exports the
            # matrix elements in Fortran order
            affines = affines.reshape((-1, 3, 4), order="F")

        elif affines.shape[-1] == 6:
            affines = fix_rank_deficiency(affines, [0, 3])
            affines = affines.reshape((-1, 2, 3), order="F")

        else:
            raise AssertionError(
                f"Unexpected number of affine components: {affines.shape[-1]}")

        # The resultant affines are independent of the transformation's vector
        # order, and they transform xy(z) coordinates. This is in keeping with
        # the fact that DisplacementFields with a non-standard vector order
        # also transform input coordinates in the xy(z) layout. The vector
        # order only refers to the storage order of the vectors within the
        # transformation.

        return affines

    # def inverse(self, domain=None):
    #     """
    #     Returns the inverse transformation, which is by default a dense
    #     displacement field, defined on a domain whose shape is specified by
    #     the transformation's dense_shape attribute.
    #
    #     If necessary, the inverse transformation can be reduced to an
    #     RbfDisplacementField by evaluating the DisplacementField on a
    #     non-compact domain, and casting the result as RbfDisplacementField.
    #
    #     While a sparse inverse transformation may only be fully accurate at the
    #     support points, as opposed to a dense DisplacementField, it will have a
    #     non-zero mapping outside the bounding box of the support points.
    #
    #     """
    #     # If the input domain is not a compact domain,
    #     # create a suitable one that cointains all support points
    #     if getattr(domain, "iscompact", False):
    #         dense = domain
    #     else:
    #         dense = Domain(
    #             self.dense_shape, transformations=self.domain.chain,
    #             offset=self.domain.offset, name=None,
    #             storage=self.domain.storage,
    #             memlimit=self.domain.memlimit)
    #
    #     # Call the parent-class method to create a TxDisplacementField instance
    #     field = self.std().absolute().field(dense)
    #     inv_tx = TxDisplacementField(field).inverse()
    #
    #     # If the input domain was non-compact, return a TxRbfDisplacementField
    #     # by res
    #     if not getattr(domain, "iscompact", True):
    #         field = inv_tx.field(domain)
    #         inv_tx = TxRbfDisplacementField(field, mode=self.mode)
    #
    #     return inv_tx

    def inverse(self, domain=None):
        """
        The inverse is another TxRbfDisplacementField that is defined at the
        points where the current instance's basis points are mapped to. The
        inverse points towards the original basis points.

        """
        pc = self.domain.get_physical_coordinates()
        target = self.map(pc)
        from tirl.tfield import TField
        field = TField(target - pc, domain=Domain(pc))
        return type(self)(field, dense_shape="auto")

    def jacobian(self, coords=None):
        # TODO: account for absolute/relative + vector order!

        # Obtain SciPy Rbf interpolator instance, and use its private functions
        # instead of implementing everything here. There is a risk that these
        # functions may be changed in future versions of SciPy. Should that
        # happen, this function needs to be changed as well.
        rbf = self._dummy_interpolator()

        # Obtain input coordinates in the pseudo-voxel space where the nodes
        # of the RbfInterpolator are defined.
        if coords is None:
            vc = self.domain.get_voxel_coordinates()
            vc = self.domain.offset.map(vc)
        else:
            vc = self.domain.chain.inverse().map(coords)

        # Calculate distance matrix (n_points, n_nodes)
        distance_matrix = rbf._call_norm(vc.T, rbf.xi)
        g = rbf._function(distance_matrix)
        # Calculate the transformation Jacobian for the vector field
        # (stacked scalar field parameters)
        jac = np.einsum("ki,ij->kj", g, np.linalg.inv(rbf.A))
        # Repeat jacobian for all displacement dimensions
        d = self.vectdim
        return np.kron(np.eye(d, d), jac[:, np.newaxis, :])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _dummy_interpolator(self):
        ip = self.interpolator
        return Rbf(*ip.basis, np.zeros(self.domain.numel), **ip.kwargs)

    @staticmethod
    def _d_multiquadric(r, epsilon):
        return r / (epsilon * np.sqrt(epsilon ** 2. + r ** 2.))

    @staticmethod
    def _dd_multiquadric(r, epsilon):
        return epsilon ** 2. / ((epsilon ** 2. + r ** 2.) ** (3. / 2.))

    @staticmethod
    def _d_inverse(r, epsilon):
        return epsilon * r / ((epsilon ** 2. + r ** 2.) ** (3. / 2.))

    @staticmethod
    def _dd_inverse(r, epsilon):
        return epsilon * (epsilon ** 2. - 2. * r ** 2.) / \
               ((epsilon ** 2. + r ** 2.) ** (5./2.))

    @staticmethod
    def _d_gaussian(r, epsilon):
        return -2. * r / (epsilon ** 2.) * np.exp(-((r / epsilon) ** 2.))

    @staticmethod
    def _dd_gaussian(r, epsilon):
        return 2. * (2. * r ** 2. - epsilon ** 2.) / (epsilon ** 4.) * \
               np.exp(-((r / epsilon) ** 2.))

    @staticmethod
    def _d_linear(r, epsilon=None):
        return 1.

    @staticmethod
    def _dd_linear(r, epsilon=None):
        return 0.

    @staticmethod
    def _d_cubic(r, epsilon=None):
        return 3. * (r ** 2.)

    @staticmethod
    def _dd_cubic(r, epsilon=None):
        return 6. * r

    @staticmethod
    def _d_quintic(r, epsilon=None):
        return 5. * (r ** 4.)

    @staticmethod
    def _dd_quintic(r, epsilon=None):
        return 20. * (r ** 3.)

    @staticmethod
    def _d_thin_plate(r, epsilon=None):
        return np.where(r == 0, 0, r * (2. * np.log(r) + 1.))

    @staticmethod
    def _dd_thin_plate(r, epsilon=None):
        return np.where(r == 0, 0, 2. * np.log(r) + 3.)

    def _d_current_model(self, r):
        """ Returns the first derivative of the radial basis function that is
        being used by the transformation object. """
        model = self.interpolator.model
        if isinstance(model, str):
            func = getattr(self, "_d_{}".format(model.lower()))
        elif hasattr(model, "__call__"):
            func = getattr(self, "_d_{}".format(model.__name__))
        else:
            raise ValueError("Invalid radial basis function definition.")
        return func(r, self.interpolator.epsilon)

    def _dd_current_model(self, r):
        """ Returns the second derivative of the radial basis function that is
        being used by the transformation object. """
        model = self.interpolator.model
        if isinstance(model, str):
            func = getattr(self, "_dd_{}".format(model.lower()))
        elif hasattr(model, "__call__"):
            func = getattr(self, "_dd_{}".format(model.__name__))
        else:
            raise ValueError("Invalid radial basis function definition.")
        return func(r, self.interpolator.epsilon)

    def _dump(self):
        objdump = super(TxRbfDisplacementField, self)._dump()
        if self.dense_shape == "auto":
            dense_shape = self.dense_shape
        else:
            dense_shape = [int(dim) for dim in self.dense_shape]
        objdump["metaparameters"]["dense_shape"] = dense_shape
        return objdump


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
