#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral


# TIRL IMPORTS

import tirl.utils as tu
from tirl.transformations import Transformation


# IMPLEMENTATION

class TxEmbed(Transformation):
    """
    TxEmbed class - changes the number of coordinates by zero padding or
    removing coordinates.

    """

    RESERVED_KWARGS = ("parameters", "gap", "name", "invertible",
                       "homogeneous", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, gap, name=None, invertible=True, homogeneous=False,
                 **metaparameters):
        """
        Initialisation of TxEmbed. This transformation has no parameters, only
        metaparameters.

        :param gap:
            Difference between the number of spatial dimensions of the input
            and the output.
        :type gap: int
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param homogeneous:
            Use homogeneous coordinates
        :type homogeneous: bool
        :param kwargs: additional keyword arguments for TxEmbed
        :type kwargs: Any

        """
        # Call superclass initialisation
        super(TxEmbed, self).__init__(bounds=None, lock=None, name=name,
                                      invertible=invertible, **metaparameters)

        # Set class-specific attributes
        self.gap = gap
        self.homogeneous = homogeneous

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        """
        Returns the number of coordinates expected by the transformation.

        """
        # Unspecified
        return None

    @property
    def gap(self):
        return self.metaparameters.get("gap")

    @gap.setter
    def gap(self, d):
        if isinstance(d, Integral):
            self.metaparameters.update({"gap": int(d)})

    @property
    def homogeneous(self):
        return self.metaparameters.get("homogeneous")

    @homogeneous.setter
    def homogeneous(self, h):
        if isinstance(h, (bool, np.bool_)):
            self.metaparameters.update({"homogeneous": bool(h)})
        else:
            raise TypeError(f"Expected boolean input for homogeneous option, "
                            f"got {h.__class__.__name__} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):
        """
        Creates TxEmbed instance from object dump.

        :param dump: object dump
        :type dump: dict

        :returns: TxEmbed instance
        :rtype: TxEmbed

        """
        meta = dump.get("metaparameters")
        gap = meta.pop("gap")
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        homogeneous = meta.pop("homogeneous")
        obj = cls(gap, name=name, invertible=invertible,
                  homogeneous=homogeneous, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def inverse(self):
        """
        Creates a transformation that undoes the embedding by orthogonal
        projection and reduction of coordinate dimensions.

        """
        meta = tu.rcopy(self.metaparameters.copy())
        gap = meta.pop("gap")
        name = meta.pop("name")
        name = f"{name}_inv"
        invertible = meta.pop("invertible")
        homogeneous = meta.pop("homogeneous")
        obj = type(self)(-gap, name=name, invertible=invertible,
                         homogeneous=homogeneous, **meta)
        return obj

    def map(self, coords, signature=None):
        """
        Transforms input coordinates by appending them with additional zeros,
        corresponding to the dimension gap.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray
        :param signature: signature of the input coordinates
        :type signature: list

        :returns: (n_points, m_dimensions) table of transformed coordinates
        :rtype: np.ndarray

        """
        coords = super(TxEmbed, self).map(coords, signature)

        if self.gap == 0:
            return coords

        n, m = coords.shape
        nhc = m - int(self.homogeneous)  # number of non-homogeneous coordinates

        # Perform embedding
        if self.gap > 0:
            # Create new coordinate array
            newcoords = np.zeros(shape=(n, m + self.gap), dtype=coords.dtype)
            # Copy non-homogeneous part
            newcoords[:, :nhc] = coords[:, :nhc]
            # Copy 1's if the input was homogeneous
            if self.homogeneous:
                newcoords[:, -1] = 1

        # Perform reduction but do not project: only remove dimensions if all
        # coordinates along that dimension are close to 0.
        else:
            redax = slice(nhc + self.gap, nhc)
            if np.allclose(coords[:, redax], 0, atol=1e-4):
                newcoords = np.delete(coords, obj=redax, axis=1)
            else:
                newcoords = coords

        return newcoords

    def map_vector(self, vects, coords=None, rule=None, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        # The mapping is identical to that of coordinates, which are
        # essentially position vectors.
        return self.map(vects)

    def map_tensor(self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        tensors = super(TxEmbed, self).map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)

        if self.gap == 0:
            return tensors

        assert tensors.shape[-1] == tensors.shape[-2]
        array_shape = tensors.shape[:-2]
        r = tensors.shape[-1]
        d = r + self.gap

        if self.gap > 0:
            embedded = np.zeros(array_shape + (d, d), dtype=tensors.dtype)
            embedded[..., :r, :r] = tensors
        else:
            embedded = tensors[..., :d, :d]

        return embedded

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=None, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point. Also returns the transformed coordinates.

        """
        vects = self.map_vector(vects, coords, rule, signature)
        if coords is not None:
            coords = self.map(coords)
        return vects, coords

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point. Also returns the transformed coordinates.

        """
        tensors = self.map_tensor(tensors, coords, rule, signature)
        if coords is not None:
            coords = self.map(coords)
        return tensors, coords
