#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import joblib
import hashlib
import numpy as np
from operator import add
from functools import reduce
from numbers import Real


# TIRL IMPORTS

from tirl import utils as tu, settings as ts
from tirl.tirlobject import TIRLObject
from tirl.parameters import ParameterVector
from tirl.parameters import ParameterGroup


# DEFINITIONS

from tirl.constants import TX_GENERIC, TX_LINEAR, TX_NONLINEAR, TX_UNKNOWN


# IMPLEMENTATION

class Transformation(TIRLObject):
    """
    Transformation - base class for all transformations within TIRL.

    """
    # Priority of the current class in operations involving two Transformation
    # objects. The Transformation base class has 0 priority, subclasses are
    # expected to take higher priority and implement arithmetic operations that
    # are specific to them.
    __tx_priority__ = 0

    # This attribute ensures that the Transformation class is not treated
    # as a scalar in arithmetic operations with ndarrays.
    __array_priority__ = 15.0

    RESERVED_KWARGS = ("parameters", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, bounds=None, lock=None, name=None,
                 invertible=False, **metaparameters):
        """
        Initialisation of Transformation.

        :param parameters: transformation parameters (numeric, finite)
        :type parameters: Iterable
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for Transformation
        :type metaparameters: Any

        """
        # Call TIRLObject initialisation
        super(Transformation, self).__init__()

        # Set parameters
        self._parameters = \
            ParameterVector(*parameters, dtype=None, bounds=bounds, lock=lock)

        # Set the metaparameters dictionary
        metaparameters = {k: v for k, v in metaparameters.items()
                          if k not in self.RESERVED_KWARGS}
        self.metaparameters = metaparameters

        # Set object-specific metaparameters
        self.name = f"tx_{id(self)}" if name is None else str(name)
        self.invertible = invertible

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        """
        Returns the number of coordinates expected by the transformation
        object. The purpose of having this attribute is to make transformation
        objects easier to understand and transformation chains easier to debug.

        Subclasses ARE HIGHLY ENCOURAGED to overload this attribute according
        to the following simple rule:
            1. scalar: if the transformation can handle a specific number of
               coordinates.
            2. tuple[int]: if the transformation can handle multiple number of
               coordinates.
            3. None: any number of coordinates; unspecified.

        The base class returns None by default.

        """
        return None

    @property
    def name(self):
        """ Name of the transformation object. """
        return self.metaparameters.get("name")

    @name.setter
    def name(self, n):
        self.metaparameters.update({"name": str(n)})

    @property
    def kind(self):
        if 0 <= self.priority < 1000:
            return TX_GENERIC
        elif 1000 <= self.priority < 2000:
            return TX_LINEAR
        elif 2000 <= self.priority < 3000:
            return TX_NONLINEAR
        else:
            return TX_UNKNOWN

    @property
    def invertible(self):
        return self.metaparameters.get("invertible")

    @invertible.setter
    def invertible(self, iv):
        if not isinstance(iv, (bool, np.bool_)):
            raise TypeError("Property must be set with a boolean.")
        else:
            self.metaparameters.update({"invertible": bool(iv)})

    @property
    def priority(self):
        """
        :rtype: int or float
        """
        return self.__tx_priority__

    @priority.setter
    def priority(self, p):
        if isinstance(p, Real):
            self.__tx_priority__ = p

    @property
    def parameters(self):
        """ Getter method for the parameters attribute.
        Parameters is a read-only property of Transformation; but the array
        is mutable, allowing individual parameters to be changed directly."""
        return self._parameters

    @parameters.setter
    def parameters(self, p):
        raise AttributeError("Transformation parameters are not allowed to be "
                             "reassigned after construction, but they can be "
                             "modified individually.")

    @property
    def metaparameters(self):
        """ Getter method for the metaparameters attribute. """
        return self._metaparameters

    @metaparameters.setter
    def metaparameters(self, kwargs):
        """
        Setter method for the metaparameters attribute.
            :param kwargs: metaparameter dictionary
            :type kwargs: dict

        """
        if isinstance(kwargs, dict):
            self._metaparameters = kwargs
        else:
            raise TypeError("Transformation metaparameters must be specified "
                            "with a dict.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ARITHMETICS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _lower_priority(self, other):
        """ Returns True if the current transformation has lower priority than
        another transformation. """
        if not isinstance(other, Transformation):
            return False
        self_priority = getattr(self, "__tx_priority__", 0)
        other_priority = getattr(other, "__tx_priority__", 0)
        return self_priority < other_priority

    # Decision: Parameter bounds are reset to class-specific defaults, because
    # the changes of the bounds under the operation is undefined.

    def __add__(self, other):
        if self._lower_priority(other) and hasattr(other, "__radd__"):
            return other.__radd__(self)
        # Note: although copying 'self' and 'other' seems desirable, it is
        # mostly unnecessary, while being computationally expensive for
        # non-linear transformations. Therefore the decision was made not to
        # copy the individual transformations into the TransformationGroup.
        if isinstance(other, Transformation):
            return TransformationGroup(self, other, copy=False)
        elif other is None:
            return self
        else:
            return NotImplemented

    def __radd__(self, other):
        if self._lower_priority(other) and hasattr(other, "__add__"):
            return other.__add__(self)
        if isinstance(other, Transformation):
            return TransformationGroup(other, self, copy=False)
        elif other is None:
            return self
        else:
            return NotImplemented

    def __neg__(self):
        return self.inverse()

    def __sub__(self, other):
        if self._lower_priority(other) and hasattr(other, "__rsub__"):
            return other.__rsub__(self)
        if isinstance(other, Transformation):
            return TransformationGroup(self, other.inverse(), copy=False)
        elif other is None:
            return self
        else:
            return NotImplemented

    def __rsub__(self, other):
        if self._lower_priority(other) and hasattr(other, "__sub__"):
            return other.__sub__(self)
        if isinstance(other, Transformation):
            return TransformationGroup(other, self.__neg__(), copy=False)
        elif other is None:
            return self
        else:
            return NotImplemented

    def __eq__(self, other):
        if self._lower_priority(other) and hasattr(other, "__eq__"):
            return other.__eq__(self)
        try:
            # Note: The truth value of an empty array is ambiguous. Direct
            # equality comparison is deprecated in NumPy v1.15.
            assert self.parameters.size == other.parameters.size, \
                "The number of parameters is not equal."
            if self.parameters.size > 0:
                assert np.all(self.parameters == other.parameters), \
                    "Parameters are different."

            self_meta = self.metaparameters.copy()
            self_meta.pop("name", None)
            other_meta = other.metaparameters.copy()
            other_meta.pop("name", None)
            assert tu.dictcmp(self_meta, other_meta),\
                "Metaparameters are different."
            assert getattr(self, "domain", None) == \
                   getattr(other, "domain", None), \
                "Transformation domains are different."
        except AssertionError as exc:
            return False
        else:
            return True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __repr__(self):
        """
        String representation of a Transformation instance.

        :returns: string representation
        :rtype: str

        """
        t = self.__class__.__name__
        npar = self.parameters.size
        nl = self.parameters.count_locked()
        try:
            dim = self.dim
        except Exception:
            dim = None
        name = self.name
        ret = f"{t}(parameters={npar}, locked={nl}, dim={dim}, name={name})"
        return ret

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _dump(self):
        """
        Creates transformation descriptor dict from current Transformation
        instance.

        :returns: transformation descriptor dict
        :rtype: dict

        """
        objdump = super(Transformation, self)._dump()
        # Note: non-linear transformations will have a Domain among their
        # metaparameters. Note that the metaparameters dictionary must be
        # copied to avoid dumping real metaparameter objects of existing
        # objects, such as a Domain of a non-linear transformation.
        objdump.update({
            "parameters": self.parameters,#.copy(),  # ParameterVector
            "metaparameters": tu.rcopy(self.metaparameters)
        })
        return objdump

    @classmethod
    def _load(cls, dump):
        """
        Creates Transformation (or subclass) instance from object dump.

        Subclasses MUST overload this method without calling the base class
        method if their constructor signature is different from that of the
        base class.

        :param dump: object dump
        :type dump: dict

        :returns: Transformation instance
        :rtype: Transformation

        """
        parameters = dump.get("parameters", ())
        params = parameters.parameters
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        metaparameters = dump.get("metaparameters", {})
        name = metaparameters.pop("name")
        invertible = metaparameters.pop("invertible")
        meta = {k: v for k, v in metaparameters.items()
                if k not in ("bounds", "lock", "name", "invertible")}
        return cls(*params, bounds=(lb, ub), lock=lock, name=name,
                   invertible=invertible, **meta)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def signature(self):
        """
        Creates Transformation signature, which is a 40-character long
        hexdigest that uniquely (but redundantly) identifies a mapping between
        two sets of coordinates. Multiple signatures may refer to the same
        mapping, but if two transformations have the same signature, it is
        guaranteed that they perform the same mapping. The transformation
        signature depends on the transformation object type, as well as the
        parameters and metaparameters of the transformation object.

        :returns: Transformation signature
        :rtype: str

        """
        def dicthash(node):
            if isinstance(node, dict):
                for key in node.keys():
                    node[key] = dicthash(node[key])
                else:
                    return node
            elif isinstance(node, (list, tuple)):
                lst = []
                for elem in node:
                    lst.append(dicthash(elem))
                else:
                    return lst
            elif hasattr(node, "signature"):
                return getattr(node, "signature")
            else:
                return joblib.hash(node)

        # Calculate the signature of the metaparameters
        # (this may include calculating the signature of TIRLObjects)
        meta = self.metaparameters.copy()
        metasig = dicthash(meta)

        hash = hashlib.sha1()
        hash.update(str(self.__class__.__name__).encode("utf-8"))
        hash.update(str(self.parameters.signature).encode("utf-8"))
        hash.update(str(metasig).encode("utf-8"))
        return hash.hexdigest()

    def apply(self, t_img):
        """
        Transforms a TImage by appending the transformation to the external
        chain, and evaluating the so transformed image on the original image
        domain.

        :param t_img: input TImage
        :type t_img: TImage

        :returns: transformed TImage
        :rtype: TImage

        """
        original = t_img.domain
        newdomain = t_img.domain[:]
        newdomain.external.append(self)
        t_img.domain = newdomain
        result = t_img.evaluate(original)
        t_img.domain = original
        return result

    def copy(self):
        """
        Copy constructor.

        :returns: An identical but independent instance of Transformation.
        :rtype: Transformation

        """
        cls = type(self)
        copyobj = super(cls, self).__new__(cls)
        copyobj._metaparameters = tu.rcopy(self.metaparameters)
        copyobj._parameters = self.parameters.copy()
        return copyobj

    def inverse(self):
        """
        Returns a transformation object that carries out the inverse
        transformation. The base class does not implement inversion by default.
        Subclasses flagged as 'invertible' MUST implement this method.

        """
        return NotImplementedError()

    def map(self, coords, signature=None):
        """
        The map function of the base class ensures uniform input (coordinate
        table) to the map functions of subclasses. When it is overloaded in
        subclasses, it is therefore encouraged to invoke the map function of
        the superclass.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: Union[np.ndarray, tuple, list]
        :param signature:
            Signature of the input coordinates. For transformations that are
            themselves defined on a domain, this can be useful to identify
            which set of voxel coordinates the input coordinates came from,
            and provide faster mapping functionality when they come from a
            specific source.

            E.g. costly interpolation of a displacement field (that is defined
            on a specific domain) may be avoided if displacements are
            defined for the exact same points that the coordinates refer to,
            i.e. the domain of the displacement field matches the domain of the
            image, the coordinates of which it is mapping.
        :type signature: list

        """
        # Append the current transformation object's signature to the signature
        # chain that can be used to trace the origin of the voxel coordinates.
        if signature is not None:
            signature.append(self.signature())
        return np.atleast_2d(np.asanyarray(
            coords, dtype=ts.DEFAULT_FLOAT_TYPE))

    def map_vector(self, vects, coords=None, rule=None, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        return np.atleast_2d(np.asanyarray(
            vects, dtype=ts.DEFAULT_FLOAT_TYPE))

    def map_tensor(self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        return np.atleast_2d(np.asanyarray(
            tensors, dtype=ts.DEFAULT_FLOAT_TYPE))

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=None, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point. This method should also return the transformed
        coordinates (if provided) as the second return value in a tuple.

        :returns: (transformed vectors, transformed coordinates)
        :rtype: tuple[np.ndarray, np.ndarray]

        """
        vects = np.atleast_2d(np.asarray(vects, dtype=ts.DEFAULT_FLOAT_TYPE))
        coords = np.atleast_2d(np.asarray(coords, dtype=ts.DEFAULT_FLOAT_TYPE))
        return vects, coords

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point. This method should also return the transformed
        coordinates (if provided) as the second return value in a tuple.

        :returns: (transformed tensors, transformed coordinates)
        :rtype: tuple[np.ndarray, np.ndarray]

        """
        tensors = \
            np.atleast_2d(np.asarray(tensors, dtype=ts.DEFAULT_FLOAT_TYPE))
        coords = np.atleast_2d(np.asarray(coords, dtype=ts.DEFAULT_FLOAT_TYPE))
        return tensors, coords

    def islinked(self, target):
        """
        Determines whether the current transformation is dynamically
        linked to a given Domain, TField or TImage instance.

        Note: all linear transformations are dynamically linked.

        :param target: target domain, tensor field or tensor image
        :type target: Union[Domain, TField, TImage]

        :returns:
            2 if the transformation is dynamically linked,
            1 if the transformation is statically linked
            0 if the transformation is not linked
        :rtype: bool

        """
        if hasattr(target, "domain") and target.domain is not None:
            domain = target.domain
        elif hasattr(target, "transformations") and isinstance(target, list):
            domain = target
        else:
            raise TypeError(
                "Input type ({target.__class__.__name__}) has no attributes "
                "'domain' or 'transformations'.")

        if hasattr(self, "domain"):
            # The domain of the non-linear transformation must match the
            # upstream part of the domain it is part of to be dynamically
            # linked.
            for i, tx in enumerate(domain.chain):
                if self is tx:
                    return 2 if self.domain == domain[:i] else 1
            else:
                # Return 0 if the transformation is not part of the chain.
                return 0
        else:
            # All transformations without a domain are dynamically linked by
            # definition
            return 2

    def get(self, mask_or_indices=None):
        """
        Returns the specified subset of transformation parameters. This method
        is a wrapper around ParameterVector.get(), providing additional
        flexibility and syntatic ease for accessing parameters without
        explicitly having to change to the lock state of the parameters in
        advance.

        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be returned.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        :returns: specified subset of the transformation parameters
        :rtype: np.ndarray

        """
        if mask_or_indices is not None:
            orig_locked = self.parameters.locked
            self.parameters.lock()
            self.parameters.unlock(mask_or_indices)

        # Update all free parameters
        parameters = self.parameters.get()

        # Restore the lock state of the parameters
        if mask_or_indices is not None:
            self.parameters.unlock()
            self.parameters.lock(orig_locked)

        return parameters

    def update(self, delta, mask_or_indices=None):
        """
        Updates the specified transformation parameters by the given amount.
        Use either a sequence of indices or a boolean parameter mask to
        select a subset of parameters to be updated. The method preserves
        dynamic linking.

        To implement a specific update rule, such as p1 = p2 + p3, subclass the
        most suitable Transformation object, and override the update method of
        the child class to implement the specific update rule. Make sure that
        the original lock state of the parameters is restored after calling the
        update() method.

        :param delta:
            Absolute parameter increments for the selected parameters. Delta
            must be either a scalar (to increment parameters globally), or a
            flat array that has one element corresponding to each selected
            (aka free, non-locked) parameter (to increment parameters
            individually).
        :type delta: Union[int, float, np.number, np.ndarray]
        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be updated.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        """
        # Save the lock state of the parameters
        if mask_or_indices is not None:
            orig_locked = self.parameters.locked
            self.parameters.lock()
            self.parameters.unlock(mask_or_indices)

        # Update all free parameters
        self.parameters.update(delta)

        # Restore the lock state of the parameters
        if mask_or_indices is not None:
            self.parameters.unlock()
            self.parameters.lock(orig_locked)

    def set(self, parameters, mask_or_indices=None):
        """
        Replaces the values of the specified subset of transformation
        parameters with the ones provided. This method is a wrapper around
        ParameterVector.set(), providing additional flexibility and syntatic
        ease for modifying parameters without explicitly having to change to
        the lock state of the parameters in advance. The method preserves
        dynamic linking.

        :param parameters: new values for the specified parameters
        :type parameters: Union[tuple[float], list[float], np.ndarray]
        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be returned.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        """
        # Save the lock state of the parameters
        if mask_or_indices is not None:
            orig_locked = self.parameters.locked
            self.parameters.lock()
            self.parameters.unlock(mask_or_indices)

        # Update all free parameters
        self.parameters.set(parameters)

        # Restore the lock state of the parameters
        if mask_or_indices is not None:
            self.parameters.unlock()
            self.parameters.lock(orig_locked)

    def jacobian(self, coords):
        """
        This method should be overridden in subclasses based on their
        implementation of the map() function. The Jacobian provides
        compatibility with optimisation methods that use gradient information.

        """

        return NotImplementedError(
            "The Jacobian of the transformation is not implemented.")

    # def _jtj(self, coords):
    #     """
    #     This is a protected function that provides a fallback for the Hessian
    #     by approximating it linearly from the Jacobian. As a consequence, this
    #     functionality relies on the implementation of the Jacobian.
    #
    #     """
    #     # TODO: This does not support memory mapping!
    #     J = self.jacobian(coords)
    #     Jt = J.transpose(0, -1, -2)
    #     return np.tensordot(Jt, J, ([-1], [-2]))
    #
    # def hessian(self, coords):
    #     """
    #     This method should be overridden in subclasses based on their
    #     implementation of the map() function. The Hessian provides
    #     compatibility with optimisation methods that use gradient information.
    #
    #     """
    #     return self._jtj(coords)

    # def get_signature(self):
    #     """
    #     Creates a unique fingerprint that identifies the transformation. Note
    #     that the signature does not change with parameter updates, and may not
    #     be entirely unique for transformations without parameters.
    #
    #     :return: a fingerprint that qualitatively identifies the transformation
    #     :rtype: str
    #
    #     """
    #     if self.parameters.size > 0:
    #         # TODO: Consider using the ID instead.
    #         s = joblib.hash(id(self))
    #     else:
    #         metaparameters = self.metaparameters.copy()
    #         # Discard the name, because it might be too unique
    #         metaparameters.pop("name", None)
    #         s = joblib.hash(metaparameters)
    #     if hasattr(self, "homogenise"):
    #         homo = "_homo" if self.homogenise else ""
    #     else:
    #         homo = ""
    #     return "tx%s%s" % (s, homo)


class TransformationGroup(Transformation):
    """
    TransformationGroup is a subclass of the Transformation base class that
    allows concatenating several separate transformations into one.

    Note that TransformationGroups are non-commutative and the order of the
    members cannot be changed after the construction of TransformationGroup.
    Therefore members should be specified in the correct order when the group
    is created.

    The combined TransformationGroup defines a unified ParameterVector
    (more precisely a ParameterGroup, which is a subclass of ParameterVector),
    that is dynamically linked to the ParameterVectors of the member
    Transformations. This means that any changes at the group level in the
    transformation parameters, bounds or locks will propagate downstream and
    will be reflected in those attributes of the members as well. Changes to
    the parameters and the bounds also propagate upstream, but changes to the
    lock state does not, and the TransformationGroup will retain access to
    those parameters that were locked within members.

    TransformationGroups may be embedded in another TransformationGroup along
    with other Transformation objects. However, each Transformation can only
    belong to one TransformationGroup at a time, unless the
    TransformationGroups are hierarchically organised (i.e. one is embedded in
    the other). When a Transformation that is already part of an existing
    TransformationGroup joins another independent TransformationGroup, it will
    become dynamically linked to the second group and statically linked to the
    first group. This means that within the second group it will continue to
    function as a Transformation, but parameter/bounds/lock state updates that
    are initiated upstream will not change the parameters/bounds of the
    Transformation.

    """
    # Priority of the current class in operations involving two Transformation
    # objects. The Transformation base class has 0 priority, subclasses are
    # expected to take higher priority and implement arithmetic operations that
    # are specific to them.
    __tx_priority__ = 10

    RESERVED_KWARGS = ("copy", "name")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *tx, copy=False, name=None, invertible=None,
                 **metaparameters):
        """
        Initialisation of TransformationGroup.

        :param tx:
            Member transformations.
        :type tx: Transformation
        :param copy:
            If False (default), member transformation objects will be
            dynamically linked to the group without copying. This may break
            existing dynamic links with other groups.
            If True, each of the specified member transformations will be
            copied first, and their copies will be dynamically linked to the
            group. The input transformations will remain independent of the
            group, but the construction may be slower if transformations with
            a large number of parameters are involved.
        :param name:
            Name of the TransformationGroup instance. If None (default), a name
            of the form tg_objectID will be automatically assigned.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). For a TransformationGroup to be invertible,
            all member Transformation objects must implement the inverse()
            method. If None (default), invertibility is inferred automatically
            from the same attribute of the member Transformations.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TransformationGroup.
        :type metaparameters: Any

        """
        # Note that the parameter management is slightly different from that of
        # the Transformation base class, therefore we call the TIRLObject
        # initialiser here.
        super(Transformation, self).__init__()

        if tx == ():
            raise ValueError("No member transformations were specified.")

        # Create a copy of the member transformations if requested
        txs = tu.ci(tx) if copy else tx
        # Store the member transformations as an immutable sequence
        elements = tuple(txs)

        # Set parameters
        # Note that this will break the link between the parameters of an
        # individual transformation and other objects that were pointing to it,
        # such as interpolation.
        self._parameters = ParameterGroup(*tuple(tx.parameters for tx in txs))

        # Set up the metaparameters dictionary
        self.metaparameters = metaparameters

        # Set class-specific metaparameters
        self.name = f"tg_{id(self)}" if name is None else name
        self.metaparameters.update(elements=elements)

        # Invertibility
        if isinstance(invertible, bool):
            self.invertible = invertible
        elif invertible is None:
            self.invertible = all([tx.invertible for tx in txs])
        else:
            raise TypeError("Invalid specification for invertibility.")

        # Re-link interpolation to the parameters of the transformations
        for tx in self.elements:
            if hasattr(tx, "interpolator"):
                if tx.interpolator.data is not None:
                    tx.interpolator.data = tx.parameters.parameters.reshape(
                        tx.interpolator.data.shape)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        return self.elements[0].dim

    @property
    def elements(self):
        return self.metaparameters.get("elements")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # def __iter__(self):
    #     return iter(self.elements)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):
        """
        Creates TransformationGroup (or subclass) instance from object dump.

        :param dump: object dump
        :type dump: dict

        :returns: TransformationGroup instance
        :rtype: TransformationGroup

        """
        meta = dump.get("metaparameters")
        txs = meta.pop("elements")
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        # meta = {k: v for k, v in meta.items() if k not in cls.RESERVED_KWARGS}
        obj = cls(*txs, copy=False, name=name, invertible=invertible, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def copy(self):
        """
        Copy constructor.

        :returns: an identical but independent instance of TransformationGroup
        :rtype: TransformationGroup

        """
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        return type(self)(*tu.ci(self.elements), name=name,
                          invertible=invertible, **meta)

    def reduce(self):
        """
        Returns a Transformation or TransformationGroup that minimises the
        number of Transformation.map() calls to achieve an identical mapping.
        Sequences of linear transformations are reduced to a single linear
        transformation. Non-linear transformations that define addition are
        also merged. TransformationGroups are recursively expanded and reduced.

        :returns: Minimum equivalent transformation (chain).
        :rtype: Transformation

        """
        # This algorithm sums any sub-chain of linear transformations in a
        # mixed sequence of linear and non-linear transformations. Non-linear
        # transformations are preserved in the output sequence unless they
        # define the add operation. TransformationGroups are recursively
        # expanded and reduced.
        def recursion(transformations):
            # Return input if there is nothing to optimise
            if len(transformations) <= 1:
                return transformations
            # Reduce linear sequences
            chain = []
            opt = []
            for tx in transformations:
                if hasattr(tx, "matrix"):
                    chain.append(tx)
                else:
                    if chain:
                        opt.append(reduce(add, chain))
                        chain = []
                    if isinstance(tx, TransformationGroup):
                        opt.extend(recursion(tx.elements))
                    else:
                        opt.append(tx)
            else:
                if chain:
                    opt.append(reduce(add, chain))
                return opt

        # Execute recursive algorithm
        opt = recursion(self.elements)
        # Run it one final time to reduce expanded transformation groups
        opt = recursion(opt)

        # Return a single transformation (either TxLinear or TxGroup)
        # NB: This summation adds the transformations to a new
        # TransformationGroup by reference. This reassings the parameter
        # vectors, but since these are always addressed via the transformation
        # object, this should be no problem for the optimisation.
        return reduce(add, opt)

    def count(self):
        return len(self.elements)

    def inverse(self):
        meta = tu.rcopy(self.metaparameters)
        invtxs = tuple(tx.inverse() for tx in self.elements[::-1])
        name = f"{self.name}_inv"
        meta = {k: v for k, v in meta.items()
                if k not in ("parameters", "copy", "name", "invertible")}
        return type(self)(*invtxs, copy=False, name=name,
                          invertible=True, **meta)

    def map(self, coords, signature=None):
        """
        Applies the member transformations on the specified input coordinates,
        and updates the signature chain.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray
        :param signature: chain of transformation signatures
        :type signature: list

        :returns: (n_points, m_dimensions) transformed coordinate table
        :rtype: np.ndarray

        """
        # Standardise input coordinates, but do not add the signature of the
        # TransformationGroup to the chain: the signature chain should list the
        # individual transformations.
        coords = super(TransformationGroup, self).map(coords, signature=None)
        try:
            for i, tx in enumerate(self.elements):
                coords = tx.map(coords, signature)
        except (ValueError, TypeError) as exc:
            raise TypeError("TransformationGroup is not internally "
                            "consistent or not compatible with the data.") \
                from exc
        return coords

    # TODO: Deprecate Transformation.jacobian()
    def jacobian(self, coords):
        """
        Jacobian of the TransformationGroup.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray

        :returns: Jacobian of the TransformationGroup at the input points
        :rtype: np.ndarray

        """
        # TODO: Needs further testing.
        # Ensure that input coordinates are in the correct layout
        coords = super(TransformationGroup, self).map(coords)

        # Calculate Jacobians
        jacobians = []
        for i, tx in enumerate(self.elements):
            if tx.parameters.size > 0:
                # Calculate current Jacobian
                try:
                    jac = tx.jacobian(coords)
                    # j_shape = jac.shape
                    # This reshape is necessary for fast mapping in subsequent
                    # transformations. Otherwise the first index should be
                    # iterated in Python, which would be a major impact on
                    # performance.
                    # jac = jac.reshape(-1, j_shape[-1])
                except NotImplementedError as exc:
                    raise NotImplementedError(
                            "The Jacobian of the transformation group cannot "
                            "be calculated, because the Jacobian function is "
                            "not implemented for at least one parametric "
                            "member transformation.")
                # Calculate exact Jacobian based on all transformations,
                # using Huszar propagation.
                if i + 1 < len(self.elements):
                    for next_tx in self.elements[i+1:]:
                        if not hasattr(next_tx, "propagator"):
                            continue
                        propagator = next_tx.propagator(coords)
                        # jac = np.tensordot(jac, propagator, ([-1], [-2]))
                        jac = np.matmul(jac, propagator)
                # Note: the indentation of the next line is crucial, as the
                # Jacobian of the last transformation also has to be appended.
                jacobians.append(jac)
                # Transform coordinates with the current transformation
                coords = tx.map(coords)
            else:
                # Transform coordinates with the current transformation
                coords = tx.map(coords)
                continue

        # Concatenate Jacobians of all element transformations
        # TODO: This doesn't work when homo and non-homo coordinates are mixed.
        jacobians = np.concatenate(jacobians, axis=1)
        return jacobians

    # TODO: Deprecate Transformation.propagator()
    def propagator(self, coords):
        # If a TransformationGroup is part of a chain, this method is required
        # for gradient-based optimisation.
        # TODO: Implement this for the sake of completeness.
        raise NotImplementedError()


if __name__ == "__main__":
    print("""This module is not intended for execution.""")
