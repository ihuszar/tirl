#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from math import pi


# TIRL IMPORTS

import tirl.utils as tu
from tirl import settings as ts

from tirl.point import Point
from tirl.transformations.rotation import TxRotation3D


# IMPLEMENTATION

class TxEulerAngles(TxRotation3D):
    """
    TxEulerAngles (child of TxRotation3D) class - uses 3-angle parametrisation
    of 3D rotations.

    Depending on the choice of the rotation axes, angles might refer to either
    Euler or Tait-Bryan angles. Depending on the order of the chosen axes,
    angles may represent either intrinsic rotations about the axes of the
    rotating frame or extrinsic rotations about the axis of the lab frame.

    The 3-angle representation of 3D rotations is intuitive, but one important
    shortcoming is the gimbal lock condition, which arises when one angle
    becomes equal to 90 degrees. This aligns 2 of the 3 rotation axes, reducing
    the degrees of freedom, hence making the corresponding rotation angles
    about the aligned axes dependent. Escaping the gimbal lock condition is
    difficult during optimisation therefore it should be avoided by ensuring
    that images have roughly the same initial orientation, and restricting the
    optimisation range to [-90, 90] degrees. See Wikipedia article on
    "Euler angles" for a discussion of the 3-angle parametrisation of 3D
    rotations, and the article on "gimbal lock" for a more detailed discussion
    of this important shortcoming.

    Rotation angles are converted to radians and mapped to the [-pi, pi) range
    before getting stored as parameters. The default parameter bounds for this
    class are therefore set to [-pi, pi] radians.

    """

    RESERVED_KWARGS = ("angles", "order", "mode", "centre", "bounds", "lock",
                       "name", "invertible", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *angles, order="zyx", mode="deg", centre=None,
                 bounds=(-pi, pi), lock=None, name=None, invertible=True,
                 **metaparameters):
        """
        Initialisation of TxEulerAngles.

        :param angles:
            Euler or Tait-Bryan angles. Either in degrees (default) or in
            radians depending on the 'mode' specification. Alternatively a
            rotation matrix may be specified, that will be decomposed into 3
            rotation angles based on the rotation axis order specification. In
            either case the angles are converted to radians and mapped to the
            [-pi, pi) range before being stored as parameters.
        :type angles: Union[int, float, np.ndarray]
        :param order:
            Order of the rotation axes. One of the following:
            Euler: ["xyx", "xzx", "yxy", "yzy", "zxz", "zyz"]
            Tait-Bryan: ["xyz", "xzy", "yxz", "yzx", "zxy", "zyx"]
            default: "zyx" (yaw - pitch - roll)
        :type order: str
        :param mode:
            Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str
        :param centre:
            The centre of the rotation, which remains stationary under the
            rotation. The centre may be specified either as an array of
            coordinates or as a Point object. With the latter, rotations can be
            defined relative to a moving Domain. Defaults to the origin.
        :type centre: Point or np.ndarray or None
        :param bounds:
            Bounds for the rotation angles must be defined in radians.
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxEulerAngles.
        :type metaparameters: Any

        """
        if angles == ():
            raise ValueError("No parameters were specified.")

        # Interpret parameter and shape specification.
        # The input shape is set to None for a practical reason. If the input is
        # specified by a matrix, shape will no longer be None, and parameters
        # will be returned in radians. If the input was given as a sequence of
        # rotation angles, shape will remain None, and parameters will be in
        # whatever is specified in mode. If the input shape was (3, 3) there
        # would be no way to decide whether the parameters have come from a
        # matrix or from direct specification, and their units would be
        # ambiguous.
        parameters, shape = self._read_parameters_and_shape(
            angles, shape=None, order=order, mode=mode)
        # Direct parameter specification -> convert angles to radians
        if shape is None:
            shape = (3, 3) if centre is None else (3, 4)
            parameters = self._rad(parameters, mode=mode)
        # Matrix specification -> parameters are already in radians
        elif shape in ((3, 3), (3, 4)):
            pass
        # Input matrix was not a (3, 3) or (3, 4) rotation matrix
        else:
            raise ValueError("Invalid rotation angle specification.")

        # Map rotation angles to the [-pi, pi) interval
        parameters = np.where(np.isclose(np.abs(parameters), pi), -pi,
            np.sign(parameters) * (np.abs(parameters) % pi))

        # Call superclass initialisation
        super(TxEulerAngles, self).__init__(
            *parameters, centre=centre, bounds=bounds, lock=lock, name=name,
            invertible=invertible, order=order, mode=mode, **metaparameters)

        # Re-set class-specific metaparameters
        self.order = order
        self.mode = mode

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def mode(self):
        """
        Returns rotation angle units. Either "deg" or "rad".

        """
        return self.metaparameters.get("mode")

    @mode.setter
    def mode(self, m):
        """
        Sets rotation angle units.

        :param m: Rotation angle unit. Either "deg" or "rad".
        :type m: str

        """
        if m not in ("rad", "deg"):
            raise ValueError("Only 'rad' and 'deg' modes are supported.")
        else:
            self.metaparameters.update({"mode": m})

    @property
    def order(self):
        """
        Returns rotation axis order.

        """
        return self.metaparameters.get("order")

    @order.setter
    def order(self, axes):
        """
        Sets rotation axis order.

        :param axes:
            Order of the rotation axes. One of the following:
            Euler: ["xyx", "xzx", "yxy", "yzy", "zxz", "zyz"]
            Tait-Bryan: ["xyz", "xzy", "yxz", "yzx", "zxy", "zyx"]
            default: "zyx" (yaw - pitch - roll)
        :type axes: str

        """
        if axes in TxRotation3D.TAIT_BRYAN_AXES:
            self.metaparameters.update({"order": axes})
        elif axes in TxRotation3D.EULER_AXES:
            self.metaparameters.update({"order": axes})
        else:
            raise ValueError(f"Unrecognised axis order: {axes}.")

    @property
    def angles(self):
        """
        Returns the parameters of the transformation object taking the mode
        setting into account.

        :returns:
            Rotation angles in degrees of radians, depending on the mode
            setting.
        :rtype: np.ndarray

        """
        angles = self.euler(order=self.order, mode=self.mode)
        return np.asarray(angles, dtype=self.parameters.dtype)

    @angles.setter
    def angles(self, phis):
        """
        Sets the rotation angles of the transformation object. New values are
        interpreted according to the mode setting (either "deg" or "rad").

        :param phis:
            New rotation angles either in degrees or in radians according to
            the mode setting.
        :type phis: Union[list[float], tuple[float], np.ndarray]

        """
        dtype = self.parameters.dtype
        parameters = self._rad(phis, mode=self.mode)
        self._parameters[:] = np.asarray(parameters, dtype=dtype)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):
        """
        Creates TxEulerAngles (or subclass) instance from object dump.

        :param dump: object dump
        :type dump: dict

        :returns: TxEulerAngles instance
        :rtype: TxEulerAngles

        """
        parameters = dump.get("parameters")
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        name = meta.pop("name")
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")
        centre = meta.pop("centre")
        order = meta.pop("order")
        obj = cls(*parameters[:], order=order, mode="rad", centre=centre,
                  bounds=(lb, ub), lock=lock, name=name, invertible=invertible,
                  **meta)
        obj.mode = mode
        return obj

    @staticmethod
    def _rad(angles, mode):
        """
        Interprets angle input based on the 'mode' setting and returns the
        rotation angles in radians.

        The purpose of having this private method is syntactic clarity: being
        able to avoid an explicit checking of self.mode.

        :param angles: angles to be interpreted
        :type angles: np.ndarray
        :param mode: rotation angle units; either "deg" or "rad"
        :type mode: str

        :returns: angles (interpreted in deg/rad based on mode) in radians
        :rtype: np.ndarray

        """
        if mode == "rad":
            return np.asarray(angles)
        elif mode == "deg":
            return np.deg2rad(angles)
        else:
            raise ValueError("Unsupported mode. Choose either 'rad' or 'deg'.")

    @staticmethod
    def _Rx(phi):
        s = np.sin(phi)
        c = np.cos(phi)
        mat = np.asarray([[1, 0, 0],
                          [0, c, -s],
                          [0, s, c]])
        return mat

    @staticmethod
    def _Ry(phi):
        s = np.sin(phi)
        c = np.cos(phi)
        mat = np.asarray([[c, 0, s],
                          [0, 1, 0],
                          [-s, 0, c]])
        return mat

    @staticmethod
    def _Rz(phi):
        s = np.sin(phi)
        c = np.cos(phi)
        mat = np.asarray([[c, -s, 0],
                          [s, c, 0],
                          [0, 0, 1]])
        return mat

    @staticmethod
    def _dRx(phi):
        s = np.sin(phi)
        c = np.cos(phi)
        mat = np.asarray([[0, 0, 0],
                          [0, -s, -c],
                          [0, c, -s]])
        return mat

    @staticmethod
    def _dRy(phi):
        s = np.sin(phi)
        c = np.cos(phi)
        mat = np.asarray([[-s, 0, c],
                          [0, 0, 0],
                          [-c, 0, -s]])
        return mat

    @staticmethod
    def _dRz(phi):
        s = np.sin(phi)
        c = np.cos(phi)
        mat = np.asarray([[-s, -c, 0],
                          [c, -s, 0],
                          [0, 0, 0]])
        return mat

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates (3, 3) rotation matrix from rotation angles, given a specific
        order of rotation axes. If the rotation has a centre that is different
        from the origin, the returned matrix is (3, 4).

        :param parameters:
            Rotation angles in radians.
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve rotation matrix
            from parameters.
        :type kwargs: Any

        :returns: (3, 3) rotation matrix or (3, 4) eccentric rotation matrix
        :rtype: np.ndarray

        """
        dtype = ts.DEFAULT_PARAMETER_DTYPE
        order = kwargs.get("order")
        parameters = np.asarray(parameters, dtype=dtype)
        Rax = {"x": TxEulerAngles._Rx,
               "y": TxEulerAngles._Ry,
               "z": TxEulerAngles._Rz}
        R = np.eye(3, dtype=dtype)
        for i, ax in enumerate(order):
            R = np.dot(R, Rax[ax](parameters[i]).astype(dtype))

        # Taking the rotation centre into account
        centre = kwargs.get("centre", False)
        if centre:
            mat = np.eye(4)
            mat[:3, :3] = R
            centre = Point(centre)
            trans = np.eye(4)
            trans[:-1, -1] = -centre.y
            R = np.linalg.inv(trans) @ mat @ trans
            R = R[:3, :]

        return R

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Calculates 3D rotation angles from (3, 3) rotation matrix or (3, 4)
        eccentric rotation matrix given an order of rotation axes, and mode
        (deg or rad).

        :param mat: (3, 3) rotation matrix or (3, 4) eccentric rotation matrix
        :type: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from rotation
            matrix.
        :type kwargs: Any

        :returns: Rotation angles in radians.
        :rtype: np.ndarray

        """
        order = kwargs.get("order")
        angles = TxEulerAngles._euler(mat[:3, :3], order=order, mode="rad")

        # If the rotation matrix is not about the origin, then the
        # centres must be identical.
        centre = kwargs.get("centre", False)
        if mat.shape[-1] == 4:
            rot = mat[:3, :3]
            trans = mat[:3, -1]
            centre = np.linalg.inv(np.eye(3) - rot) @ trans.reshape(-1, 1)
            kwargs.update(centre=Point(centre.ravel()))
            assert np.allclose(mat @ centre, centre)
        if mat.shape[-1] == 3 and centre:
            raise AssertionError("Conflicting input: the rotation matrix is "
                                 "about the origin, but a centre point was "
                                 "also specified.")

        return np.asarray(angles, dtype=ts.DEFAULT_PARAMETER_DTYPE)

    def euler(self, order="zyx", mode="deg"):
        """
        Returns Euler (or Tait-Bryan) rotation angles of the transformation.

        This method was overloaded to avoid unnecessary computations and the
        accumulation of numerical errors.

        :param order:
            Three-letter string specification of the rotation axis order. Any
            permutation of x, y, z is valid. The default setting is "zyx".
        :type order: str
        :param mode:
            Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str

        :returns: Euler or Tait-Bryan angles
        :rtype: tuple

        """
        if self.order == order:
            if mode == "rad":
                return tuple(self.parameters[:].tolist())
            elif mode == "deg":
                return tuple(np.rad2deg(self.parameters[:]).tolist())
        else:
            return super(TxEulerAngles, self).euler(order=order, mode=mode)

    def inverse(self):
        """
        Creates inverse TxEulerAngles (or subclass) instance that undoes the
        rotation of the current transformation object.

        :returns: inverse TxEulerAngles object
        :rtype: TxEulerAngles

        """
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")
        centre = meta.pop("centre")
        order = meta.pop("order")
        try:
            if centre:
                mat = np.eye(4)
                mat[:3, :] = self.matrix
                inv_mat = np.linalg.inv(mat)[:3, :]
            else:
                inv_mat = np.linalg.inv(self.matrix)
        except np.linalg.linalg.LinAlgError as exc:
            raise ArithmeticError("Matrix is not invertible.") from exc
        else:
            # Use same order, mode and invertibility, default bounds
            # and no lock.
            # Initialise centre with None: the constructor will infer it from
            # the inverted rotation matrix
            obj = type(self)(inv_mat, order=order, mode=mode, centre=None,
                             bounds=(-pi, pi), lock=None, name=name,
                             invertible=invertible, **meta)
            if centre:
                epsilon = np.finfo(inv_mat.dtype).eps
                assert np.allclose(obj.centre.y, self.centre.y, atol=epsilon)
            return obj

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     # Force coordinate table input format
    #     coords = super(TxLinear, self).map(coords)
    #     n_points, n_coords = coords.shape
    #     dRax = {"x": self._dRx, "y": self._dRy, "z": self._dRz}
    #     Rax = {"x": self._Rx, "y": self._Ry, "z": self._Rz}
    #     angles = self.parameters  # in radians
    #     dR0 = np.eye(3)
    #     dR1 = np.eye(3)
    #     dR2 = np.eye(3)
    #     for i, ax in enumerate(self.order):
    #         next_mat = dRax[ax](angles[i]) if i == 0 else Rax[ax](angles[i])
    #         dR0 = np.dot(dR0, next_mat)
    #         next_mat = dRax[ax](angles[i]) if i == 1 else Rax[ax](angles[i])
    #         dR1 = np.dot(dR1, next_mat)
    #         next_mat = dRax[ax](angles[i]) if i == 2 else Rax[ax](angles[i])
    #         dR2 = np.dot(dR2, next_mat)
    #     else:
    #         d = 3 + int(self.homogenise)
    #         dR0h = np.zeros((d, d))
    #         dR0h[:3, :3] = dR0
    #         dR1h = np.zeros((d, d))
    #         dR1h[:3, :3] = dR1
    #         dR2h = np.zeros((d, d))
    #         dR2h[:3, :3] = dR2
    #
    #     # Derivative tensor
    #     t = np.stack((dR0h, dR1h, dR2h), axis=0)
    #
    #     # Calculate Jacobian
    #     jac = np.einsum("ijk,hk", t, coords)
    #     return jac


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
