#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from math import sqrt
from math import floor


# TIRL IMPORTS

from tirl import utils as tu, settings as ts
from tirl.transformations.linear import TxLinear


# IMPLEMENTATION

class TxAffine(TxLinear):
    """
    TxAffine (child of TxLinear) - generic affine transformation class.

    """

    RESERVED_KWARGS = ("parameters", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, bounds=None, lock=None, name=None,
                 invertible=True, **metaparameters):
        """
        Initialisation of TxAffine.

        :param parameters:
            Affine parameters may be specified by one of the following methods:
            1. (n, n+1) affine matrix
            2. (n * (n+1)) flattened affine parameters
        :type parameters: Union[np.ndarray, list, tuple]
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxAffine.
        :type metaparameters: Any

        """
        if parameters == ():
            raise ValueError("No parameters were specified.")

        # Interpret the parameter and shape input. Shape is None for this class.
        parameters, shape = \
            self._read_parameters_and_shape(parameters, shape=None)

        # Calculate shape and call superclass initialisation
        shape = self._get_matrix_shape(parameters)
        super(TxLinear, self).__init__(
            *parameters, shape=shape, bounds=bounds, lock=lock,
            name=name, invertible=invertible, **metaparameters)

        # Set class-specific metaparameters
        # Nothing to do.

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def _get_matrix_shape(parameters):
        """
        Calculates the shape of the affine matrix from the number of
        parameters.

        """
        N = np.asarray(parameters).size
        n = 0.5 * (sqrt(4 * N + 1) - 1)
        if floor(n) != n:
            raise ValueError(f"Invalid number of affine parameters: {N}.")
        else:
            n = int(round(n))
            return n, n + 1

    @classmethod
    def _load(cls, dump):
        parameters = dump.get("parameters")
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        shape = meta.pop("shape")
        obj = cls(*parameters[:], bounds=(lb, ub), lock=lock, name=name,
                  invertible=invertible, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        """
        Returns the number of coordinate that is expected by the transformation.

        """
        return self.shape[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Creates affine parameter vector from affine matrix. If exists, the
        homogeneous part of the matrix (last row) is removed automatically.
        (This was the reason for overloading the parent-class method.)

        :param mat: affine transformation matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve quaternion from rotation
            matrix.
        :type kwargs: Any

        :returns: vector of affine parameters
        :rtype: np.ndarray

        """
        mat = np.asarray(mat, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        # Discard homogeneous row of the affine matrix
        if np.allclose(mat[-1, :-1], 0) and np.allclose(mat[-1, -1], 1):
            mat = mat[:-1, :]
        n, m = mat.shape
        if m != n + 1:
            raise ValueError(f"Affine matrix has invalid shape: {n, m}")
        parameters = np.asarray(mat, dtype=ts.DEFAULT_PARAMETER_DTYPE).ravel()
        return parameters

    def inverse(self):
        """
        Creates the inverse transformation object that undoes the affine
        transformation conveyed by the current object. The resultant object is
        also an affine transformation.

        :returns: inverse transformation
        :rtype: TxAffine

        """
        # Calculate inverse parameters
        n, m = self.shape
        mat = np.eye(n + 1, m)
        mat[:n, :m] = self.matrix
        invmat = np.linalg.inv(mat)

        # Construct inverse transformation object
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        invertible = meta.pop("invertible")
        shape = meta.pop("shape")
        obj = type(self)(invmat, bounds=None, lock=None, name=name,
                         invertible=invertible, **meta)
        return obj

    def map(self, coords, signature=None):
        """
        Transforms input coordinates by applying the scale-skew-rotation matrix
        and the translation parameters separately.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray
        :param signature: signature of the input coordinates
        :type signature: list

        :returns: (n_points, m_dimensions) table of transformed coordinates
        :rtype: np.ndarray

        """
        # Call the Transformation base class method
        coords = super(TxLinear, self).map(coords, signature)

        # Create new coordinate table
        mat = self.matrix
        scaleskewrot = mat[:, :-1]
        trans = mat[:, -1].ravel()

        return np.dot(scaleskewrot, coords.T).T + trans

    # TODO: Deprecate Transformation.jacobian()
    def jacobian(self, coords):
        coords = super(TxLinear, self).map(coords)
        n_points, n_coords = coords.shape
        n_dim = self.matrix.shape[-1]
        # Homogenise coordinates if necessary
        if n_dim == n_coords + 1:
            coords = np.hstack((coords, np.ones((n_points, 1))))
            jac = super().jacobian(coords)
            n_params = self.parameters.size
            return np.ascontiguousarray(jac[:, :n_params, :-1])
        elif n_dim == n_coords:
            jac = super().jacobian(coords)
            n_params = self.parameters.size
            return np.ascontiguousarray(jac[:, :n_params, :])
        else:
            raise AssertionError(
                "This TxAffine instance is not compatible "
                "with the specified coordinates.")


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
