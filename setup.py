#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import os
import shutil

import tirl
import numpy as np
from Cython.Build import cythonize
from tirl.settings import CONFIGFILE, TSREGISTRY
from setuptools import setup, Extension, find_packages

#-------------------------------- C/C++ Modules -------------------------------#

# Field inversion (2D and 3D)
fieldinversion = \
    Extension("tirl.cmodules.finv",
              sources=["tirl/cmodules/finv2d_src.c",
                       "tirl/cmodules/finv3d_src.c",
                       "tirl/cmodules/finv.pyx"],
              include_dirs=[np.get_include()],
              extra_link_args=[
                  "-llapacke", "-llapack", "-lcblas", "-lblas"],
              extra_compile_args=["-std=c99"],
              language="c")

# FSLInterpolator
# Implements the linear interpolation routine of FLIRT (FSL).
# This is an example of a C++ extension to TIRL.
fslinterpolator = \
    Extension("tirl.cmodules.fslinterpolator",
              sources=["tirl/cmodules/fslinterpolator_src.cpp",
                       "tirl/cmodules/fslinterpolator.pyx"],
              extra_compile_args=["-fpermissive"],
              language="c++")

cython_modules = cythonize([fieldinversion, fslinterpolator],
    compiler_directives={'embedsignature': True, 'language_level': "3"}
)


# -------------------------------- User config --------------------------------#

configdir = os.path.dirname(CONFIGFILE)
if not os.path.isdir(configdir):
    os.makedirs(configdir)
shutil.copy2("tirl.yml", CONFIGFILE)
with open(CONFIGFILE, "r") as fp:
    cnf = fp.read()
    cnf = cnf.replace("<TWD>", f"/tmp/tirl-{os.getuid()}/TWD")
with open(CONFIGFILE, "w") as fp:
    fp.write(cnf)


# ---------------------------- TIRLScripts registry ---------------------------#

tsconfigdir = os.path.dirname(TSREGISTRY)
if not os.path.isdir(tsconfigdir):
    os.makedirs(tsconfigdir)
shutil.copy2("tirlscripts.yml", TSREGISTRY)


#-------------------------------- TIRL Installer ------------------------------#

setup(
    name="tirl",
    version=tirl.__version__,
    description="Tensor Image Registration Library",
    author="Istvan N. Huszar",
    ext_modules=cython_modules,
    install_requires=None,
    packages=find_packages("."),
    package_dir={"tirl": "tirl"},
    include_package_data=True,
    scripts=["tirl/tirl"]
)
